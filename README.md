# dts_ghana

Scripts and (some) data for calibrating DTS measurements and estimating soil moisture using data assimilation with Hydrus-1D.

## Getting started
Some tips to get started with the scripts and notebooks in this repository:

1. Clone this repository and install the Python environment (I recommend Anaconda): `conda env create -f environment.yml`
2. Look at the three calibration notebooks to get a quick introduction to DTS data: reading the data, aligning the measurements, calibrating the data, and visualizing the results.
3. Look at the soil moisture notebook to get an introduction to the method for estimating soil moisture. This will explain what data is required, how to write input files, how to run the Hydrus model and how to read and visualize the results.
4. Take a look at the reports and memos in the `./report` directory.

## Contents
The repository contains the following information:
- `data`:
   - `nyankpala`: data related to the DTS measurements: temperature timeseries of the calibration baths and the start times of the measurements. The raw DTS data is not included.
   - `tahmo`: meteorological data and soil probe measurements from TAHMO station in Nyankpala.
- `hydrus_src`:
  - source code for the custom Hydrus model written by Jianzhi Dong
  - compiled executable of this Hydrus version (in the `./bin` folder).
- `report`:
   - report about the installation of the Nyankpala DTS set-up
   - two logs showing calibration results (one by Corjan, one by Davíd)
   - memo about soil moisture estimation
- `scripts`:
  - `calibration`: scripts and notebooks related to DTS calibration
    - original script by Corjan (`Halo_DTS_calibration_dev_new.py`)
    - numbered scripts for reading the data (`01`), calibrating the temperature (`02`), and presenting the results (`03`)
    - notebooks for reading the data (`01`), calibrating the temperature (`02`), and presenting the results (`03`). These are similar to the scripts but with a bit more explanation about the code.
  - `soilmoisture`: scripts and notebook related to estimating soil moisture
    - `write_*.py` files: functions for writing Hydrus input files
    - `util.py`: some helper functions
    - `data_generator_*.py`: script to generate data for Hydrus (one for Jianzhi Dong's case, one for Nyankpala)
    - `run_data_assimilation_*.py`: script to run the Hydrus model and read the results (one for Jianzhi Dong's case, one for Nyankpala)
    - `soilmoisture_nyankpala_dts.ipynb`: notebook combining the data generation, running and post-processing of the Hydrus model (i.e. the two python scripts from the previous two bullets). The notebook contains extra explanation about the code.
- `environment.yml`: a file describing the Python environment I used to run everything. Re-create my environment on your PC by running `conda env create -f environment.yml`.

## References
Some potentially useful references:
- [Jianzhi Dong's dissertation](https://repository.tudelft.nl/islandora/object/uuid:b6234864-c97b-4efd-91b3-89295b6b5a4a?collection=research): for information about the soil moisture estimation method.
- [Hydrus manual](https://github.com/phydrus/phydrus/blob/master/source/HYDRUS1D-4.08.pdf): for information and help on the original Hydrus-1D
- [Phydrus Python package](https://github.com/phydrus/phydrus): for running Hydrus models from Python
- [dtscalibration Python package](https://python-dts-calibration.readthedocs.io/en/latest/): for information on DTS data and calibration

## Contributors
The following people have contributed directly to this project:
- Maxwell Boateng
- Patrick Lamptey
- Corjan Nolet, Future Water
- Davíd Brakenhoff, Artesia
- Dr. ir. Marie-Claire ten Veldhuis
- Prof. Nick van de Giesen

And special thanks to Jianzhi Dong for developing the method and helping out by answering questions about his original code.
