c*****************************************************************************	
	!Subroutine Assi(data1,data2,dm1,obs_val,obslen,upbound,
 !    !				lowbound,N,Node,t)
      
      	Subroutine Assi(data1,data2,dm1,dm2,obs_val,obslen,upbound,
     !				lowbound,N,Node,t,flag,smooth)

	Integer N,dm1,obslen,Node,dm2
	real upbound,lowbound,H(obslen,dm2),
     !	 obs_val(obslen),Gam(1,N),
     !	 Gamma(obslen,N),PF11(dm1,N),PF12(dm2,N),PF1(dm2,dm2),
     !	 PF2(dm1,dm2),PF3(dm1,obslen),R(obslen,obslen),
     !	 KG1(obslen,dm2),Rec_Gam,
     !	 KG2(obslen,obslen),KG3(obslen,obslen),KG4(dm1,obslen),
     !	 KG(dm1,obslen),ann(dm1,N),obs_mat(obslen,N),
     !	 KG5(obslen,obslen),HT(dm2,obslen),dpr(obslen,N),
     !	 dpr1(obslen,N),t,weit,smooth
	real TPF11(N,dm1),TPF12(N,dm2),TGamma(N,obslen),SF
	integer iHours,iMins,iSecs,i100th,n_ran,flag
      double precision Ran
      real t_check
      
      real PBias(obslen,obslen), InflateFac, BiasRec(obslen,N)
      real BiasRecT(N,obslen), InflateFacAll(obslen*obslen)

	dimension Node(obslen),data1(dm1,N),data2(dm2,N),data3(dm1,N)

************Setting the observation error******************
	!weit=abs(0.05*sum(data2(1,:))/N)
      weit=0.2
      U=0
      n_ran=1
      !smooth=0.2
      !if (flag.eq.1) smooth=0.2
      SF= smooth
      if (flag.eq.0) SF=1
      !if (t-floor(t)<0.5 .and. flag.eq.1) smooth=0.05
      t_check = 190
***********Generating observation operator***************
	do i=1,obslen
		do j=1,dm2
			if(j.eq.Node(i)) then
				H(i,j)=1
			else
				H(i,j)=0
			end if
			HT(j,i)=H(i,j)
		end do
          !write(205,111) H(i,:)
	end do
      
      call BRMUL(H,data2,obslen,dm2,N,dpr1)
      
***********Generating observation data******************		
	do i=1,obslen
		Ran=t+data2(1,1)+n_ran
          
          call NGRNS(U,weit,Ran,N,Gam)
          
         do j=1,N
			Gamma(i,j)=Gam(1,j)
			obs_mat(i,j)=obs_val(i)+Gamma(i,j)
			TGamma(j,i)=Gamma(i,j)  
              n_ran=n_ran+1
              BiasRec(i,j) = obs_val(i) - dpr1(i,j)
              BiasRecT(j,i) = BiasRec(i,j)
       end do   
       
           if(t.gt.t_check) then
               write(205,111) Gamma(i,:)
           end if
           
	end do
      
       call BRMUL(BiasRec,BiasRecT,obslen,N,obslen,PBias)
***********Calculating coverances**********************
	do i=1,dm1
          do j=1,N
			PF11(i,j)=data1(i,j)-sum(data1(i,:))/N
			TPF11(j,i)=PF11(i,j)
		end do
	end do
      
        do i=1,dm2
		do j=1,N
			PF12(i,j)=(data2(i,j)-sum(data2(i,:))/N)
			TPF12(j,i)=PF12(i,j)
		end do
	 end do
      
      
	call BRMUL(PF12,TPF12,dm2,N,dm2,PF1)
	call BRMUL(PF11,TPF12,dm1,N,dm2,PF2)
      call BRMUL(Gamma,TGamma,obslen,N,obslen,R)

	do i=1,dm2
		do j=1,dm2
			PF1(i,j)=PF1(i,j)/N
		end do
	end do
      
        do i=1,dm1
		do j=1,dm2
			PF2(i,j)=PF2(i,j)/N
         end do
	 end do

      do i=1,obslen
          do j=1,obslen
			R(i,j)=R(i,j)/N
		 end do
      end do
!**********Calculating the Kalman Gain******************
	call BRMUL(H,PF1,obslen,dm2,dm2,KG1)
	call BRMUL(KG1,HT,obslen,dm2,obslen,KG2)
      
      n_ran=1
      do i=1,obslen
          do j=1,obslen
              InflateFacAll(n_ran)= PBias(i,j)/(N-1)/KG2(i,j)
              n_ran=n_ran+1
            end do
        end do
        InflateFac = maxval(InflateFacAll)      
        InflateFac = max(InflateFac,1.1)
        
        !InflateFac=1
        
	do i=1,obslen
		do j=1,obslen
			KG3(i,j)=KG2(i,j)*InflateFac+R(i,j)
              KG2(i,j)=KG3(i,j)
           end do
      end do
		
	call inverse(KG2,KG3,obslen)

	call BRMUL(PF2,HT,dm1,dm2,obslen,PF3)
      
      do i=1,dm1
          do j=1,obslen
              PF3(i,j) = PF3(i,j) * InflateFac**0.5
            end do
        end do
	call BRMUL(PF3,KG3,dm1,obslen,obslen,KG)
      
      do i=1,obslen
           write(201,111) t, obs_val(i), InflateFac, dpr1(i,:)
          !write(201,111) t, obs_val(i), PF3(:,i)
      end do
	
******************************************************
	!call BRMUL(H,data2,obslen,dm2,N,dpr1)
	


	do i=1,obslen
		do j=1,N
			dpr(i,j)=obs_mat(i,j)-dpr1(i,j)
		end do
	end do

*********************************************************
	call BRMUL(KG,dpr,dm1,obslen,N,ann)

	mean=sum(data1(1,:))/N
      
      if(t.gt.t_check) then
	!write(200,*) t
      write(205,*) t
	write(205,*) obs_val
      write(205,*) Node
      write(205,111) BiasRec
      write(205,*) PBias
      write(205,*) InflateFacAll
      end if
      
100	   format(1x,f16.11,1x,f16.11,1x,f16.11,1x,f16.11,1x,f16.11x,
     !    f16.11,1x,f16.11,1x,f16.11,1x,f16.11,1x,f16.11)
101	   format(1x,f20.1,1x,f20.1,1x,f20.1,1x,f20.1,1x,f20.1,1x,f20.1,1x,
     !        f20.1,1x,f20.1,1x,f20.1,1x,f20.1,1x,f20.1,1x,f20.1)
          !write(201,*) t
	 do i=1,dm1

       if(t.gt.t_check) then
			write(206,111) data1(i,:)
              write(206,111) data2(i,:)
        end if
              
        
		do j=1,N
			data1(i,j)=data1(i,j)+ann(i,j)*SF
          end do
          
      if(t.gt.t_check) then
              write(206,111) data1(i,:)
         end if
         
	   end do
         if(t.gt.t_check) stop
         
      
111	   format(220E20.10,1x)

 	return
	end 
      
      
! ========================================================================
! Assimilation with varied H 
      	Subroutine AssiVary(data1,data2,dm1,dm2,obs_val,obslen,upbound,
     !				lowbound,N,Node,t,flag,smooth,Depths)

	Integer N,dm1,obslen,Node,dm2
	real upbound,lowbound,H(obslen,dm2),
     !	 obs_val(obslen),Gam(1,N),
     !	 Gamma(obslen,N),PF11(dm1,N),PF12(dm2,N),PF1(dm2,dm2),
     !	 PF2(dm1,dm2),PF3(dm1,obslen),R(obslen,obslen),
     !	 KG1(obslen,dm2),Rec_Gam,
     !	 KG2(obslen,obslen),KG3(obslen,obslen),KG4(dm1,obslen),
     !	 KG(dm1,obslen),ann(dm1,N),obs_mat(obslen,N),
     !	 KG5(obslen,obslen),HT(dm2,obslen),dpr(obslen,N),
     !	 dpr1(obslen,N),t,weit,smooth
	real TPF11(N,dm1),TPF12(N,dm2),TGamma(N,obslen),SF
	integer iHours,iMins,iSecs,i100th,n_ran
      double precision Ran
      real t_check
      
      real PBias(obslen,obslen), InflateFac, BiasRec(obslen,N)
      real BiasRecT(N,obslen), InflateFacAll(obslen*obslen)
      
      Integer A
      Real Depths(obslen,N),B
      Real Hall(obslen*N,dm2),Tdiff(dm2,N), meanT, data1Diff(dm1,N)
      Real HTdiff(obslen, N), HTdiffT(N,obslen)
	dimension Node(obslen),data1(dm1,N),data2(dm2,N),data3(dm1,N)

************Setting the observation error******************
	!weit=abs(0.05*sum(data2(1,:))/N)
      weit=0.2
      U=0
      n_ran=1
      !smooth=0.2
      !if (flag.eq.1) smooth=0.2
      SF= smooth
      if (flag.eq.0) SF=1
      if (flag.eq.1) SF=0.2
      !if (t-floor(t)<0.5 .and. flag.eq.1) smooth=0.05
      t_check = 120
***********Generating observation operator***************
        Do i = 1,N*obslen ! initialize the Hall and HallT
           Do j=1,dm2
	        Hall(i,j) =0
            End do
        End do
        

       do i=1,obslen ! Hall Checked
           do j=1,N
	          A= floor (Depths(i, j)) ! For example, 4.3 will set A to be 4
	          B= Depths(i,j) - A ! B is the weight, here is 0.3
                !write(*,*) Depths(i,j),A, B
                Hall(i+(j-1)*obslen,A+1)= B ! I will set depth 5 with a weight of 0.3
                Hall(i+(j-1)*obslen,A)= 1 - B ! I will set depth 4 with a weight of 0.7
             End do
        End do
        
      
        Do i=1,N ! Get the dpr1, which is HTf, Checked
           A = (i-1)*obslen+1
        ! Hall(A:A+2,:), is the H for the first ensemble
        ! data2(:,A), is the temperature simulations for the first ensemble
        ! dpr1(:,A), is for 1 ensemble member
           call BRMUL(Hall(A:A+obslen-1,:),data2(:,i),
     !        obslen,dm2,1,dpr1(:,i))
          end do
          
          
          
          Do i=1,dm2 ! Pf has the dimension of dm2*dm2
           meanT= sum(data2(i,:))/N
           Do j=1,N
	        Tdiff(i,j) = data2(I,j) - meanT
           end do
        end do
        
          Do i=1,dm1!
           meanT= sum(data1(i,:))/N
           Do j=1,N
	        data1Diff (i,j) = data1(I,j) - meanT
           end do
        end do
        
       do i=1,N
            A = (i-1)*obslen+1
        ! HTdiff has size of (obslen,N)
            call BRMUL(Hall(A:A+obslen-1,:),Tdiff(:,i),obslen,dm2,1, 
     !        HTdiff (:,i))
        end do
        
       do i=1, obslen
           do j=1,N
        ! HTdiffT has size of (N,obslen)
	        HTdiffT(j,i) = HTdiff(I,j)
          End do
        End do


***********Generating observation data******************		
	do i=1,obslen
		Ran=t+data2(1,1)+n_ran
          
          call NGRNS(U,weit,Ran,N,Gam)
          
         do j=1,N
			Gamma(i,j)=Gam(1,j)
			!obs_mat(i,j)=obs_val(i)+Gamma(i,j)
              obs_mat(i,j)=obs_val(i)
			TGamma(j,i)=Gamma(i,j)  
              n_ran=n_ran+1
       end do   
          if(t.gt.t_check) then
               !write(205,111) data1Diff(i,:) ! Checked
           end if
           
	end do
***********Calculating coverances**********************

      call BRMUL(Gamma,TGamma,obslen,N,obslen,R)

!**********Calculating the Kalman Gain******************
      
      call BRMUL(HTdiff, HTdiffT,obslen,N,obslen,KG2) ! Checked
      
           
	do i=1,obslen
		do j=1,obslen
			KG3(i,j)=KG2(i,j)+R(i,j)
              KG2(i,j)=KG3(i,j)
           end do
      end do
		
	call inverse(KG2,KG3,obslen)

      !call BRMUL(data1Diff,HTdiffT,dm2,N,obslen,PF3)
      
      PF3= MATMUL(data1Diff, HTdiffT) ! checked
            

      
	call BRMUL(PF3,KG3,dm1,obslen,obslen,KG)
      
      
      
	
******************************************************
	!call BRMUL(H,data2,obslen,dm2,N,dpr1)
	
	do i=1,obslen
		do j=1,N
			dpr(i,j)=obs_mat(i,j)-dpr1(i,j)
		end do
	end do

*********************************************************
	call BRMUL(KG,dpr,dm1,obslen,N,ann)

	mean=sum(data1(1,:))/N
      
      
      
100	   format(1x,f16.11,1x,f16.11,1x,f16.11,1x,f16.11,1x,f16.11x,
     !    f16.11,1x,f16.11,1x,f16.11,1x,f16.11,1x,f16.11)
101	   format(1x,f20.1,1x,f20.1,1x,f20.1,1x,f20.1,1x,f20.1,1x,f20.1,1x,
     !        f20.1,1x,f20.1,1x,f20.1,1x,f20.1,1x,f20.1,1x,f20.1)
          !write(201,*) t
	 do i=1,dm1

       if(t.gt.t_check) then
			write(205,111) data1(i,:)
        end if
              
        
		do j=1,N
			data1(i,j)=data1(i,j)+ann(i,j)*SF
              if(flag.eq.2) then
                  if (data1(i,j).gt.99) then
                      data1(i,j) = 99
                  end if
              end if
                      
          end do
          
        if(t.gt.t_check) then
			write(205,111) data1(i,:)
        end if
          
	   end do
         
         
         do i=1,dm2
               if(t.gt.t_check) then
              write(206,111) data2(i,:)
              endif
         end do
         
         
      if(t.gt.t_check) then
      write(205,*) t
	write(205,*) obs_val
      write(205,*) Node
      end if
         
         if(t.gt.t_check) stop
         
      
111	   format(220E20.10,1x)

 	return
	end 
c-------------------------------------------------------------------------------------------------------
      Subroutine PFVary(data1,data2,dm1,dm2,obs_val,obslen,upbound,
     !				lowbound,N,Node,t,flag,smooth,Depths)

	Integer N,dm1,obslen,Node,dm2
	real upbound,lowbound,H(obslen,dm2),
     !	 obs_val(obslen),Gam(1,N),
     !	 Gamma(obslen,N),PF11(dm1,N),PF12(dm2,N),PF1(dm2,dm2),
     !	 PF2(dm1,dm2),PF3(dm1,obslen),R(obslen,obslen),
     !	 KG1(obslen,dm2),Rec_Gam,
     !	 KG2(obslen,obslen),KG3(obslen,obslen),KG4(dm1,obslen),
     !	 KG(dm1,obslen),ann(dm1,N),obs_mat(obslen,N),
     !	 KG5(obslen,obslen),HT(dm2,obslen),dpr(obslen,N),
     !	 dpr1(obslen,N),t,weit,smooth
	real TPF11(N,dm1),TPF12(N,dm2),TGamma(N,obslen),SF
	integer iHours,iMins,iSecs,i100th,n_ran
      double precision Ran
      real t_check,P_cum(N)
      
       real  L(obslen,N)
      real Likelyhood(1,N),aaa, p(1,N)
      real PBias(obslen,obslen), InflateFac, BiasRec(obslen,N)
      real BiasRecT(N,obslen), InflateFacAll(obslen*obslen)
      
      Integer A
      Real Depths(obslen,N),B, var, std,scale_std
      Real Hall(obslen*N,dm2),Tdiff(dm2,N), meanT, data1Diff(dm1,N)
      Real HTdiff(obslen, N), HTdiffT(N,obslen)
	dimension Node(obslen),data1(dm1,N),data2(dm2,N),data3(dm1,N)

************Setting the observation error******************
	!weit=abs(0.05*sum(data2(1,:))/N)
      weit=0.2
      U=0
      n_ran=1
      !smooth=0.2
      !if (flag.eq.1) smooth=0.2
      SF= smooth
      if (flag.eq.0) SF=1
      if (flag.eq.2) SF=1
      if (flag.eq.1) SF=1
      !if (flag.eq.1) SF=0.2
      !if (t-floor(t)<0.5 .and. flag.eq.1) smooth=0.05
      t_check = 120
***********Generating observation operator***************
        Do i = 1,N*obslen ! initialize the Hall and HallT
           Do j=1,dm2
	        Hall(i,j) =0
            End do
        End do
        

       do i=1,obslen ! Hall Checked
           do j=1,N
	          A= floor (Depths(i, j)) ! For example, 4.3 will set A to be 4
	          B= Depths(i,j) - A ! B is the weight, here is 0.3
                !write(*,*) Depths(i,j),A, B
                Hall(i+(j-1)*obslen,A+1)= B ! I will set depth 5 with a weight of 0.3
                Hall(i+(j-1)*obslen,A)= 1 - B ! I will set depth 4 with a weight of 0.7
             End do
        End do
        
      
        Do i=1,N ! Get the dpr1, which is HTf, Checked
           A = (i-1)*obslen+1
        ! Hall(A:A+2,:), is the H for the first ensemble
        ! data2(:,A), is the temperature simulations for the first ensemble
        ! dpr1(:,A), is for 1 ensemble member
           call BRMUL(Hall(A:A+obslen-1,:),data2(:,i),
     !        obslen,dm2,1,dpr1(:,i))
          end do
          
*********************************************************

      do i = 1, obslen
          do j=1,N
              L(i,j) = (dpr1(i,j) - obs_val(i))**2
          end do
      end do
      
      do i=1,N
          Likelyhood(1,i) =  exp(-sum(L(:,i)) /2/weit)/sqrt(2*3.14*weit)
      end do
      
      do i=1,N
          p(1,i) = Likelyhood(1,i)/sum(Likelyhood)
      end do
      
      ! Resampling
      P_cum(1) =P(1,1)
      do i=2,N
          P_cum(i) = P_cum(i-1) + P(1,i)
      end do
      rec_find = 1
      do i=1,N
          aa= 0 + 0.995/100*i
          !write(*,*) a
          do j = rec_find,N
              if(P_cum(j).ge.aa) then
                  rec_find = j
                  do jj = 1, dm1
                      data3(jj,i) = data1(jj,j)
                  end do  
                  goto 99
              end if
          end do
          
99          continue
      end do
        !if(minval(data3(1,:)).lt.10) then
        !  do i=1,dm1
        !      write(205,111) data1(i,:)
        !  end do
        !  do i=1,dm1
        !      write(205,111) p_cum
        !  end do
        !  !write(205,111) obs_val
        !  do i=1,dm2
        !      write(206,111) data2(i,:)
        !  end do
        !  stop
        !  end if
              
      
111	   format(220E20.10,1x)
         
         

      do i=1,dm1
      		Ran=t+data2(1,1)+n_ran
          do j=1,N
              data1(i,j) = data3(i,j)*SF + (1-SF)* data1(i,j) 
          end do
      end do
      
          if(flag.eq.2) then
              do i=1,dm1
                call variance(var,data1(i,:),N)
                std =sqrt(var)
                scale_std = max(0.2- std,0.0)
                !write(*,*) scale_std,std
                call NGRNS(U,1.0,Ran,N,Gam)
                !call NGRNS(U,std*0.2,Ran,N,Gam)
                !scale_std=1
                do j=1,N
                  data1(i,j) = data1(i,j) + Gam(1,j)*scale_std
               end do
              end do     
           end if
           !
           !   if(flag.eq.1) then
           !   do i=1,dm1
           !     call variance(var,data1(i,:),N)
           !     std = (var**0.5)
           !     scale_std = max(0.03- std,0.0)
           !     call NGRNS(U,1.0,Ran,N,Gam)
           !     !write(*,*) scale_std,std
           !     !call NGRNS(U,std,Ran,N,Gam)
           !     do j=1,N
           !       data1(i,j) = data1(i,j) + Gam(1,j)*scale_std
           !    end do
           !   end do     
           !end if

 	return
	end 
c********Random number generator********************************
      Subroutine Partiflter(data1,data2,dm1,dm2,obs_val,obslen,N,
     !        Node,t,flag,smooth)	
      Integer N,dm1,obslen,Node,dm2
	integer iHours,iMins,iSecs,i100th,n_ran, rec_find
      double precision Ran
      real weit, L(obslen,N),H(obslen,dm2),dpr1(obslen,N)
      real Likelyhood(1,N), a, p(1,N),smooth
      real P_cum(N),obs_val(obslen)
      dimension Node(obslen),data1(dm1,N),data2(dm2,N),data3(dm1,N)
      
      SF=smooth
      if (flag.eq.0) SF=1
      weit =0.2**2
      
      
      do i=1,obslen
		do j=1,dm2
			if(j.eq.Node(i)) then
				H(i,j)=1
			else
				H(i,j)=0
			end if
		end do
	end do
      call BRMUL(H,data2,obslen,dm2,N,dpr1)
      
      do i = 1, obslen
          do j=1,N
              L(i,j) = (dpr1(i,j) - obs_val(i))**2
          end do
      end do
      

      
      do i=1,N
          a=sum(L(:,i))   
          Likelyhood(1,i) =  exp(-a/2/weit)/sqrt(2*3.14*weit)
      end do
      
      do i=1,N
          p(1,i) = Likelyhood(1,i)/sum(Likelyhood)
      end do
      
      ! Resampling
      P_cum(1) =P(1,1)
      do i=2,N
          P_cum(i) = P_cum(i-1) + P(1,i)
      end do
      rec_find = 1
      do i=1,N
          a= 0 + 1.0/100*i
          !write(*,*) a
          do j = rec_find,N
              if(P_cum(j).ge.a) then
                  rec_find = j
                  do jj = 1, dm1
                      data3(jj,i) = data1(jj,j)
                  end do  
                  goto 99
              end if
          end do
          
99          continue
      end do
      
      !do i=1,obslen
      !        write(201,111) dpr1(i,:)
      !        write(201,111) data1(i,:)
      !        write(201,111) data3(i,:)
      !        write(201,111) p
      !         write(201,111) P_cum
      !end do
      !write(*,*) obs_val
              
      !continue
111	   format(220E20.10,1x)
         
      do i=1,dm1
      do j=1,N
          data1(i,j) = data3(i,j)*SF + (1-SF)* data1(i,j)
      end do
      end do
              
      
      
      end      

C------------------------------------------------
	SUBROUTINE NGRNS(U,G,R,N,A)
      
	double precision R,S,W,V,T
      
	DIMENSION A(N)
	S=65536.0
	W=2053.0
	V=13849.0
      
	DO 20 J=1,N
	  T=0.0
	  DO 10 I=1,12
	    R=W*R+V
	    M=R/S
	    R=R-M*S
	    T=T+R/S
10	  CONTINUE
	  A(J)=U+G*(T-6.0)
20	CONTINUE
	 RETURN
	 END

c********Production of the matrix************************************************

      SUBROUTINE BRMUL(A,B,M,N,K,C)
	INTEGER M,N,K
	DIMENSION A(M,N),B(N,K),C(M,K)
c	REAL A,B,C
	DO 50 I=1,M
	DO 50 J=1,K
	  C(I,J)=0.0
	  DO 10 L=1,N
	    C(I,J)=C(I,J)+A(I,L)*B(L,J)
10	   CONTINUE
50    CONTINUE
	 RETURN
	 END

c********Inverse of the matrix***********************************************

	 subroutine inverse(a,c,n)
!===========================================================
	implicit none 
	integer n
	 real a(n,n), c(n,n)
	 real L(n,n), U(n,n), b(n), d(n), x(n)
	 real coeff
	 integer i, j, k

! step 0: initialization for matrices L and U and b
! Fortran 90/95 aloows such operations on matrices
	L=0.0
	U=0.0
	b=0.0

	! step 1: forward elimination
	do k=1, n-1
	   do i=k+1,n
	      coeff=a(i,k)/a(k,k)
	      L(i,k) = coeff
          do j=k+1,n
	         a(i,j) = a(i,j)-coeff*a(k,j)
           end do
         end do
       end do

	do i=1,n
	  L(i,i) = 1.0
	 end do
	! U matrix is the upper triangular part of A
	do j=1,n
	   do i=1,j
	    U(i,j) = a(i,j)
         end do
	 end do

      do k=1,n
        b(k)=1.0
	  d(1) = b(1)
	! Step 3a: Solve Ld=b using the forward substitution
	  do i=2,n
	    d(i)=b(i)
         do j=1,i-1
	      d(i) = d(i) - L(i,j)*d(j)
         end do
	   end do
	! Step 3b: Solve Ux=d using the back substitution
	  x(n)=d(n)/U(n,n)
	  do i = n-1,1,-1
	    x(i) = d(i)
	    do j=n,i+1,-1
	      x(i)=x(i)-U(i,j)*x(j)
	    end do
	    x(i) = x(i)/u(i,i)
	  end do
	! Step 3c: fill the solutions x(n) into column k of C
        do i=1,n
	    c(i,k) = x(i)
	  end do
	  b(k)=0.0
	end do
	end subroutine inverse
c***************************************************************
	 subroutine variance(var,data1,N)

      Integer N
	real var,meand1
	dimension data1(1,N),rec_data1(1,N)

	meand1=sum(data1)/N

      do i=1,N
		rec_data1(1,i)=(data1(1,i)-meand1)*(data1(1,i)-meand1)
      end do
	var=sum(rec_data1)/N

      return
	end

**************************************************************************
	subroutine Addrand(data1,M,N,rate)
      Integer M,N
	Real iHours,iMins,iSecs,i100th,rate,Gam(1,N),mean
      Real var,t,rate2
      double precision Ran
	dimension data1(M,N),R(M,N),weight(M,N),rec_mean(1,N)
      
      
      U=0
	
	do i=1,M
		call gettim(iHours,iMins,iSecs,i100th)	
		Ran=iSecs+i*10
          

              !call NGRNS(U,1.0,Ran,N,Gam)
          call RANDOM_NUMBER(Gam)
              
		    do j=1,N  
    
			    data1(i,j)=data1(i,j)+(Gam(1,j)-0.5)*rate
		    end do
             
	end do

      return
	end