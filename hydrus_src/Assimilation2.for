	subroutine Assi2(data1,data2,dm1,obs_val,obslen,N,Node)
****************************************************************************************
c	I think I'm probably wrong with some understandings of the assimialtion.
c	As shown in Assimilation.for code, I update moisture and temperature respectively.
c	However, after some talk with Susan, and re-read the paper, I found I should put both
c	moisture and temperature into state vectors, and update it at the same time.
c	Here, I'm coding this, to see what's the difference.
****************************************************************************************
c*****************************************************************************	

c      data1 (dm1*N) is the modeled data going to be updated
c      data2 (dm1*N) is the modeled data with the same type with obseravtions
c      obs_val are the observed values
c	 obslen is the number of observed layers
c      N is the size of ensembles
c      Node is the depth of observation points

	Integer N,dm1,obslen,Node,dmm
	Real Ran,H(obslen,dm1*2),
     !	 obs_val(obslen),Gam(1,N),
     !	 Gamma(obslen,N),PF11(dm1*2,N),PF1(dm1*2,dm1*2),
     !	 PF3(dm1*2,obslen),R(obslen,obslen),
     !	 KG1(obslen,dm1*2),Rec_Gam,
     !	 KG2(obslen,obslen),KG3(obslen,obslen),
     !	 KG(dm1*2,obslen),ann(dm1*2,N),obs_mat(obslen,N),
     !	 KG5(obslen,obslen),HT(dm1*2,obslen),dpr(obslen,N),
     !	 dpr1(obslen,N),weit
	Real TPF11(N,dm1*2),TGamma(N,obslen)
	integer iHours,iMins,iSecs,i100th

	dimension Node(obslen),data1(dm1,N),data2(dm1,N),data3(dm1*2,N)

************Setting the observation error******************
	weit=1
c	if(maxval(obs_val).lt.1) weit=0.06
***********Put all the thing into state vector***************
	dmm=dm1*2
	do i=1,dmm
		if(i.le.dm1) data3(i,:)=data1(i,:)
		if(i.gt.dm1) data3(i,:)=data2(i-dm1,:)
	end do
***********Generating observation operator***************		
	do i=1,obslen
		do j=1,dm1
			H(i,j)=0
			HT(j,i)=H(i,j)
		end do
		do j=dm1+1,dmm		
			if((j-dm1).eq.Node(i)) then
				H(i,j)=1
			else
				H(i,j)=0
			end if
			HT(j,i)=H(i,j)
		end do
	end do
***********Generating observation data******************		
	do i=1,obslen
		call gettim(iHours,iMins,iSecs,i100th)	
		Ran=iSecs+i*10
		call GenRandom(Gam,N,Ran)
		Rec_Gam=sum(Gam)/N
		do j=1,N
			Gam(1,j)=Gam(1,j)-Rec_Gam
		end do
		do j=1,N
			Gamma(i,j)=Gam(1,j)*weit
			obs_mat(i,j)=obs_val(i)+Gamma(i,j)
			TGamma(j,i)=Gamma(i,j)
		end do
	end do
***********Calculating coverances**********************
	do i=1,dmm
		do j=1,N
			PF11(i,j)=data3(i,j)-sum(data3(i,:))/N
			TPF11(j,i)=PF11(i,j)
		end do
	end do
	call BRMUL(PF11,TPF11,dmm,N,dmm,PF1)
	call BRMUL(Gamma,TGamma,obslen,N,obslen,R)

	do i=1,dmm
		do j=1,dmm
			PF1(i,j)=PF1(i,j)/N
		end do
	end do

	do i=1,obslen
		do j=1,obslen
			R(i,j)=R(i,j)/N
		end do
	end do
**********Calculating the Kalman Gain******************
	call BRMUL(H,PF1,obslen,dmm,dmm,KG1)
	call BRMUL(KG1,HT,obslen,dmm,obslen,KG2)
	do i=1,obslen
		do j=1,obslen
			KG3(i,j)=KG2(i,j)+R(i,j)
			KG2(i,j)=KG3(i,j)
		end do
	end do
		
	call inverse(KG2,KG3,obslen)

	call BRMUL(PF1,HT,dmm,dmm,obslen,PF3)
	call BRMUL(PF3,KG3,dmm,obslen,obslen,KG)

c	call BRMUL(KG2,KG3,obslen,obslen,obslen,KG5)

	
******************************************************
	call BRMUL(H,data3,obslen,dmm,N,dpr1)
	

	do i=1,obslen
		do j=1,N
			dpr(i,j)=obs_mat(i,j)-dpr1(i,j)
		end do
	end do


*********************************************************
	call BRMUL(KG,dpr,dmm,obslen,N,ann)

	do i=1,dmm
		do j=1,N
			data3(i,j)=data3(i,j)+ann(i,j)
		end do
	end do

	do i=1,dm1
		do j=1,N
			data1(i,j)=data3(i,j)
		end do
	end do

	do i=dm1+1,dmm
		do j=1,N
			data2(i-dm1,j)=data3(i,j)
		end do
	end do

	return
	end 
