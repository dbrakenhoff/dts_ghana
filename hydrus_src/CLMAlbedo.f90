    subroutine AlbedoCal(Latitude, longitude, day,timezone, xL, alpha_leaf, alpha_stem, tau_leaf, tau_stem, weight, &
        & fwet, mois, LAI, Iu_albedo, I_albedo, phi, Satm, Sveg, Sground, Lsun, Lsha, K, decl)
    real latitude, longitude, day, timezone, m, theta, thetaL, albedo_g(2), LAI
    real u, fai, sigma, h, Gu, u_bar
    real pi, as, aa
    real phi_1, phi_2, xL
    real alpha_leaf(2), alpha_stem(2), tau_leaf(2), tau_stem(2), weight, alpha(2), tau(2), omega(2), LAI_All ! weight is the ratio between leaf and stem
    real omega_beta(2), omega_beta0(2), beta0(2), K
    real fwet
    real b, c, d, h1, u1, u2, u3, s1,s2, p1,p2, p3, p4, d1, d2, f, aa1, aa2, h2,h3, h4, h5, h6, h7, h8, h9, h10
    real I_up_dir(2), I_up_diffu(2), I_down_dir(2), I_down_diffu(2), Iu_albedo(2), I_albedo(2)
    real mois ! the soil moisture of surface
    real eqtime, decl,time_offset, tst, ha
    
    real Satm !shortwave raditaion
    real Rvis, Rnir,alphaS, alphaS1, S_down_vis_beam
    real S_vis_b,S_nir_b,S_vis_d, S_nir_d
    real a0, a1, a2, a3, b0, b1, b2, b3, phi_diff
    
    real fsun, Lsun, Lsha, phi_sun, phi_sha, phi(2)
    
    pi = 3.1415926
    LAI_All = LAI/weight ! the sum of leaf and stem
    
    call Albedo_Ground(mois, Albedo_g) ! calculating soil albedo, which assumes the albedo for diffuse and direct light are the same
    
! ----------Calculating solar zenith, using LSM------------------------------
    !m = (day - floor(day)) * 86400
    !theta = longitude/180 * pi ! (radians, positive east of the Greenwich meridian)
    fai = Latitude/180.0 * pi
    !t = (m + (theta/2/pi)*86400)/3600
    !h = 360/24*(t-12)*pi/180
    !
    !thetaL = 2*pi*floor(day)/365 
    !sigma = 0.006918 - 0.399912*cos(thetaL) + 0.070257*sin(thetaL) - 0.006758*cos(2*thetaL)+ 0.000907*sin(2*thetaL)-0.002697*cos(3*thetaL)+0.001480*sin(3*thetaL)
    !u = sin(fai)*sin(sigma) + cos(fai)*cos(sigma)*cos(h)
    ! The LSM does not seem to be correct, I'm using NOAA one: http://www.esrl.noaa.gov/gmd/grad/solcalc/solareqns.PDF
    hour = floor((day - floor(day))*1440.0/60.0) ! hour of the day
    
    gamma = 2.0*pi/365.0*(floor(day) -1.0 + (hour-12.0)/24.0)
    eqtime = 229.18*(0.000075 + 0.001868*cos(gamma) - 0.032077*sin(gamma)-0.014615*cos(2.0*gamma) - 0.040849*sin(2.0*gamma))
    decl  = 0.006918 - 0.399912*cos(gamma) + 0.070257*sin(gamma) - 0.006758*cos(2.0*gamma)+ 0.000907*sin(2.0*gamma)-0.002697*cos(3.0*gamma)+0.001480*sin(3.0*gamma)
    time_offset = eqtime - 4.0*longitude + 60.0*timezone
    tst = hour*60.0 + (hour - floor(hour))*1440.0 + time_offset
    ha = (tst/4.0) -180.0
    
    !write(*,*) longitude, timezone
    
    u = sin(fai)*sin(decl) + cos(fai)*cos(decl)*cos(ha/180.0*pi)
    !write(*,*) u

    if(u.lt.0.001) u =0.001 ! added by Dong, for numerical stability
!--------------------------------------------------------------------------------------
    phi_1 = 0.5 - 0.633*xL - 0.33 *xL*xL
    phi_2 = 0.877*(1.0 - 2.0*phi_1)
    Gu = phi_1 + phi_2*u ! 3.3
    u_bar = 1.0/phi_2 *(1.0 - phi_1/phi_2 * log((phi_1+phi_2)/phi_1)) ! 3.4
    
    !write(*,*) phi_1, phi_2, xL, u_bar, Gu,u
    
    
    do i = 1,2
        alpha(i) = alpha_leaf(i) * weight + (1.0 - weight) * alpha_stem(i)
        tau(i) = tau_leaf(i) * weight + (1.0 - weight) * tau_stem(i)
        omega(i) = (alpha(i) + tau(i)) * (1.0 - fwet)
        omega_beta(i) =0.5*(alpha(i)  + tau(i) + (alpha(i) - tau(i))*((1.0 +xL)/2.0 )*((1.0 +xL)/2.0 )) ! 3.13, 3.14
        aa= 1.0 - u*phi_1/(u*phi_2 + Gu)*log((u*phi_1 + u*phi_2 + Gu)/(u*phi_1))
        as= omega(i)/2.0 * Gu/(u*phi_2 + Gu) * aa !3.16
        K = Gu/u
        omega_beta0(i) = (1.0 + u_bar*K)/(u_bar*K)*as ! 3.15
        beta0(i) = omega_beta0(i)/omega(i)
        
        
        b = 1.0 - omega(i) + omega_beta(i)
        c= omega_beta(i)
        d= omega(i)*u_bar*K*beta0(i)
        f= omega(i) *u_bar*K*(1.0 - beta0(i))
        h= sqrt(b*b -c*c)/u_bar
        sigma= (u_bar*K)* (u_bar*K) + c*c - b*b
        u1= b - c/albedo_g(i)
        u2= b - c* albedo_g(i)
        u3= f+ c*albedo_g(i)
        s1= exp(-1.0*min(h*LAI_All,40.0))

        !s2= exp(-K*LAI_All)
        s2= exp(-1.0*min(K*LAI_All,40.0))
        p1= b+ u_bar*h
        p2= b- u_bar*h
        p3= b+u_bar*K
        p4= b-u_bar*K
        d1= p1*(u1-u_bar*h)/s1 - p2*(u1+u_bar*h)*s1
        d2= (u2+u_bar*h)/s1 - (u2 - u_bar*h)*s1
        h1= -d*p4 - c*f
        aa1= (d - h1/sigma*p3)*(u1-u_bar*h)/s1
        aa2= p2*(d-c-h1/sigma*(u1+u_bar*K))*s2
        h2= 1.0/d1* (aa1 - aa2)
        
        aa1= (d - h1/sigma*p3)*(u1+u_bar*h)*s1
        aa2= p1*(d-c-h1/sigma*(u1+u_bar*K))*s2
        h3= -1.0/d1* (aa1 - aa2)
        
        h4= -f*p3 - c*d
        aa1= h4*(u2+u_bar*h)/sigma/s1
        aa2= (u3- h4/sigma*(u2 - u_bar*K))*s2
        h5 = -1.0/d2 * (aa1 + aa2)
        
        aa1= h4*(u2-u_bar*h)/sigma*s1
        aa2= (u3- h4/sigma*(u2 - u_bar*K))*s2
        h6 = 1.0/d2* (aa1 + aa2)
        
        h7= c*(u1-u_bar*h)/(d1*s1)
        h8= -c*(u1 + u_bar*h)*s1/d1
        h9 = (u2+u_bar*h)/(d2*s1)
        h10 = -s1*(u2-u_bar*h)/d2
        
        I_up_dir(i) = h1/sigma +h2 + h3
        !I_up_dir(i) = h1/sigma*s2 +h2*exp(-h*LAI_All) + h3*exp(h*LAI_All)
        I_up_diffu(i)= h7 + h8
        I_down_dir(i) = h4/sigma*exp(-K*LAI_All) + h5*s1 + h6/s1
        I_down_diffu(i)= h9*s1 + h10/s1
        
        !Iu_albedo(i) = max(0.0, 1.0 - I_up_dir(i) - (1.0-albedo_g(i))*I_down_diffu(i) - (1.0-albedo_g(i))*exp(-1.0*K*LAI_All))
        !I_albedo(i) = max(0.0, 1.0 - I_up_diffu(i) - (1.0 -albedo_g(i))*I_down_diffu(i))
        
        Iu_albedo(i) =  1.0 - I_up_dir(i) - (1.0-albedo_g(i))*I_down_dir(i) - (1.0-albedo_g(i))*exp(-1.0*K*LAI_All)
        I_albedo(i) = 1.0 - I_up_diffu(i) - (1.0 -albedo_g(i))*I_down_diffu(i)
            
    end do
    !write(370,100)  day, Iu_albedo, I_albedo, I_up_dir, I_up_diffu
    
    !-----------------------------------------------------------------------------------------------------
    ! Calculate the radiation
    !-----------------------------------------------------------------------------------------------------
    
    alphaS = 0.4 ! page 400, offline model, 17.6
    alphaS1 = 1.0- alphaS
    
    a0 = 0.17639
    a1 = 0.00380
    a2= -9.0039D-6
    a3 = 8.1351D-9
    b0 = 0.29548
    b1= 0.00504
    b2= -1.4957D-5
    b3= 1.4881D-8
    Rvis = a0 + a1*alphaS*Satm + a2*(alphaS*Satm)*(alphaS*Satm)+ a3*(alphaS*Satm)*(alphaS*Satm)*(alphaS*Satm)
    
    
    if(Rvis.gt.0.99) Rvis = 0.99
    if(Rvis.lt.0.01) Rvis = 0.01
    
    Rnir = b0 + b1*alphaS1*Satm + b2*(alphaS1*Satm)*(alphaS1*Satm)+ b3*(alphaS1*Satm)*(alphaS1*Satm)*(alphaS1*Satm)
    
    if(Rnir.gt.0.99) Rnir = 0.99
    if(Rnir.lt.0.01) Rnir = 0.01
    
    S_vis_b = Rvis*alphaS *Satm
    S_nir_b = Rnir*alphaS1*Satm
    S_vis_d = (1 - Rvis)*alphaS *Satm
    S_nir_d = (1 - Rnir)*alphaS1 *Satm
    
    
    Sveg = Iu_albedo(1) * S_vis_b  + S_nir_b*Iu_albedo(2) + I_albedo(1)*S_vis_d + I_albedo(2) * S_nir_d
    
    Sground = S_vis_b *exp(-K*LAI_All)*(1.0 - albedo_g(1)) + (S_vis_b*I_down_dir(1) + &
     & S_vis_d*I_down_diffu(1))*(1.0 - albedo_g(1)) + S_nir_b *exp(-K*LAI_All)*(1.0 - albedo_g(2)) &
     & + (S_nir_b*I_down_dir(2) + S_nir_d*I_down_diffu(2))*(1.0 - albedo_g(2))
    
     
    !---Calculate SunLit area------------------------------------------------------------------------
    fsun= (1.0 - exp(-K*LAI))/(K*LAI) ! Page 55 - 56
    if(fsun.lt.0.0001) fsun = 0.0001
    phi_u_dir= S_vis_b * (1 - exp(-K*LAI_All)) * (1- omega(1)) ! 4.9
    phi_u_diff= S_vis_b * Iu_albedo(1) - phi_u_dir ! 4.10
    if(phi_u_diff.lt.0) phi_u_diff = 0
    Lsun = LAI * fsun
    Lsha = LAI * (1 - fsun)
    
    if(u.lt.0.005) then
        Lsun = LAI
        Lsha = 0
    endif
    
    phi_diff = S_vis_d*I_albedo(1) ! 4.11
    
    !phi_sun= (phi_u_dir + phi_u_diff*fsun + phi_diff*fsun) * weight/Lsun
    !phi_sha= (phi_u_diff*(1-fsun) + phi_diff*(1-fsun)) * weight/Lsha ! 4.12
    
    !=======check LSM P 42===============================
    phi_sun = ( S_vis_b*Iu_albedo(1) + fsun*S_vis_d*I_albedo(1)) * weight/Lsun
    phi_sha = (1 - fsun)* fsun*S_vis_d*I_albedo(1) * weight/Lsha
    
    if(Lsun.lt.0.01) phi_sun=0
    if(Lsha.lt.0.01) phi_sha= 0

    
    
    phi(1) = phi_sun
    phi(2) = phi_sha
    
100      Format(E20.10,1x,E20.10,1x,E20.10,1x,E20.10,1x,E20.10,1x,E20.10,E20.10,1x,E20.10,1x,E20.10,1x,E20.10,E20.10,1x,E20.10,1x,E20.10,E20.10,1x,E20.10,1x,E20.10)
    end
    
    !---------------------------------------------------------------------------------------------------------------------------------
    subroutine Albedo_Ground(mois, Albedo_g)
    real Albedo_g(2)
    real mois, delta, Albedo_dry(2), Albedo_sat(2)
    
    ! Refer to CLM page 38, I'm only using Class 10 here
    delta = max(0.0, 0.11 - 0.4*mois)
    Albedo_dry(1) = 0.18! visible light
    Albedo_sat(1) = 0.09
    Albedo_dry(2) = 0.29 ! near infrared
    Albedo_sat(2) = 0.19
    !Albedo_dry(2) = Albedo_dry(1)
    !Albedo_sat(2) = Albedo_sat(1)
    
    
    do i=1,2
        Albedo_g(i) = Albedo_sat(i) + delta
        if(Albedo_g(i).gt.Albedo_dry(i)) Albedo_g(i) = Albedo_dry(i)
    end do
    
 continue
    end
    
    !-----------------------------------------------------------------------------------------------------------------------------------
    subroutine CLM_inputs(fileNum, Latitude, longitude, timezone, xL, alpha_leaf, &
     & alpha_stem, tau_leaf, tau_stem, LAI, weight, Zatmm, zom, d, ztop, CNL, SLA0, &
     & SLAm,m_photo, alpha_wj, FN, FLNR, fai_c, fai_o, ra_photo, rb_photo,NumNP, ri,field_cap,T3Veg,vcmax)
    
    integer fileNum, NumNP, T3Veg
    real Latitude, longitude, timezone, xL, alpha_leaf(2), alpha_stem(2), tau_leaf(2), tau_stem(2), LAI
    real weight, Zatmm, zom, d, ztop, CNL, SLA0
    real SLAm,m_photo, alpha_wj, FN, FLNR, fai_c, fai_o, ra_photo, rb_photo
    real ri(NumNP), a(NumNP),field_cap, vcmax
    
    read(fileNum,*)
    read(fileNum,*)
    read(fileNum,*)
    read(fileNum,*)
    read(fileNum,*) Latitude, longitude, timezone
    read(fileNum,*)
    read(fileNum,*) xL, alpha_leaf, alpha_stem
    read(fileNum,*)
    read(fileNum,*) tau_leaf, tau_stem
    read(fileNum,*)
    read(fileNum,*) LAI, weight,T3Veg,vcmax
    read(fileNum,*)
    read(fileNum,*)
    read(fileNum,*) Zatmm
    read(fileNum,*)
    read(fileNum,*) zom, d, ztop
    read(fileNum,*)
    read(fileNum,*) CNL, SLA0, SLAm,m_photo, alpha_wj, FN, FLNR, fai_c, fai_o,field_cap
    read(fileNum,*)
    read(fileNum,*) ra_photo, rb_photo


    do i = 1,NumNP
        !a(i) = 0.5*(exp(-ra_photo*real(i - 1)/100.0) + exp(-rb_photo*real(i - 1)/100.0) - exp(-ra_photo*real(i )/100.0) - exp(-ra_photo*real(i )/100.0) )
        !a(i) = 1 - 0.5*(exp(-ra_photo*real(i - 1)/100.0) + exp(-rb_photo*real(i - 1)/100.0)) ! cumulative root fraction distribution
        !a(i) = 1 - 0.96**real(i) ! cumulative root density, LSM page 10
        a(i) = 1 - (1.0 - rb_photo/100.0)**real(i) 
    end do
    !ri(1) = a(1)
    do i = 1,NumNP-1
        ri(i) = a(i+1) - a(i)
        !ri(i) = a(i)/sum(a)
        !write(*,*) ri(i), i
    end do

    ri(NumNP) = 0.0
    
    !write(*,*) sum(ri)
    
        
    continue
    
     end
!=========================================================================
subroutine calculateUstar(eta, L, zatmm, zom, d, Va, u_star)
real eta_m, eta, L
real zom, xx, fai_fun1, fai_fun2,x
real a, d

eta_m = -1.574
if(eta.lt.eta_m) then
    x = (1 - 16*eta_m)**0.25
    fai_fun1 = 2.0*log((1.0+x)/2) + log((1.0+x*x)/2) - 2/atan(x) + 3.1415926/2
    x = (1 - 16*zom/L)**0.25
    fai_fun2= 2.0*log((1.0+x)/2) + log((1.0+x*x)/2) - 2/atan(x) + 3.1415926/2
    a = 1.0/0.41*(log(eta_m*L/zom) - fai_fun1 + 1.14*((-eta)**0.333 - (-eta_m)**0.333) + fai_fun2)
elseif(eta_m.lt.eta.and.eta.lt.0) then
    x = (1 - 16*eta)**0.25
    fai_fun1 =2.0*log((1.0+x)/2) + log((1.0+x*x)/2) - 2/atan(x) + 3.1415926/2
    x = (1 - 16*zom/L)**0.25
    fai_fun2=2.0*log((1.0+x)/2) + log((1.0+x*x)/2) - 2/atan(x) + 3.1415926/2
    a = 1.0/0.41*(log((zatmm-d)/zom) - fai_fun1 + fai_fun2)
elseif(eta.ge.0.and.eta.le.1) then
    a = 1.0/0.41*(log((zatmm-d)/zom) + 5.0*eta - 5.0*zom/L)
else
    a = 1.0/0.41*(log(L/zom)+5.0 + 5.0*log(eta) + eta -1.0 -5.0*zom/L)
endif

u_star = Va/a
continue

end

!==========================================================
subroutine LAI_calculator(LAI, t)
real LAI, t
real LAITime(5), time_series(5)

!LAITime(1) = 1.0
LAITime(1) = 0.5
LAITime(2) = 1.0
LAITime(3) = 2.1
LAITime(4) = 3.2
LAITime(5) = 4.0

!time_series(1) = 85
time_series(1) = 108
time_series(2) = 118
time_series(3) = 128
time_series(4) = 138
time_series(5) = 148

if(t.lt.time_series(1)) then
    LAI = 0.0
elseif(t.ge.time_series(5)) then
    LAI = 4.0
else
    do i=1,4
        if(t.ge.time_series(i).and.t.lt.time_series(i+1)) then
            LAI = LAITime(i) + (t - time_series(i))/(time_series(i+1) - time_series(i))*(LAITime(i+1)-LAITime(i))
        endif
    enddo
endif

end
    
        
    










    
    
     