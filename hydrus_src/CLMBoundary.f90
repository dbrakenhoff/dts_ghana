   

!-----------------------------------------------------------------------------------------------------
subroutine CanopyWater(R_preci,LAI,dt,Wcan,Evpot,weight, fwet ,fdry,WcanNew, qdrip_unit, qthru_unit)
! R: Rain, cm/min
! dt should be converted into seconds, remember to check
real R_preci,LAI,dt,Wcan,Evpot,weight
real p, qrain, qinter, qthru
real Wcan_max
real fwet, fdry,WcanNew, qdrip_unit, qthru_unit

if(Evpot.lt.0) Evpot = 0 ! added by J.Dong, it is not in CLM
p = 1.0 ! kgm^-2 ! CLM was using 0.1
! m/s * Kg/m^3 = Kg m^-2 s^-1
qrain = R_preci*1000 ! from m/s to Kg m^-2 s^-1, CHECK!!!
qinter = 0.25 * qrain * (1.0 - exp(-0.5*LAI/weight)) ! Kg m^-2 s^-1
qthru = qrain - qinter
Wcan_max = 0.1*LAI/weight
Wcan_inter = Wcan + qinter*dt ! Check the units of dt
qdrip = max(0.0,(Wcan_inter - Wcan_max)/dt) ! 7.5;
WcanNew= max(0.0,Wcan_inter - qdrip*dt - Evpot*dt)

fwet = min(0.99,(WcanNew/(p*LAI))**(0.667))


fdry = (1.0-fwet)*LAI*weight/LAI


! Kg m^-2 s^-1 Kg^-1 m^3 = m/s
qdrip_unit = qdrip/1000
qthru_unit = qthru/1000


end



!----------------------------------------------------------------------------------
subroutine Saturated_vaporPressure(Tv_K, et_sat, d_et_sat)
double precision a_coff(9), b_coff(9)
real Tv, Tv_K, et_sat, d_et_sat


a_coff(1) = 6.11213476; a_coff(2) = 4.44007856D-1; a_coff(3)= 1.43064234D-2; 
a_coff(4) = 2.64461437D-4; a_coff(5)= 3.05903558D-6; 
a_coff(6) = 1.96237241D-8; a_coff(7)= 8.92344772D-11; 
a_coff(8) = -3.73208410D-13; a_coff(9) = 2.09339997D-16;
    
    
b_coff(1)=4.44017302D-1; b_coff(2) = 2.86064092D-2;  b_coff(3) = 7.94683137D-4; 
b_coff(4) = 1.21211669D-5; b_coff(5) =1.03354611D-7; b_coff(6) = 4.04125005D-10; 
b_coff(7) = -7.88037859D-13; b_coff(8) = -1.14596802D-14; b_coff(9) = 3.81294516D-17

Tv = Tv_K - 273.15
et_sat = 0.0
d_et_sat = 0.0
do i = 1,9
    et_sat = Tv**(real(i) - 1.0)*a_coff(i)*100.0 + et_sat ! The Tv used here is in oC, CHECK!!!
    d_et_sat =  Tv**(real(i) - 1.0)*b_coff(i) *100.0 + d_et_sat
end do

end

  
    

!!*************************************************************************
subroutine PhotosynthesisCal_LSM(CNL, SLA0, SLAm, LAI, K, Lsun,Lsha, FN, FLNR, Tv, decl, Latitude, ra, rb, rb_w,Hnew, & 
    & N, ri, fai_c, fai_o, Patm, alpha_wj, phi, qs, m_photo, rs, beta_t,xconv,w_root,thr, ths, mois,Tair,Assi,Tg,Respiration,T3Veg,vcmax25)
! CNL, SLA0, SLAm, FN, FLNR are in table 8.1
! ra, rb are in table 8.3
! ri(N) is the fractions of the root, fai_c and fai_o are in Table 8.1
! alpha_wj is in Table 8.1, it is either 0.6 or 0.4
! phi is from AlbedoCal
! m_photo in Table 8.1
integer N ! the number of the nodes
integer T3Veg
real ca, CNL, SLA0, SLAm, SLAx, LAI, c, m_photo,Tair,rb_w ! rb_w is the bulk resistance for the water vapor transfer
real Lsun, SLAsun, Lsha, SLAsha
real Nasun, Nasha, alphaR25, FNR, FN, FLNR
real vcmax25, Tf, Rgas, fTv, decl, Latitude, Lat
real decl_max, DYL_max, f_DYL, Patm
real Hnew(N), w(N), ri(N), beta_t, Vcmax
real oi, ci(2), wc(2), wj(2), we(2), alpha_wj, phi(2), A(2)
real ei, Tv, cs(2), ea_
real b_p, coffe_1, coffe_2, coffe_3, rb, rs(2)
real fai_c, fai_o
real K,xconv,w_root(N),thr, ths, mois(N)
real convert_uni
real T_star, Tg
real power_alpha, Kc, K0
real Rf25, Rs25, Rr25, alpha_rm, a3, vb_s, vb_r
real Rm, Rss, Rg, mois_a, Respiration(3), Assi(2)
!=====================================================
real fTv_1
Integer c3_plant
!=====================================================

c3_plant = 1 ! if it is c3, c3_plant =1 
ca = 3.3D-4 * 101*1D3! atmospheric co2 partial pressure
ci(1) = 0.4*ca ! first guess of internal leaf CO2
ci(2) = 0.4*ca ! I guess ci is for sun shine and shade area respectively

Tf=273.15 ! Table 1.4
Rgas=8.314 ! J/molK

convert_uni = 8.314D-9*Tair/101.0
rb= rb_w * convert_uni

!vcmax25 = 17.0
Ko25 = 30000.0
Kc25 = 30.0
alpha_kc = 2.1
alpha_ko = 1.2
alpha_vmax = 2.4
alpha = 0.06

do iij = 1,3


    beta_t = 0.0
    do i=1,N
         !w(i) = (fai_c - Hnew(N-i+1)*1000)/(fai_c - fai_o) ! Hnew is in mm here, I assume no freezing water here
         !w(i) = max(0.0, min(1.0, (mois(N-i+1) - 1.1*thr)/(0.25*ths - 1.1*thr)))
         w(i) = max(0.0, min(1.0, (Hnew(N-i+1) +1500000)/(1500000 - 33000)))
         !write(*,*) w(i), fai_c, Hnew(N-i+1)*1000,fai_c, fai_o
        if(w(i).gt.1.0) w(i) = 1.0
        
        !w(i) = 0.0 ! added by Dong
        beta_t = beta_t + w(i) * ri(i)

    end do
    if(beta_t.gt.0) then
        do i=1,N
            w_root(i) = w(i) * ri(i)/beta_t
        end do  
    else
        do i=1,N
            w_root(i) = 0
        end do
    end if
    
    
    fTv_1 =1.0 + exp((-220000.0 + 710.0 *(Tv - 273.16) )/(8.314*(Tv - 273.16)))

    Vcmax = vcmax25 * alpha_vmax**((Tv - 25.0 - 273.16)/10.0)/fTv_1*beta_t
    ! --------------------------------------------------------
    ! 8.2 to 8.4, I'm only doing C4 plants here
    oi = 0.209*Patm !Page 164
    ei= 0.611*exp(17.27*(Tv-273.15)/(Tv-35.85))*1000.0 ! Pa, saturation vapor pressure at leaf temperature
    
    power_alpha_kc = alpha_kc**((Tv - 273.16 -25)*0.1)
    power_alpha_k0 = alpha_ko**((Tv - 273.16 -25)*0.1)
    do i=1,2
        
        Kc = Kc25*power_alpha_kc
        K0 = Ko25*power_alpha_k0        
        T_star = 0.5* Kc/K0*0.21*oi
        
        !write(*,*) Kc, K0, T_star, power_alpha_kc, power_alpha_k0, (ci(i) - T_star)/(ci(i) + 2*T_star)
        if(T3Veg.gt.0) then ! C3 vegetation
            wc(i) = (ci(i) - T_star)*Vcmax/(ci(i) + Kc*(1.0 + oi/K0))
            wj(i) = 4.6*alpha_wj*phi(i)*(ci(i) - T_star)/(ci(i) + 2*T_star)
            we(i) = 0.5*Vcmax
        else
            wc(i) = Vcmax
            wj(i) = 4.6*phi(i)*alpha_wj
            we(i) = 4000.0*Vcmax*ci(i)/Patm
        endif
                   
            
            
        
        !write(*,*) wj(i), power_alpha_kc, power_alpha_k0
        
        A(i) = min(wc(i), we(i), wj(i))
        
          
        cs(i) = ca - 1.37*rb*Patm*A(i) ! check the units here
        ea= Patm*qs/0.622
        ea_ = max(min(ea,ei),0.25*ei)
    
        b_p= 1000;
        coffe_1 = (m_photo * A(i) * Patm * ea_/(cs(i)*ei)) + b_p
        coffe_2 = (m_photo * A(i) * Patm * rb/cs(i)) + b_p*rb - 1.0
        coffe_3= -1.0 * rb
        rs(i) = max(0.0,(-coffe_2 + sqrt(coffe_2*coffe_2 - 4.0 * coffe_1 *coffe_3))/(2.0*coffe_1))
        ci(i) = cs(i) - 1.65*rs(i)*Patm*A(i)
        
    end do
    
end do

do i=1,2
    rs(i) = rs(i) /convert_uni  ! convert the units to s/m
end do

! plant and microbial respiration
!Rf25 = 0.5; Rs25 = 0.0; Rr25 = 0.59; alpha_rm = 2.0; a3 = 0.17
Rf25 = 0.82; Rs25 = 0.0; Rr25 = 2.27; alpha_rm = 2.0; a3 = 0.17
vb_s = 0.0; vb_r = 0.3
Rm = (LAI*Rf25*1.0*beta_t + vb_s*Rs25 + vb_r*Rf25)*alpha_rm**((Tv - 273.15- 25.0)/10.0)
Rg = 0.25*(A(1) * Lsun + A(2)*Lsha)
mois_a = sum(mois)/real(N)
Rss = mois_a/(0.2+mois_a)*0.23/(0.23+mois_a)*a3*10.0*2.0**((Tg - 273.15 - 10.0)/10.0)

Respiration(1) = Rm;Respiration(2) = Rg;Respiration(3) = Rss;
Assi(1) = A(1) * Lsun; Assi(2) =  A(2)*Lsha

continue
100      Format(E20.10,1x,E20.10,1x,E20.10,1x,E20.10,1x,E20.10,1x,E20.10,E20.10,1x,E20.10,1x,E20.10,1x,E20.10)

end
    





    
        
    
        
    
    
    
    
    
    
    
    
    
        
            
    
    