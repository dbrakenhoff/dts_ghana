subroutine CLM(N, Latitude, longitude, day, xL, alpha_leaf, alpha_stem, tau_leaf, tau_stem, weight, fwet, mois, LAI, &
    & CNL, SLA0, SLAm,m_photo, alpha_wj, FN, FLNR, fai_c, fai_o, ra_photo, rb_photo, zom, d, Zatmm, Tair, Hnew, ri, xLat, &
    & ForcingUse,timezone, dt, tconv, xconv, Tg, ztop, Wcan,  rTop, Prec, Rld, EnergyTerm,RaditionTerm,field_cap,w_root,Evap1,&
    & thr,ths,ThNew,Assi,Respiration,T3Veg,vcmax,th_field)
    
    real Latitude, longitude, day, xL, alpha_leaf(2), alpha_stem(2), tau_leaf(2), tau_stem(2), weight, fwet, mois, LAI
    real Iu_albedo(2), I_albedo(2), timezone, phi_sun, phi_sha, Satm, Sveg, Sground, phi(2)
    real  CNL, SLA0, SLAm,m_photo, alpha_wj, FN, FLNR, fai_c, fai_o, ra_photo, rb_photo, zom, d, Zatmm
    real ForcingUse(5), Tref, TK_air, TK_g,  Lsun, Lsha, K, decl, Rld,w_root(N)
    character cfilename*260
    Integer N, T3Veg
    real Hnew(N), ri(N), dtseconds, EnergyTerm(6),RaditionTerm(4)
    real R_preci,dt,Wcan, qdrip_unit, qthru_unit, Tv_pre, LAI_All, ztop
    real lambda, Eground, Prec, field_cap,Evap1,thr,ths
    real ThNew(N), Assi(2), Respiration(3),vcmax,th_field
    
    lambda= xLat! J/Kg
    Patm = 101000 ! Pa
    day = ForcingUse(1)
    Satm = ForcingUse(2)

    !-------Calculate the Albedo-----------------------------------------------------------------------------------------------------
    call AlbedoCal(Latitude, longitude, day, timezone,xL, alpha_leaf, alpha_stem, tau_leaf, tau_stem, &
        &weight, fwet, mois, LAI, Iu_albedo, I_albedo, phi, Satm, Sveg, Sground,  Lsun, Lsha, K, decl)
    !-------Calculate the Albedo-----------------------------------------------------------------------------------------------------

    dtseconds = dt/tconv
    TK_g = Tg + 273.15
    LAI_All = LAI/weight
    

        R_preci = Prec / xconv*tconv ! converted to m/s. CHECK!!!

    

    ! EnergyTerm: Hv, lambda*Ev, Hg, lambda*Eg, G
    call SurfaceEnergyInovan(ForcingUse, Hnew(N), TK_g, Zatmm, d, zom,ztop, CNL, SLA0, SLAm, LAI_All,LAI, K, Lsun,Lsha, FN, FLNR,  &
    & decl,Latitude, ra_photo, rb_photo, Hnew, ri, N, fai_c, fai_o, Patm, alpha_wj, phi, m_photo, R_preci,dtseconds,Wcan, & 
    & qdrip_unit, qthru_unit,Sveg,Sground, Rld, EnergyTerm,RaditionTerm,lambda, fwet,field_cap,mois,xconv,w_root,thr,ths, &
    & ThNew,Assi,Respiration,T3Veg,vcmax,th_field)
    
    
    !--------Modify the flux of top soil---------------------------------------------------------------------------------------------
    !write(*,*) qdrip_unit, qthru_unit
    ! J s^-1 m^-2 / (J Kg^-1) = Kg m^-2 s^-1
    ! J s^-1 m^-2 / (J Kg^-1)  /( Kg m^-3) = m s^-1
    Eground = EnergyTerm(4)/lambda/ 1000.0 ! Dong: Here I assume water density is 1000 Kg/m^3
    
    if(ThNew(N).lt.1.1*thr) then 
    !if(ThNew(N).lt.0.05) then 
        Eground= 0.0
        EnergyTerm(1) = EnergyTerm(1) + EnergyTerm(2)
        EnergyTerm(2) = 0
    end if
    
    R_preci = -(qdrip_unit + qthru_unit)  + Eground ! in m/s

    !R_preci = -(qdrip_unit + qthru_unit)   ! in m/s
    
    rTop = R_preci*xconv/tconv 

    ! update the rTop
    

    !--------------------------------------------------------------------------------------------------------------------------------------
    continue
    
    
    end
    