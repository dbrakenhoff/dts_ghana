!===============================================================================================
    
    
    
    subroutine SurfaceEnergyInovan(ForcingUse, Htop, Tg, Zatmm, d, zom, ztop, CNL, SLA0, SLAm, LAI_All,LAI, K, Lsun,Lsha, FN, FLNR,  &
    & decl,Latitude, ra_photo, rb_photo, Hnew, ri, N, fai_c, fai_o, Patm, alpha_wj, phi, m_photo, R_preci,dt,Wcan, qdrip_unit, qthru_unit, &
    & Sveg,Sground, Rld, EnergyTerm,RaditionTerm,lambda, fwet,field_cap,mois,xconv,w_root,thr,ths,ThNew,Assi,Respiration,T3Veg,vcmax,th_field)
    ! ForcingUse should contain air temperature, humidity, wind speed
    ! Htop is the water pressure head of surface soil
    ! zom, and d are from tables
    !ztop
    integer N, iteration_step, T3Veg! T3Veg =1 means T3 vegetation, and 0 is T4 vegetation
    real Tair, Hr_air, uatm,uatm1, Hsur, Htop, rho_atm, error_ev,xconv
    real dtSeconds, M, R, g, xMol, Hr, rovsS, qg, qatm, eta, qg_sat
    real Ts, qs, Rib, Zatmm, d, zom, fai_h, fai_m, fai_w, x,xx, fai_m_om, fai_eta_m
    real fai_h_oh, fai_eta_h
    real eta_m, eta_h, u_star, theta_ratio, q_ratio
    real ram, rah, raw, Uav, rb, rdry_
    real Cs, Cs_bare, Cs_dense, LAI_All, ztop, S, W
    real rah_, raw_, Tg, r_s
    real CNL, SLA0, SLAm, LAI, K, Lsun,Lsha, FN, FLNR, decl, Latitude, ra_photo, rb_photo, Hnew(N), ri(N)
    real fai_c, fai_o, Patm, alpha_wj, phi(2), m_photo, rs(2)
    real cah, cgh, cvh
    real R_preci,Wcan,Evpot,weight, fwet ,fdry,WcanNew, qdrip_unit, qthru_unit
    real et_sat, d_et_sat, qT_sat, beta_t,dqT_sat
    real a1, a2, r_, Llitter, r_litter
    real caw, cvw, cgw, cgw2
    real Hv, Ev, Ev_pre, Delta1, Delta2, Delta3, lambda, Evt, Hg, Eg, Eg_r_s
    real epslion_g, epslion_s, epslion_v
    real w_Latm, epslion_a, Latm, Tv, Tv_pre ! Tv and Tv_pre are the vegetation temperature at current and previous step
    real Lup, Lv_down, Lg, Sveg, dTv, theta_star, Sground
    real Lv, thr,ths
    real eground, eground1, ForcingUse(5), L
    real EnergyTerm(6), RaditionTerm(4), Rld, Uc,field_cap,mois,w_root(N),ThNew(N)
    real absorptive_g, absorptive_v, Respiration(3)
    real uztop, rb_1, rb_, Kh_ztop, Assi(2)! assimilation rate
    
    real beta_soil,th_field
    
    !real Lv_down, Lg_up, Lg, Lv
    double precision a_coff(9), b_coff(9),sigma
    
    real zatm_wind, zow_, zoh_
    real Kh_Hv, u_Hv, vcmax
    
    !----Set up the constants----------------------------------
    M=0.018015 ! J/molK
    R=8.314 ! J/molK
    g=9.8 ! m/s^2
    xMol=0.018015
    lambda= 2.8440D6! J/Kg
    epslion_g = 0.96
    epslion_s = 0.96
    epslion_v = 1 - exp(-LAI_All)
    
    absorptive_g = 0.93
    absorptive_v = epslion_v
    
    sigma = 5.67D-8
    iteration_step = 1
    Tref = 273.15 ! all the temperatures should be in K
!===========================================
!-----Pay attention to this part---------------------------------
! some of the equations are not same with page 72
    Tair = ForcingUse(4)
    Hr_air = ForcingUse(5)/100
    uatm1= max(ForcingUse(3),1.0) ! wind speed, m/s. CLM did this

    dtSeconds= dt*60 ! in seconds, remember to check the units when it is coupled with Hydrus 
    Hsur=Htop/xconv !  into m, remember to check the units when it is coupled with Hydrus
    if(Hsur.lt.-1D-6) Hsur = -1D-6
    if(Hsur.gt.-0.1) Hsur = -0.1
    
    Hr=min(0.999, exp(Hsur*xMol*g/R/Tg))
    
    !eground = 0.611*exp(17.27*(Tg-273.15)/(Tg-35.85))*Hr*1000 ! vapor pressure of ground, Tg in K, eg in Pa
    call Saturated_vaporPressure(Tg, eground1, d_et_sat)
    eground = eground1*Hr
    qg=  0.622 * eground/(Patm-0.378*eground) ! specific humidity Kg/Kg
    qg_sat = 0.622 * eground1/(Patm-0.378*eground1) 
    
!P48 LSM
    Patm= 101000 ! I assume the air temperature to be 101 K Pa
    !eair= 0.611*exp(17.27*(Tair-273.15)/(Tair-35.85))*1000
    !eatm= eair*1000 ! % vapor pressure into Pa
    call Saturated_vaporPressure(Tair, eair, d_et_sat)
    eatm = eair * Hr_air
    rho_atm = Patm/287/(Tair + Tref)! Density of air kg/cm3

    qatm= 0.622 * eatm/(Patm-0.378*eatm) ! specific humidity Kg/Kg
    Cp = 1004.64! specific heat capacity of air (J kg^-1 K^-1), Table 1.4 in CLM4 

    !if(qg_sat.gt.qatm.and.qatm.gt.qg) qg = qatm ! Page 85 CLM4.5
!===========================================

    Ts = (Tg + Tair)/2.0 ! canopy air temperature, in K
    
    !Tv = (Tg + Tair)/2.0 ! It is not in CLM. I'm not sure!!!
    Tv = Tair - 2.0 
    
    qs = (qg + qatm)/2.0 ! specific humidity of the canopy air
    
    uatm = uatm1
    Latm = Rld*1D6/86400.0
    
    
    ! define the heights
    ztop = 0.75*LAI_All ! assumed value
    zatm_wind = zatmm + ztop 
    d = 0.67 * ztop
    zow_ = 0.005
    zoh_ = 0.005
    zom = 0.123*ztop
    zoh = 0.1*zom
    zow = 0.1*zom

99     continue ! This is where the iteration starts. Pay attention here
    !==========================================
    ! pay attention here, I'm using equations from LSM
    ! If uatm<1.0 there are some other equations for cvh csh ctw csw
    
    
    ! calculating the resistances, 5.55 to 5.57
    rah = 1.0/0.41/0.41/uatm*alog((zatm_wind - d)/zom)*alog((zatm_wind - d)/zoh)
    raw = rah
    
    u_star = 0.41*uatm/alog((zatmm - 0.1)/0.05) ! check this part
    
    Kh_Hv = u_star*0.41*(ztop - d)
    raw_ = ztop/(3.0*Kh_Hv)*(exp(3.0*(1.0 - zow_/ztop)) - exp(3.0*(1.0 - (zow+d)/ztop)))
    rah_ = raw_
    
    u_Hv = u_star/0.41*alog((ztop - d)/zom)
    ! calculating the leaf boundary layer reistance rb
    rb = 1.0/(0.02/3.0*sqrt(u_Hv/0.04)*(1.0 - exp(-3.0/2.0)))
    
    
    call PhotosynthesisCal_LSM(CNL, SLA0, SLAm, LAI, K, Lsun,Lsha, FN, FLNR, Tv, decl, Latitude, ra_photo, rb_photo, rb,&
        &Hnew, N, ri, fai_c, fai_o, Patm, alpha_wj, phi, qs, m_photo, rs, beta_t,xconv,w_root, thr, ths, ThNew,Tair,Assi,&
        &Tg,Respiration,T3Veg,vcmax)
    
    cah = 1.0/rah
    cgh = 1.0/rah_
    cvh = 2*LAI_ALL/rb ! 5.91 to 5.93
    
    weight = LAI/LAI_All
    
    call Saturated_vaporPressure(Tv, et_sat, d_et_sat)
    
    qT_sat = 0.622*et_sat/(Patm - 0.378*et_sat)
    
    dqT_sat = 0.622*Patm/(Patm - 0.378*et_sat)/(Patm - 0.378*et_sat)*d_et_sat ! equation 5.158
       
    
    Evpot = -rho_atm*(qs - qT_sat)/rb ! 5.109
    
    call CanopyWater(R_preci,LAI,dt,Wcan,Evpot,weight, fwet ,fdry,WcanNew, qdrip_unit, qthru_unit)
 
    
    caw = 1.0/raw

    
    !----------Equations from LSM--------------------------------- JDong April 29
    cvw = fwet*LAI_All/rb + (1.0 - fwet)*(1.0/(rb/Lsun+rs(1)) + 1.0/(rb/Lsha+rs(2))) ! page 60, check the rs
    !---------------------------------------------------------------------
    
    
     r_s=10.0
     if(mois.lt.0.25) r_s=10.0*exp(35.63*(0.25-mois))
     
     r_s = 0.0
     beta_soil = 0.25*(1.0 - cos(3.14159 * mois/th_field))*(1.0 - cos(3.14159 * mois/th_field))
     
    !if(mois.lt.0.15) r_s=10.0*exp(35.63*(0.15-mois))
    
    !beta_t = (mois - thr)/(0.75*ths - thr)
    !r_s = exp(6.6 - 4.255*beta_t)
    
    !write(*,*) beta_t, r_s, mois
      
     cgw = 1.0/(raw_+r_s) !equation 5.104 to 5.107, beta_soil is set to be 1

       
     
    Hv = -rho_atm*Cp*(cah*Tair + cgh*Tg - (cah+ cgh)*Tv) * cvh/(cah+cvh+cgh) ! all the T should be in K here
    Ev = -rho_atm*(caw*qatm + cgw*qg -(caw + cgw) * qT_sat)*cvw/(caw+cvw+cgw) ! 5.111

    if(iteration_step.eq.1) Ev_pre = Ev
    if(Ev_pre*Ev.lt.0.0) then
        Delta1 = 0.9*Ev*lambda
        Ev = Ev*0.1
    else
        Delta1=0.0
    endif

        
    !---------Change in Tv----------------------------
    
    

      if(iteration_step.eq.1) Tv_pre = Tv
      

    continue
    
 
    
    !  LSM Page 46: Assumes absorptivity equals emissity
    Latm = (0.7 + 5.95D-5*0.01*eatm*exp(1500/Tair))*sigma*Tair**4
    
    !write(*,*) eatm, Tair, sigma, Latm,5.95D-5*0.01*eatm*exp(1500/Tair)
    
    Lv_down = (1.0 - epslion_v) * Latm + epslion_v*sigma*Tv**4.0 
    Lg_up = (1.0 - epslion_g) * Lv_down + epslion_g*sigma*Tg**4.0
    
    Lv = -1.0* epslion_v * (Latm + Lg_up) + 2.0*epslion_v*sigma*Tv**4.0 
    !Lv = (2.0 - epslion_v*(1-epslion_g))*epslion_v*Tv**4.0 - epslion_v*epslion_g*sigma*Tg**4.0 - epslion_v*(1+(1-epslion_v)*(1-epslion_g))*Latm
    Lg = -1.0*epslion_g*Lv_down + epslion_g*sigma*Tg**4.0
    
    Lup = (1 - epslion_v)*Lg_up + epslion_v*sigma*Tv**4.0
    

    a1 = Sveg - Lv - Hv - lambda*Ev
    !dLv = 4.0*epslion_v*sigma*(2.0 -epslion_v*(1.0 -epslion_g))*Tv**3.0
    dLv = 10.0 ! LSM 69
    dHv = rho_atm*Cp*(cah + cgh)*cvh/(cah+cvh+cgh)
    dEv = lambda*rho_atm*(caw+cgw)*cvw/(caw+cvw+cgw)*dqT_sat 
    
    a2 = dLv + dHv + dEv
    dTv = a1/a2
        
    
    if(dTv.gt.1.0) then
        dTv = 1.0
        Delta2 = Sveg - Lv - dLv*dTv - Hv - dHv*dTv - lamda*Ev - dEv*dTv
    elseif(dTv.lt.-1.0) then
        dTv = -1.0
        Delta2 = Sveg - Lv - dLv*dTv - Hv - dHv*dTv - lamda*Ev - dEv*dTv
    else
        Delta2 = 0.0
    end if
    Delta2 = 0.0
    
    !--------------------------------------------------------------------
    Ev = -rho_atm*(caw*qatm + cgw*qg - (caw + cgw)*(qT_sat + dqT_sat*dTv))*cvw/(caw+cvw+cgw) ! 5.134, total vapor flux
    !Evt = -rdry_*rho_atm*(caw*qatm + cgw*qg - (caw + cgw)*(qT_sat + dqT_sat*dTv))*cvh/(caw + cvw+cgw) ! transpiration
    
    !if(Ev.gt.Evt) then ! commented by JDong April 28
    !    Ev = Evt
    !    Delta3 = max(0.0, Ev-Evt-Wcan/dt) ! pay attention to the dt here
    !    !Delta3=0.0 ! added by Dong
    !else
        Delta3=0.0
    !end if
    
    Hv = - rho_atm*Cp*(cah*Tair + cgh*Tg - (cah+cgh)*(Tv + dTv)) * cvh/(cah + cvh + cgh) + Delta1 + Delta2 + Delta3*lambda
    
    Tv = Tv_pre + dTv
    ! evaluate the specific humidity and it derivative again
    call Saturated_vaporPressure(Tv, et_sat, d_et_sat)

    qT_sat = 0.622*et_sat/(Patm - 0.378*et_sat)
    
    dqT_sat = 0.622*Patm/(Patm - 0.378*et_sat)/(Patm - 0.378*et_sat)*d_et_sat ! equation 5.158
    
    Ts = (cah*Tair + cgh*Tg + cvh*Tv)/(cah+cgh+cvh) ! 5.90
    qs = (caw*qatm + cgw*qg + cvw*qT_sat)/(caw+cvw+cgw)
    
    iteration_step = iteration_step+1
    error_ev = (Ev_pre - Ev)*lambda
    
    
    if(iteration_step.lt.40) then
        if(abs(error_ev).gt.0.1.or.abs(dTv).gt.0.01) then
            Ev_pre = Ev
            Tv_pre = Tv
            goto 99
        endif
    end if
    
    Eg = -rho_atm*(caw*qatm + cvw*qT_sat -(caw+cvw)*qg)*cgw/(caw+cvw+cgw)*beta_soil ! 5.112
  
    
    Hg = -rho_atm*Cp*(cah*Tair + cvh*Tv -(cah + cvh)*Tg)*cgh/(cah+cvh+cgh)! 5.95
    
    
    
    G = Sground - Lg - Hg - lambda*Eg
    
    !write(*,*) G, Sground, Lg, 
    
    EnergyTerm(1) = Hv
    EnergyTerm(2) = Ev*lambda
    EnergyTerm(3) = Hg
    EnergyTerm(4) = Eg*lambda
    EnergyTerm(5) = G
    EnergyTerm(6) = Sveg

    RaditionTerm(1) = Sveg
    RaditionTerm(2) = Sground
    RaditionTerm(3) = -Lv
    RaditionTerm(4) = -Lg

    !dHg_Tg = Cp*rho_atm/rah_*(cah+cvh)/(cah+cvh+cgh)
    !dEg_Tg = beta_soil*rho_atm/raw_*(cah+cvh)/(cah+cvh+cgh)
    
    continue
    
    Wcan = WcanNew ! update the canopy water
    
    !write(370,100) day, rb, r_s, rs, Lsha, Lsun, fwet
    
    
100      Format(E20.10,1x,E20.10,1x,E20.10,1x,E20.10,1x,E20.10,1x,E20.10,E20.10,1x,E20.10,1x,E20.10,1x,E20.10)

end
    
  