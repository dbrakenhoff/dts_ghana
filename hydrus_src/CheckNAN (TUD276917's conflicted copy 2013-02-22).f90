subroutine checkNAN(data1,M,N)
    Integer M,N,n_rec,rec
    real iHours,iMins,iSecs,i100th,Ran
    dimension data1(M,N),no_nan(200),nan(200)
    dimension Gam(3),Int_Gam(3)
    
    n_rec=1
    rec=1
    do i=1,N
        if(data1(1,N).eq.data1(1,N)) then
            no_nan(n_rec)=i
            n_rec=n_rec+1
        else
            nan(rec)=i
            rec=rec+1
        end if
    end do
    


if(rec-1.gt.0) then
    do i=1,M
        do j=1,rec-1
        
            call gettim(iHours,iMins,iSecs,i100th)	
		    Ran=iSecs+j*10+i
		    call GenRandom(Gam,3,Ran)
        
            do kk=1,3
                Int_Gam(kk)=int(Gam(kk)*(n_rec-1))
                if ( Int_Gam(kk).lt.1)  Int_Gam(kk)=1
            end do

            data1(i,nan(j))=(data1(i,no_nan(Int_Gam(1)))+data1(i,no_nan(Int_Gam(2)))+data1(i,no_nan(Int_Gam(3))))/3
        end do
    end do
end if


end

    




