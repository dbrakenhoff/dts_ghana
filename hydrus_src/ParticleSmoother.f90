subroutine PsL(data2,dm2,obs_val,obslen,N,Depths,Lsum,dpr1)
    
	Integer N,obslen,dm2
	real upbound,lowbound,H(obslen,dm2),obs_val(obslen),Gam(1,N), Gamma(obslen,N)
    real obs_mat(obslen,N),HT(dm2,obslen),dpr(obslen,N)
    real dpr1(obslen,N),t,weit,smooth
	real TGamma(N,obslen),SF
	integer iHours,iMins,iSecs,i100th,n_ran
    double precision Ran
    real t_check,P_cum(N)
      
    real  L(obslen,N),Lsum(1,N), sumL
    real Likelyhood(1,N), p(1,N)
    real PBias(obslen,obslen), InflateFac, BiasRec(obslen,N)
    real BiasRecT(N,obslen), InflateFacAll(obslen*obslen)
      
    Integer A
    Real Depths(obslen,N),B, var, std,scale_std
    Real Hall(obslen*N,dm2),Tdiff(dm2,N), meanT
    Real HTdiff(obslen, N), HTdiffT(N,obslen)
	dimension data2(dm2,N)
    
    
    weit=0.1
       Do i = 1,N*obslen ! initialize the Hall and HallT
           Do j=1,dm2
	        Hall(i,j) =0
            End do
        End do
        
!write(*,*) Depths
       do i=1,obslen ! Hall Checked
           do j=1,N
               !write(*,*) i,j,i+(j-1)*obslen
	          A= floor (Depths(i, j)) ! For example, 4.3 will set A to be 4
	          B= Depths(i,j) - A ! B is the weight, here is 0.3
                !write(*,*) Depths(i,j),A, B
                Hall(i+(j-1)*obslen,A+1)= B ! I will set depth 5 with a weight of 0.3
                Hall(i+(j-1)*obslen,A)= 1 - B ! I will set depth 4 with a weight of 0.7
             End do
       End do
        
       
      
       
        Do i=1,N ! Get the dpr1, which is HTf, Checked
           A = (i-1)*obslen+1
        ! Hall(A:A+2,:), is the H for the first ensemble
        ! data2(:,A), is the temperature simulations for the first ensemble
        ! dpr1(:,A), is for 1 ensemble member
           call BRMUL(Hall(A:A+obslen-1,:),data2(:,i),obslen,dm2,1,dpr1(:,i))
        end do
        
        do i = 1, obslen
            call gettim(iHours,iMins,iSecs,i100th)
            Ran=iSecs+i100th
            call NGRNS(0.0,weit,Ran,N,Gam)
          do j=1,N
              L(i,j) = (dpr1(i,j) - obs_val(i) + Gam(1,j))**2
          end do
        end do
        
        do i=1,N
            Lsum(1,i) = sum(L(:,i))
        end do
        
        !sumL=sum(Lsum)
        !do i=1,N
        !    Lsum(1,i) = Lsum(1,i)/sumL
        !end do
        
      !do i=1,N
      !    Likelyhood(1,i) =  exp(-Lsum(1,i) /2/weit)/sqrt(2*3.14*weit)
      !end do
      !
      !do i=1,N
      !    Lsum(1,i) = Likelyhood(1,i)/sum(Likelyhood)
      !end do
        
        
              
end
!==============================================================================================
subroutine Resampling(data1,dm1,N,flag,smooth,Lsum)

	Integer N,dm1,Node
	real upbound,lowbound,Gam(1,N),t,weit,smooth
	real SF,Lsum(1,N)
	integer iHours,iMins,iSecs,i100th,n_ran
    double precision Ran
    real t_check,P_cum(N)
      
    real Likelyhood(1,N), p(1,N)

    Integer A
    Real B, var, std,scale_std
	dimension data1(dm1,N),data3(dm1,N)

!************Setting the observation error******************
      weit=0.1
      U=0
      n_ran=1
      SF= smooth
      if (flag.eq.0) SF=1
      if (flag.eq.2) SF=1
      if (flag.eq.1) SF=1
      t_check = 120
!***********Generating observation operator***************
      
      
      do i=1,N
          Likelyhood(1,i) =  exp(-Lsum(1,i) /2/weit)/sqrt(2*3.14*weit)
      end do
      
      do i=1,N
          p(1,i) = Likelyhood(1,i)/sum(Likelyhood)
      end do
      
      ! Resampling
      P_cum(1) =P(1,1)
      do i=2,N
          P_cum(i) = P_cum(i-1) + P(1,i)
      end do
      rec_find = 1
      do i=1,N
          aa= 0 + 0.9995/100*i
          !write(*,*) a
          do j = rec_find,N
              if(P_cum(j).ge.aa) then
                  rec_find = j
                  do jj = 1, dm1
                      data3(jj,i) = data1(jj,j)
                  end do  
                  goto 99
              end if
          end do
          
99          continue
      end do
        !if(minval(data3(1,:)).lt.10) then
          !do i=1,dm1
          !    write(205,111) data1(i,:)
          !end do
          !write(205,111) p
        !  do i=1,dm1
        !      write(205,111) p_cum
        !  end do
        !  !write(205,111) obs_val
        !  do i=1,dm2
        !      write(206,111) data2(i,:)
        !  end do
        !  stop
        !  end if
              
      
111	   format(220E20.10,1x)
         
         

      do i=1,dm1
      		Ran=t+data1(1,1)+n_ran
          do j=1,N
              !data1(i,j) = data3(i,j)*SF + (1-SF)* data1(i,j) 
              data1(i,j) = data3(i,j)
          end do
      end do
      
         !do i=1,dm1
         !     write(205,111) data3(i,:)
         ! end do
      
          if(flag.eq.2) then
              do i=1,dm1
                call variance(var,data1(i,:),N)
                std =sqrt(var)
                scale_std = max(0.3- std,0.0)
                !write(*,*) scale_std,std
                Ran= data1(1,1) + data1(2,1)+i
                call NGRNS(0.0,1.0,Ran,N,Gam)
                !call NGRNS(U,std*0.2,Ran,N,Gam)
                !scale_std=1
                do j=1,N
                  data1(i,j) = abs(data1(i,j) + Gam(1,j)*scale_std)
               end do
              end do     
          end if
          
         if(flag.eq.1) then
              do i=1,dm1
                call variance(var,data1(i,:),N)
                std =sqrt(var)
                scale_std = max(0.02- std,0.0)
                !write(*,*) scale_std,std
                Ran= data1(1,1) + data1(2,1)+i
                call NGRNS(0.0,1.0,Ran,N,Gam)
                !call NGRNS(U,std*0.2,Ran,N,Gam)
                !scale_std=1
                do j=1,N
                  data1(i,j) = data1(i,j) + Gam(1,j)*scale_std
               end do
              end do     
           end if

         !do i=1,dm1
         !     write(205,111) data1(i,:)
         !end do
         
 	return
end 


!==========================================
subroutine Resampling_index(N,indx_sample,Lsum)

	Integer N
    integer indx_sample(N)
	real Lsum(1,N)
	integer iHours,iMins,iSecs,i100th,n_ran
    double precision Ran
    real t_check,P_cum(N)
      
    real Likelyhood(1,N), p(1,N), MeanLikely, SelectLikely

    Integer A, obs_freq

!************Setting the observation error******************
      weit=0.1
      U=0
      n_ran=1
      SF= smooth
      if (flag.eq.0) SF=1
      if (flag.eq.2) SF=1
      if (flag.eq.1) SF=1
      t_check = 120
      obs_freq=2
!***********Generating observation operator***************
      
      
      do i=1,N
          Likelyhood(1,i) =  exp(-Lsum(1,i) /2/weit)/sqrt(2*3.14*weit)
      end do
      
      !MeanLikely= sum(Likelyhood)/N
      !SelectLikely= (maxval(Likelyhood) - MeanLikely)*0.5 + MeanLikely
      !  do i=1,N
      !    if (Likelyhood(1,i).le.SelectLikely) Likelyhood(1,i)=0 
      !end do
      
      do i=1,N
          p(1,i) = Likelyhood(1,i)/sum(Likelyhood)
      end do
      


      ! Resampling
      P_cum(1) =P(1,1)
      do i=2,N
          P_cum(i) = P_cum(i-1) + P(1,i)
      end do
      !write(*,*) P_cum
      rec_find = 1
      do i=1,N
          aa= 0 + 0.9995/N*i
          !write(*,*) a
          do j = rec_find,N
              if(P_cum(j).ge.aa) then
                  rec_find = j
                  indx_sample(i) = j
                  goto 99
              end if
          end do
          
99        continue
          !write(*,*) aa, P_cum(rec_find),rec_find
      end do
      
      !      write(206,111) Lsum
      !write(206,111) Likelyhood
      !write(206,111) P_cum
      
111	   format(220E20.10,1x)
end


!=============================================
subroutine updatePF(data1,data2,dm1,N, index_sample,flag)

	real upbound,lowbound,Gam(1,N),t,weit,smooth
	real SF,Lsum(1,N)
	integer iHours,iMins,iSecs,i100th,n_ran,flag
    double precision Ran
    integer dm1, index_sample(N)
    real data1(dm1,N), data2(dm1,N)


                  
    do i=1,dm1
        do j=1,N
            kk=index_sample(j)
            data1(i,j) = data2(i,kk)
        end do
    end do
    

    if(flag.eq.2) then
        do i=1,dm1
        call variance(var,data1(i,:),N)
        std =sqrt(var)
        scale_std = max(0.3- std,0.0)
        !write(*,*) scale_std,std
        Ran= data1(1,1) + data1(2,1)+i
        call NGRNS(0.0,1.0,Ran,N,Gam)
        !call NGRNS(U,std*0.2,Ran,N,Gam)
        !scale_std=1
        do j=1,N
            data1(i,j) = abs(data1(i,j) + Gam(1,j)*scale_std)
        end do
        end do     
    end if
          
    if(flag.eq.1) then
        do i=1,dm1
        call variance(var,data1(i,:),N)
        std =sqrt(var)
        scale_std = max(0.01- std,0.0)
        scale_std=0.0005
        !write(*,*) scale_std,std
        Ran= data1(1,1) + data1(2,1)+i
        call NGRNS(0.0,1.0,Ran,N,Gam)
        !call NGRNS(U,std*0.2,Ran,N,Gam)
        !scale_std=1
        do j=1,N
            data1(i,j) = data1(i,j) + Gam(1,j)*scale_std
        end do
        end do     
    end if
    
    !  if(flag.eq.0) then
    !    do i=1,dm1
    !    call variance(var,data1(i,:),N)
    !    std =sqrt(var)
    !    scale_std = max(0.5- std,0.0)
    !    Ran= data1(1,1) + data1(2,1)+i
    !    call NGRNS(0.0,1.0,Ran,N,Gam)
    !    do j=1,N
    !        data1(i,j) = data1(i,j) + Gam(1,j)*scale_std
    !    end do
    !    end do     
    !end if
        
111	   format(220E20.10,1x)
end
!        
    
!=====================================================
subroutine ES_algorithm(dpr1_all,obs_val_rec,obslen,ES_num1,rec_ens,Bmatrix,weight_GM)
integer obslen,ES_num1,rec_ens
double precision Ran

real Gam(rec_ens), meanX,weit,Gamma(ES_num1,rec_ens),GammaT(rec_ens,ES_num1)
integer iHours,iMins,iSecs,i100th,n_ran,flag
real dpr1_all(ES_num1,rec_ens), obs_val_rec(ES_num1),R(ES_num1,ES_num1)
real Amatrix(ES_num1,rec_ens),Imatrix(ES_num1,rec_ens)
real P1(ES_num1,ES_num1) ! P1=h^2*A*AT, and P1= (h^2*A*AT+R)^-1
real P2(ES_num1,ES_num1) ! P2=h^2*A*AT+R
real P3(ES_num1,ES_num1)
real P(rec_ens,ES_num1) ! P=h*AT*(h^2*A*AT+R)^-1
real Bmatrix1(rec_ens,rec_ens),Bmatrix(rec_ens,rec_ens) ! Bmatrix1= P * Imatrix, Bmatrix=h*Bmatrix1
real AmatrixT(rec_ens,Es_num1)
real h, tuning
double precision a(Es_num1), Like_GM(rec_ens), sumL, weight_GM(rec_ens)
    
    h = 0.0 ! bandwidth
    weit=0.5
    tuning = 0.1
!----------Get matrix Amatrix=(x-mean(x))----------------------------------------   Checked
    do i=1,ES_num1
        meanX= sum(dpr1_all(i,:))/rec_ens
        do j=1, rec_ens
            Amatrix(i,j) = (dpr1_all(i,j) - meanX)*h
            AmatrixT(j,i) = Amatrix(i,j)
        end do
    end do
! ----------Get matrix Imatrix=(d-dpr1_all)-----------------------------------------  Checked
     do i=1,ES_num1
        call gettim(iHours,iMins,iSecs,i100th)
        Ran = i + i100th + iSecs + iMins
        call NGRNS(0.0,weit,Ran,rec_ens,Gam)
        do j=1, rec_ens
            Imatrix(i,j) = obs_val_rec(i) -  dpr1_all(i,j)  + Gam(j)
            Gamma(i,j) = Gam(j)
            GammaT(j,i) = Gam(j)
        end do
    end do     

!----------------------------------------------------------------------------------
    call BRMUL(Amatrix,AmatrixT,Es_num1,rec_ens,Es_num1,P1) ! Checked
    call BRMUL(Gamma,GammaT,Es_num1,rec_ens,Es_num1,R)
    

        
    do i=1,Es_num1
        do j=1,Es_num1
            P2(i,j) = P1(i,j) + R(i,j)
            P3(i,j) = P2(i,j)
        end do
    end do ! Checked
    
        
    call inverse(P2,P1,Es_num1)
     
    call BRMUL(AmatrixT,P1,rec_ens,Es_num1,Es_num1,P)

    
    Bmatrix1= MATMUL(P, Imatrix) 
    
    do i=1,rec_ens
        do j=1,rec_ens
            Bmatrix(i,j) = h*Bmatrix1(i,j)
        end do
    end do
    
    ! Imatrix(ES_num1,rec_ens)
    do i = 1, rec_ens
        do j = 1,ES_num1
            a(j) = Imatrix(j,i)**2.0/P3(j,j)*-0.5*(real(rec_ens)-1.0)*0.25*0.25
        end do
        !write(*,*) sum(a)
        Like_GM(i) = exp(sum(a)) +  1D-100
    end do
    
    sumL = sum(Like_GM)
    
    do i=1,rec_ens
        weight_GM(i) = Like_GM(i)/sumL!*(1.0 - tuning) + tuning*1.0/real(rec_ens)
    end do
    !write(*,*) weight_GM
    !write(*,*) sum(weight_GM)
    
    
    111	   format(1220E20.10,1x)
end

      
!===================================================
subroutine ES_update(data1,data2,Bmatrix,N,rec_ens, weight_GM)
integer N, rec_ens
real Bmatrix(rec_ens,rec_ens) ! Bmatrix= P * Imatrix
real data1(N,rec_ens),data2(N,rec_ens)
real data3(N,rec_ens), data4(N,rec_ens),mean, aa
integer rec_find, indx_sample(rec_ens)

double precision weight_GM(rec_ens), pcum(rec_ens)

do i=1,N
    mean= sum(data1(i,:))/real(rec_ens)
    do j=1,rec_ens
        data3(i,j) = data1(i,j) - mean
    end do
end do

data4= MATMUL(data3, Bmatrix)  !(x-mean(x))*B

!do i=1,N
!    do j=1,rec_ens
!        data2(i,j) = data1(i,j) + data4(i,j)
!    end do
!end do

pcum(1) = weight_GM(1)
do i = 2, rec_ens
    pcum(i) = pcum(i-1) + weight_GM(i)
end do

      rec_find = 1
      do i=1,rec_ens
          aa= 0.0 + 0.9995/real(rec_ens)*real(i)
          !write(*,*) a
          do j = rec_find,rec_ens
              if(pcum(j).ge.aa) then
                  rec_find = j
                  indx_sample(i) = j
                  goto 99
              end if
          end do
          
99        continue
          !write(*,*) indx_sample(i),aa,Pcum(j), i
      end do
      
!write(*,*) indx_sample

do i=1,N ! Resample the kernels
    do j=1,rec_ens
        data2(i,j) = data1(i,indx_sample(j)) + data4(i,indx_sample(j))
        if (data2(i,j).lt.0.02) data2(i,j) = data1(i,indx_sample(j))
        !write(*,*) data4(i,indx_sample(j))
    end do
end do
continue
!write(*,*) indx_sample
continue


end

!====================================================
subroutine Par_Inflation(Par, rec_ens,ParVariance)
integer  rec_ens
integer iHours,iMins,iSecs,i100th,n_ran,ss
real Par(1,rec_ens), ParVariance, var, std, Gam(rec_ens)
double precision Ran

    call variance(var,Par,rec_ens)
    std =sqrt(var)
    !scale_std = 0.05*std
    ss = 0.05
    if(std.gt.20.0) ss = 0.001
    scale_std = max(ParVariance**0.5/5.0 - std,ss*ParVariance)
    
    call gettim(iHours,iMins,iSecs,i100th)
    Ran=iSecs+i100th
    call NGRNS(0.0,1.0,Ran,rec_ens,Gam)

    do j=1,rec_ens
        Par(1,j) = abs(Par(1,j) + Gam(j)*scale_std)
    end do
    !write(*,*) std, scale_std, ParVariance**0.5/25
    continue

end
!==================================================
!=====================================
subroutine ES_algorithmAdp(dpr1_all,obs_val_rec,obslen,ES_num1,rec_ens,Bmatrix,weight_GM)
integer obslen,ES_num1,rec_ens
double precision Ran

real Gam(rec_ens), meanX,weit,Gamma(ES_num1,rec_ens),GammaT(rec_ens,ES_num1)
integer iHours,iMins,iSecs,i100th,n_ran,flag
real dpr1_all(ES_num1,rec_ens), obs_val_rec(ES_num1),R(ES_num1,ES_num1)
real Amatrix(ES_num1,rec_ens),Imatrix(ES_num1,rec_ens)
real P1(ES_num1,ES_num1) ! P1=h^2*A*AT, and P1= (h^2*A*AT+R)^-1
real P2(ES_num1,ES_num1) ! P2=h^2*A*AT+R
real P3(ES_num1,ES_num1)
real P(rec_ens,ES_num1) ! P=h*AT*(h^2*A*AT+R)^-1
real Bmatrix1(rec_ens,rec_ens),Bmatrix(rec_ens,rec_ens) ! Bmatrix1= P * Imatrix, Bmatrix=h*Bmatrix1
real AmatrixT(rec_ens,Es_num1)
real h, tuning, tunningF, noise(rec_ens),aa, pcum(rec_ens), kk_all(ES_num1)
integer rec_find, indx_sample(rec_ens),kk(rec_ens), nn, indx_sampleRec(100,rec_ens)
integer rec_alphanum
double precision a(Es_num1), Like_GM(rec_ens), sumL, weight_GM(rec_ens),weight_GM2(rec_ens), weight_GMRec(100,rec_ens)
real dpr1_all_res(ES_num1,rec_ens), alpha_un1,alpha_un, tuningN_rec

    
    h = 0.0!bandwidth
    weit=0.5
    tuning = 1.0
!----------Get matrix Amatrix=(x-mean(x))----------------------------------------   Checked
    do i=1,ES_num1
        meanX= sum(dpr1_all(i,:))/rec_ens
        do j=1, rec_ens
            Amatrix(i,j) = (dpr1_all(i,j) - meanX)*h
            AmatrixT(j,i) = Amatrix(i,j)
        end do
    end do
! ----------Get matrix Imatrix=(d-dpr1_all)-----------------------------------------  Checked
     do i=1,ES_num1
        call gettim(iHours,iMins,iSecs,i100th)
        Ran = i + i100th + iSecs + iMins
        call NGRNS(0.0,weit,Ran,rec_ens,Gam)
        do j=1, rec_ens
            Imatrix(i,j) = obs_val_rec(i) -  dpr1_all(i,j)  + Gam(j)
            Gamma(i,j) = Gam(j)
            GammaT(j,i) = Gam(j)
        end do
    end do     

!----------------------------------------------------------------------------------
    call BRMUL(Amatrix,AmatrixT,Es_num1,rec_ens,Es_num1,P1) ! Checked
    call BRMUL(Gamma,GammaT,Es_num1,rec_ens,Es_num1,R)
    

        
    do i=1,Es_num1
        do j=1,Es_num1
            P2(i,j) = P1(i,j) + R(i,j)
            P3(i,j) = P2(i,j)
        end do
    end do ! Checked
    
        
    call inverse(P2,P1,Es_num1)
     
    call BRMUL(AmatrixT,P1,rec_ens,Es_num1,Es_num1,P)

    
    Bmatrix1= MATMUL(P, Imatrix) 
    
    do i=1,rec_ens
        do j=1,rec_ens
            Bmatrix(i,j) = h*Bmatrix1(i,j)
        end do
    end do
    !call RANDOM_NUMBER(noise)
    ! Imatrix(ES_num1,rec_ens)
    nn=1
    alpha_un1 = 0
    rec_alphanum =1
    tuningN_rec = 0.0
    
    do tuning = 0.05,1,0.05
    do i = 1, rec_ens
        do j = 1,ES_num1
            a(j) = Imatrix(j,i)**2.0/P3(j,j)*-0.5*(real(rec_ens)-1.0)*tuning*tuning
        end do
        Like_GM(i) = exp(sum(a)) +  1D-100
    end do
    
    sumL = sum(Like_GM)
    
    do i=1,rec_ens
        weight_GM(i) = Like_GM(i)/sumL
        weight_GMRec(nn,i) = weight_GM(i)
    end do
    ! Resampling
    
pcum(1) = weight_GM(1)
do i = 2, rec_ens
    pcum(i) = pcum(i-1) + weight_GM(i)
end do


! reampling 
      rec_find = 1
      do i=1,rec_ens
          aa= 0.0 + 0.9995/real(rec_ens)*real(i)
          !aa = noise(i)
          do j = rec_find,rec_ens
          !do j = 1,rec_ens
              if(pcum(j).ge.aa) then
                  rec_find = j
                  indx_sample(i) = j
                  goto 99
              end if
          end do
99        continue
      end do
      
      do i=1,rec_ens
          indx_sampleRec(nn,i) = indx_sample(i)
      end do
      
    ! resample the observations
    do i=1, ES_num1
        do j=1,rec_ens
            dpr1_all_res(i,j) = dpr1_all(i,indx_sample(j))
            kk(j) = 0
            if(dpr1_all_res(i,j).gt.obs_val_rec(i))  kk(j) = 1
        end do
        kk_all(i) =real(sum(kk))/real(rec_ens) 
    enddo
    call selection_sort(kk_all,ES_num1)
    do i=1,Es_num1
        kk_all(i) = abs(kk_all(i) - real(i)/real(Es_num1))
    end do    
    alpha_un = 1.0 - sum(kk_all)*2.0/real(rec_ens)
        
    if(alpha_un.gt.alpha_un1*1.005) then
    !if(alpha_un.gt.alpha_un1*1.001) then
        alpha_un1 = alpha_un
        rec_alphanum = nn
        tuningN_rec = tuning
    end if
    
    
    
    nn = nn+1
    
    end do
    
     do i=1,rec_ens
         indx_sample(i) = indx_sampleRec(rec_alphanum,i)
         weight_GM(i) = weight_GMRec(rec_alphanum,i)
     end do
        
     write(*,*) alpha_un, tuningN_rec
    

    
    111	   format(1220E20.10,1x)
end
!!=======================================
subroutine selection_sort(a,n)
  implicit none
  integer :: n
  real a(n)
  integer i,j  
  real min  
  real temp 
  do i=1,n-1
    min=a(i)     
    do j=i+1,n
      if ( min > a(j) ) then   
        temp=a(j)        
        a(j)=a(i)
        a(i)=temp
        min=a(i)
      end if
 end do
  end do                              
  return
end subroutine  
!=====================================
!=====================================
subroutine ES_algorithmAdpPrint(dpr1_all,obs_val_rec,obslen,ES_num1,rec_ens,Bmatrix,weight_GM,t)
integer obslen,ES_num1,rec_ens
double precision Ran

real t
real Gam(rec_ens), meanX,weit,Gamma(ES_num1,rec_ens),GammaT(rec_ens,ES_num1)
integer iHours,iMins,iSecs,i100th,n_ran,flag
real dpr1_all(ES_num1,rec_ens), obs_val_rec(ES_num1),R(ES_num1,ES_num1)
real Amatrix(ES_num1,rec_ens),Imatrix(ES_num1,rec_ens)
real P1(ES_num1,ES_num1) ! P1=h^2*A*AT, and P1= (h^2*A*AT+R)^-1
real P2(ES_num1,ES_num1) ! P2=h^2*A*AT+R
real P3(ES_num1,ES_num1)
real P(rec_ens,ES_num1) ! P=h*AT*(h^2*A*AT+R)^-1
real Bmatrix1(rec_ens,rec_ens),Bmatrix(rec_ens,rec_ens) ! Bmatrix1= P * Imatrix, Bmatrix=h*Bmatrix1
real AmatrixT(rec_ens,Es_num1)
real h, tuning, tunningF, noise(rec_ens),aa, pcum(rec_ens), kk_all(ES_num1)
integer rec_find, indx_sample(rec_ens),kk(rec_ens), nn, indx_sampleRec(100,rec_ens)
integer rec_alphanum
double precision a(Es_num1), Like_GM(rec_ens), sumL, weight_GM(rec_ens),weight_GM2(rec_ens), weight_GMRec(100,rec_ens)
real dpr1_all_res(ES_num1,rec_ens), alpha_un1,alpha_un, tuningN_rec

    
    h = 0.0!bandwidth
    weit=0.5
    tuning = 1.0
!----------Get matrix Amatrix=(x-mean(x))----------------------------------------   Checked
    do i=1,ES_num1
        meanX= sum(dpr1_all(i,:))/rec_ens
        do j=1, rec_ens
            Amatrix(i,j) = (dpr1_all(i,j) - meanX)*h
            AmatrixT(j,i) = Amatrix(i,j)
        end do
    end do
! ----------Get matrix Imatrix=(d-dpr1_all)-----------------------------------------  Checked
     do i=1,ES_num1
        call gettim(iHours,iMins,iSecs,i100th)
        Ran = i + i100th + iSecs + iMins
        call NGRNS(0.0,weit,Ran,rec_ens,Gam)
        do j=1, rec_ens
            Imatrix(i,j) = obs_val_rec(i) -  dpr1_all(i,j)  + Gam(j)
            Gamma(i,j) = Gam(j)
            GammaT(j,i) = Gam(j)
        end do
    end do     

!----------------------------------------------------------------------------------
    call BRMUL(Amatrix,AmatrixT,Es_num1,rec_ens,Es_num1,P1) ! Checked
    call BRMUL(Gamma,GammaT,Es_num1,rec_ens,Es_num1,R)
    

        
    do i=1,Es_num1
        do j=1,Es_num1
            P2(i,j) = P1(i,j) + R(i,j)
            P3(i,j) = P2(i,j)
        end do
    end do ! Checked
    
        
    call inverse(P2,P1,Es_num1)
     
    call BRMUL(AmatrixT,P1,rec_ens,Es_num1,Es_num1,P)

    
    Bmatrix1= MATMUL(P, Imatrix) 
    
    do i=1,rec_ens
        do j=1,rec_ens
            Bmatrix(i,j) = h*Bmatrix1(i,j)
        end do
    end do
    !call RANDOM_NUMBER(noise)
    ! Imatrix(ES_num1,rec_ens)
    nn=1
    alpha_un1 = 0
    rec_alphanum =1
    tuningN_rec = 0.0
    
    do tuning = 0.05,1.0,0.05
    do i = 1, rec_ens
        do j = 1,ES_num1
            a(j) = Imatrix(j,i)**2.0/P3(j,j)*-0.5*(real(rec_ens)-1.0)*tuning*tuning
        end do
        Like_GM(i) = exp(sum(a)) +  1D-50
    end do
    
    sumL = sum(Like_GM)
    
    do i=1,rec_ens
        weight_GM(i) = Like_GM(i)/sumL
        weight_GMRec(nn,i) = weight_GM(i)
    end do
    ! Resampling
    
pcum(1) = weight_GM(1)
do i = 2, rec_ens
    pcum(i) = pcum(i-1) + weight_GM(i)
end do


! reampling 
      rec_find = 1
      do i=1,rec_ens
          aa= 0.0 + 0.9995/real(rec_ens)*real(i)
          !aa = noise(i)
          do j = rec_find,rec_ens
          !do j = 1,rec_ens
              if(pcum(j).ge.aa) then
                  rec_find = j
                  indx_sample(i) = j
                  goto 99
              end if
          end do
99        continue
      end do
      
      do i=1,rec_ens
          indx_sampleRec(nn,i) = indx_sample(i)
      end do
      
    ! resample the observations
    do i=1, ES_num1
        do j=1,rec_ens
            dpr1_all_res(i,j) = dpr1_all(i,indx_sample(j))
            kk(j) = 0
            if(dpr1_all_res(i,j).gt.obs_val_rec(i))  kk(j) = 1
        end do
        kk_all(i) =real(sum(kk))/real(rec_ens) 
    enddo
    call selection_sort(kk_all,ES_num1)
    do i=1,Es_num1
        kk_all(i) = abs(kk_all(i) - real(i)/real(Es_num1))
    end do    
    alpha_un = 1.0 - sum(kk_all)*2.0/real(rec_ens)
        
    !if(alpha_un.gt.alpha_un1*1.00075) then
    if(alpha_un.gt.alpha_un1*1.005) then
        alpha_un1 = alpha_un
        rec_alphanum = nn
        tuningN_rec = tuning
    end if
    
    nn = nn+1
    
    write(226,*) t,tuning,alpha_un,tuningN_rec
    
    end do
    
     do i=1,rec_ens
         indx_sample(i) = indx_sampleRec(rec_alphanum,i)
         weight_GM(i) = weight_GMRec(rec_alphanum,i)
     end do
        
     write(*,*) alpha_un, tuningN_rec
    

    
    111	   format(1220E20.10,1x)
end
!!=======================================
      