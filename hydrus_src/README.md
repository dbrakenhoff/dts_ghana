# HYDRUS-1D Source Code

Developed by Jianzhi Dong. This is the version labeled as `RealDTScodes_Dec_23_RealDTS` in files provided by TU Delft.
- Can only be compiled with `ifort`
- Cannot be compiled at this time as subroutine `GENRANDOM` is missing.