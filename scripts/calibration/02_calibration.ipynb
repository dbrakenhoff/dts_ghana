{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Calibrating DTS temperature observations <a id=\"top\"></a>\n",
    "_Notebook created by Davíd Brakenhoff, Artesia, March 2020_\n",
    "\n",
    "<hr>\n",
    "\n",
    "## Goal\n",
    "This notebook performs a calibration for double-ended DTS measurements and stores the result as a netCDF file. This script assumes the double-ended measurement was already aligned and the reference temperature timeseries were added to the datastore.\n",
    "\n",
    "## Contents:\n",
    "- [User settings](#1)\n",
    "- [Calibration sections](#2)\n",
    "- [Load data and set calibration sections](#3)\n",
    "- [Calibration](#4)\n",
    "- [Export result to netCDF](#5)\n",
    "\n",
    "<hr>\n",
    "\n",
    "Import the requisite packages:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "C:\\Users\\dbrak\\Anaconda3\\envs\\dts\\lib\\typing.py:898: FutureWarning: xarray subclass DataStore should explicitly define __slots__\n",
      "  super().__init_subclass__(*args, **kwargs)\n"
     ]
    }
   ],
   "source": [
    "import dtscalibration as dts"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## [User settings](#top)\n",
    "The settings below can be changed to control the script i.e. \n",
    "- which channel to use for visualization,\n",
    "- which name to give the calibration result\n",
    "- where to store the resulting netCDF file\n",
    "\n",
    "The `drop_deviating_acquisition_time` flag will look for the lowest forward acquisition time and remove it from the datastore. All acquisition times have to be constant in order for the calibration to work. Please check your data before setting this flag to `True` as it might cause unwanted results if all acquisition times are constant. In the dataset used here, this step was necessary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Script settings\n",
    "channel = \"ch1\"\n",
    "calibstr = \"ambientearth\"\n",
    "drop_deviating_acquisition_time = True  # drops lowest forward acquisition time\n",
    "\n",
    "# netcdf to load (containing aligned DTS data)\n",
    "fnetcdf = f\"../../data/nc/nyankpala_campaign1_{channel}.nc\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## [Calibration sections](#top)\n",
    "Define the calibration sections"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "# sections for calibration\n",
    "sections_calib = {\n",
    "    'ambient': [slice(10, 25)],\n",
    "    'earth': [slice(610, 640)]\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## [Load data and set calibration sections](#top)\n",
    "\n",
    "Load the DTS data (which is assumed to be aligned, and contains the reference temperature series)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %% Read data\n",
    "ds = dts.open_datastore(fnetcdf)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "# for avoiding error about non-constant forward acquisistion time,\n",
    "# drop timestep with deviating step\n",
    "if drop_deviating_acquisition_time:\n",
    "    fws = ds[\"acquisitiontimeFW\"].to_pandas()\n",
    "    t = fws.idxmin()\n",
    "    ds = ds.drop_sel(time=t)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# data labels\n",
    "st_label = 'st'\n",
    "ast_label = 'ast'\n",
    "rst_label = 'rst'\n",
    "rast_label = 'rast'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Add the calibration sections to the datastore"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %% Set calibration sections\n",
    "ds.sections = sections_calib"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## [Calibration](#top)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Get some data ready for calibration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %% Stokes/Anti-stokes variance\n",
    "# See: https://python-dts-calibration.readthedocs.io/en/latest/examples/notebooks/08Calibrate_double_wls.ipynb.html\n",
    "st_var, st_resid = ds.variance_stokes_constant(st_label=st_label)\n",
    "ast_var, ast_resid = ds.variance_stokes_constant(st_label=ast_label)\n",
    "rst_var, _ = ds.variance_stokes_constant(st_label=rst_label)\n",
    "rast_var, _ = ds.variance_stokes_constant(st_label=rast_label)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the calibration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %% Run the calibration, check the residuals\n",
    "# See: https://python-dts-calibration.readthedocs.io/en/latest/reference/dtscalibration.html\n",
    "ds.calibration_double_ended(\n",
    "    sections=sections_calib,\n",
    "    st_label=st_label,\n",
    "    ast_label=ast_label,\n",
    "    rst_label=rst_label,\n",
    "    rast_label=rast_label,\n",
    "    st_var=st_var,\n",
    "    ast_var=ast_var,\n",
    "    rst_var=rst_var,\n",
    "    rast_var=rast_var,\n",
    "    store_tmpw='tmpw',\n",
    "    method='wls',\n",
    "    solver='sparse')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Calculate the confidence intervals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %% Confidence intervals\n",
    "ds.conf_int_double_ended(\n",
    "    p_val='p_val',\n",
    "    p_cov='p_cov',\n",
    "    st_label=st_label,\n",
    "    ast_label=ast_label,\n",
    "    rst_label=rst_label,\n",
    "    rast_label=rast_label,\n",
    "    st_var=st_var,\n",
    "    ast_var=ast_var,\n",
    "    rst_var=rst_var,\n",
    "    rast_var=rast_var,\n",
    "    store_tmpf='tmpf',\n",
    "    store_tmpb='tmpb',\n",
    "    store_tmpw='tmpw',\n",
    "    store_tempvar='_var',\n",
    "    conf_ints=[2.5, 50., 97.5],\n",
    "    mc_sample_size=500,  # <- choose a much larger sample size\n",
    "    ci_avg_time_flag=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## [Export result to netCDF](#top)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %% Save calibrated datastore\n",
    "ds.to_netcdf(path=f\"../../data/nc/nyankpala_campaign1_{channel}_calib_{calibstr}.nc\",\n",
    "             format=\"NETCDF4\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.8.1 64-bit ('dts': conda)",
   "language": "python",
   "name": "python38164bitdtsconda67a3c8b3766a449081f5b0a9bf2cabb3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}