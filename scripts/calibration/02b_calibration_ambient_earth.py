import dtscalibration as dts

# %% User settings

# Script settings
channel = "ch1"
calibstr = "ambientearth"
savefig = True
figdir = "./fig"
drop_deviating_acquisition_time = True  # drops lowest forward acquisition time

# netcdf to load (containing aligned DTS data)
fnetcdf = f"../../data/nc/nyankpala_campaign2_{channel}.nc"

# sections for calibration
sections_calib = {
    'ambient': [slice(10, 25)],
    'earth': [slice(610, 640)]
}

# %% Read data
ds = dts.open_datastore(fnetcdf)

# for avoiding error about non-constant forward acquisistion time,
# drop timestep with deviating step
if drop_deviating_acquisition_time:
    fws = ds["acquisitiontimeFW"].to_pandas()
    t = fws.idxmin()
    ds = ds.drop_sel(time=t)

# data labels
st_label = 'st'
ast_label = 'ast'
rst_label = 'rst'
rast_label = 'rast'

# %% Set calibration sections
ds.sections = sections_calib

import numpy as np
a = ds.ufunc_per_section(label='st',
                         ref_temp_broadcasted=True,
                         calc_per='all')
a = np.array(a)

# %% Stokes/Anti-stokes variance
# See: https://python-dts-calibration.readthedocs.io/en/latest/examples/notebooks/08Calibrate_double_wls.ipynb.html
st_var, st_resid = ds.variance_stokes_constant(st_label=st_label)
ast_var, ast_resid = ds.variance_stokes_constant(st_label=ast_label)
rst_var, _ = ds.variance_stokes_constant(st_label=rst_label)
rast_var, _ = ds.variance_stokes_constant(st_label=rast_label)

# %% Run the calibration, check the residuals
# See: https://python-dts-calibration.readthedocs.io/en/latest/reference/dtscalibration.html
ds.calibration_double_ended(
    sections=sections_calib,
    st_var=st_var,
    ast_var=ast_var,
    rst_var=rst_var,
    rast_var=rast_var,
    store_tmpw='tmpw',
    method='wls',
    solver='sparse',
    verbose=True)

# temperature residuals
tmp_resid = ds.temperature_residuals('tmpw')

# %% Confidence intervals

ds.conf_int_double_ended(
    p_val='p_val',
    p_cov='p_cov',
    st_var=st_var,
    ast_var=ast_var,
    rst_var=rst_var,
    rast_var=rast_var,
    store_tmpf='tmpf',
    store_tmpb='tmpb',
    store_tmpw='tmpw',
    store_tempvar='_var',
    conf_ints=[2.5, 50., 97.5],
    mc_sample_size=500)  # <- choose a much larger sample size

# %% Save calibrated store
ds.to_netcdf(path=f"../../data/nc/nyankpala_campaign1_{channel}_calib_{calibstr}.nc",
             format="NETCDF4")
