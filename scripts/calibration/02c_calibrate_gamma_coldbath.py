import dtscalibration as dts

# %% User settings

# script settings
channel = "ch1"
drop_deviating_acquisition_time = True  # drops lowest forward acquisition time

# netcdf to load (containing aligned DTS data)
fnetcdf = f"../data/nc/nyankpala_campaign1_{channel}.nc"

# time for calibration (period of constant temp in cold bath)
timeslice = slice("2019-08-28 22:00", "2019-08-29 21:00")

# sections for calibration
sections_calib = {
    'cold': [slice(45, 55)]
}

# %% Read data
ds = dts.open_datastore(fnetcdf)

# for avoiding error about non-constant forward acquisistion time,
# drop timestep with deviating step
if drop_deviating_acquisition_time:
    fws = ds["acquisitiontimeFW"].to_pandas()
    t = fws.idxmin()
    ds = ds.drop_sel(time=t)

ds = ds.sel(time=timeslice)

# data labels
st_label = 'ST'
ast_label = 'AST'
rst_label = 'REV-ST'
rast_label = 'REV-AST'

# %% Set calibration sections
ds.sections = sections_calib

# %% Stokes/Anti-stokes variance
# See: https://python-dts-calibration.readthedocs.io/en/latest/examples/notebooks/08Calibrate_double_wls.ipynb.html
st_var, st_resid = ds.variance_stokes_constant(st_label=st_label)
ast_var, ast_resid = ds.variance_stokes_constant(st_label=ast_label)
rst_var, _ = ds.variance_stokes_constant(st_label=rst_label)
rast_var, _ = ds.variance_stokes_constant(st_label=rast_label)

# %% Run the calibration, check the residuals
# See: https://python-dts-calibration.readthedocs.io/en/latest/reference/dtscalibration.html
ds.calibration_double_ended(
    sections=sections_calib,
    st_label=st_label,
    ast_label=ast_label,
    rst_label=rst_label,
    rast_label=rast_label,
    st_var=st_var,
    ast_var=ast_var,
    rst_var=rst_var,
    rast_var=rast_var,
    store_tmpw='TMPW',
    method='wls',
    solver='sparse')

# print gamma value
print("Calibrated gamma = ", ds.gamma.values)
