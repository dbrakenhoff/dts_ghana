import dtscalibration as dts

# %% USer settings

# Script settings
channel = "ch1"
calibstr = "fixedgamma"
savefig = True
figdir = "./fig"
drop_deviating_acquisition_time = True  # drops lowest forward acquisition time

# netcdf to load (containing aligned DTS data)
fnetcdf = f"../../data/nc/nyankpala_campaign1_{channel}.nc"

# fixed gamma by calibrating on cold bath in period given in timeslice
# see 02c_calibrate_gamma_coldbath.py
fixedgamma = (484.51092888052636, 0)  # (gamma value, gamma variance)
timeslice = slice("2019-08-28 22:00", "2019-08-29 21:00")

# Sections for calibration
sections_calib = {
    # 'cold': [slice(45, 55)]
    'ambient': [slice(10, 25)]
}

# %% Read data
ds = dts.open_datastore(fnetcdf)

# for avoiding error about non-constant forward acquisistion time,
# drop timestep with deviating step
if drop_deviating_acquisition_time:
    fws = ds["acquisitiontimeFW"].to_pandas()
    t = fws.idxmin()
    ds = ds.drop_sel(time=t)

# data labels
st_label = 'ST'
ast_label = 'AST'
rst_label = 'REV-ST'
rast_label = 'REV-AST'

# %% Set calibration sections
ds.sections = sections_calib

# %% Stokes/Anti-stokes variance
# See: https://python-dts-calibration.readthedocs.io/en/latest/examples/notebooks/08Calibrate_double_wls.ipynb.html
st_var, st_resid = ds.variance_stokes_constant(st_label=st_label)
ast_var, ast_resid = ds.variance_stokes_constant(st_label=ast_label)
rst_var, _ = ds.variance_stokes_constant(st_label=rst_label)
rast_var, _ = ds.variance_stokes_constant(st_label=rast_label)

# %% Run the calibration, check the residuals
# See: https://python-dts-calibration.readthedocs.io/en/latest/reference/dtscalibration.html
ds.calibration_double_ended(
    sections=sections_calib,
    fix_gamma=fixedgamma,
    st_label=st_label,
    ast_label=ast_label,
    rst_label=rst_label,
    rast_label=rast_label,
    st_var=st_var,
    ast_var=ast_var,
    rst_var=rst_var,
    rast_var=rast_var,
    store_tmpw='TMPW',
    method='wls',
    solver='sparse')

print("Gamma = ", ds.gamma.values)  # test if value remained fixed

# temperature residuals
tmp_resid = ds.temperature_residuals('TMPW')

# %% Confidence intervals

ds.conf_int_double_ended(
    p_val='p_val',
    p_cov='p_cov',
    st_label=st_label,
    ast_label=ast_label,
    rst_label=rst_label,
    rast_label=rast_label,
    st_var=st_var,
    ast_var=ast_var,
    rst_var=rst_var,
    rast_var=rast_var,
    store_tmpf='TMPF',
    store_tmpb='TMPB',
    store_tmpw='TMPW',
    store_tempvar='_var',
    conf_ints=[2.5, 50., 97.5],
    mc_sample_size=500,  # <- choose a much larger sample size
    ci_avg_time_flag=False)

# %% Save calibrated store
ds.to_netcdf(path=f"../data/nc/nyankpala_campaign1_{channel}_calib_{calibstr}.nc",
             format="NETCDF4")
