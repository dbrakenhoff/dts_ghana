import os

import matplotlib.pyplot as plt

import dtscalibration as dts
from dtscalibration import plot as dtsplot

# %% USer settings

# Script settings
channel = "ch1"
calibstr = "ambientearth"
savefig = True
figdir = "./fig"

# netcdf filename to create figures for
fnetcdf = f"../../data/nc/nyankpala_campaign1_{channel}_calib_{calibstr}.nc"

# sections for verification (i.e. all baths)
sections_verif = {
    'ambient': [slice(10, 25), slice(1235, 1245)],
    'cold': [slice(45, 55), slice(1210, 1220)],
    'earth': [slice(610, 640)]
}

# %% Read data
ds = dts.open_datastore(fnetcdf)

# %% Visualizations (post calibration)

# 2. Check the calibrated temperatures
# plot temp and confidence intervals last timestep
ds1 = ds.isel(time=-1)  # take only the last timestep
fig, ax = plt.subplots(1, 1, figsize=(12, 6))
ds1.tmpw.plot(ax=ax, linewidth=0.7, label="calibrated")
ds1.tmpw_mc.isel(CI=0).plot(linewidth=0.7, label='CI: 2.5%', ax=ax)
ds1.tmpw_mc.isel(CI=2).plot(linewidth=0.7, label='CI: 97.5%', ax=ax)

for name, sect in sections_verif.items():
    if name == "ambient":
        c = "LightGreen"
    elif name == "cold":
        c = "LightBlue"
    elif name == "earth":
        c = "Sienna"
    else:
        c = "gray"
    lbl = name
    for s in sect:
        ax.axvspan(s.start, s.stop, facecolor=c, alpha=0.5, label=lbl)
        lbl = None

ax.legend(loc="best")
ax.grid(b=True)
fig.tight_layout()
if savefig:
    fig.savefig(os.path.join(
        figdir, f"temp_ci_last_timestep_{channel}_{calibstr}.png"), dpi=150)

# Plot st dev of TWMPW
fig, ax = plt.subplots(1, 1, figsize=(12, 6))
(ds1.tmpw_mc_var**0.5).plot(ax=ax)
ax.set_ylabel('$\sigma$ ($^\circ$C)')
ax.grid(b=True)

# 3. Residuals in reference sections
for name, sect in sections_verif.items():
    ds.sections = {name: sect}
    temp_resid_verif = ds.temperature_residuals('tmpw')
    try:
        fig = dtsplot.plot_residuals_reference_sections(
            temp_resid_verif, {name: sect}, title='Calibrated temperature residuals')
    except:
        continue
    if savefig:
        fig.savefig(
            os.path.join(
                figdir, f"temp_residuals_{name}_after_calib_{channel}_{calibstr}.png"),
            dpi=150, bbox_inches="tight")


# 4. Mean DTS temp vs. probe temp
for name, sect in sections_verif.items():

    fig, ax = plt.subplots(1, 1, figsize=(16, 6))

    for s in sect:
        dsx = ds.sel(x=s).mean(dim="x")
        dsx.tmpw.plot.line(ax=ax, label=(f"mean DTS temp in {name} bath "
                                         f"(x:[{s.start}, {s.stop}])"))

    probe = ds.get(name)
    probe.plot.line(ax=ax, label=f"probe {name} bath")

    ax.legend(loc="best")
    ax.set_ylabel("temperature ($^\circ$C)")
    ax.grid(b=True)
    fig.savefig(f"./fig/bath_{name}_temp_over_time_calib_{channel}_{calibstr}.png",
                bbox_inches="tight", dpi=150)

plt.close("all")
