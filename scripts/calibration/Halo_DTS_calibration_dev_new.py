# -*- coding: utf-8 -*-
"""
Created on Tue Oct 22 09:01:19 2019

@author: bartschilperoo
"""
# Install development version dtscalibration (https://python-dts-calibration.readthedocs.io/en/latest/readme.html)
# pip install https://github.com/dtscalibration/python-dts-calibration/zipball/master --upgrade

from glob import glob
from datetime import datetime
import xarray as xr
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from dtscalibration import read_sensornet_files
from dtscalibration import plot as dtscplot
from dtscalibration.datastore_utils import suggest_cable_shift_double_ended, shift_double_ended

#%% Load in reference baths (.txt files, comma seperated)
# Define time correction
dt_ambient = pd.Timedelta(hours=-1)
dt_cold = pd.Timedelta(hours=+1)
dt_earth = pd.Timedelta(hours=-3, minutes=-42)

# Ambient bath
df_ambient = pd.read_csv(r'D:\Active\2017026_TWIGA\SENSORNET\warm_bath.txt', delimiter=',')
df_ambient = df_ambient.rename(columns={'Time':'time','Temperature':'ambient'})
df_ambient.time = [datetime.strptime(x, '%Y-%m-%d %H:%M:%S.%f') for x in df_ambient.time.astype(str)]
df_ambient['time'] = df_ambient.time + dt_ambient
df_ambient = df_ambient.set_index('time')
ds_ambient = df_ambient.to_xarray()

# earth bath
df_earth = pd.read_csv(r'D:\Active\2017026_TWIGA\SENSORNET\earth_bath.txt', delimiter=',')
df_earth = df_earth.rename(columns={'Time':'time','Temperature':'earth'})
df_earth.time = [datetime.strptime(x, '%Y-%m-%d %H:%M:%S.%f') for x in df_earth.time.astype(str)]
df_earth['time'] = df_earth.time + dt_earth
df_earth = df_earth.set_index('time')
ds_earth = df_earth.to_xarray()

# cold bath
df_cold = pd.read_csv(r'D:\Active\2017026_TWIGA\SENSORNET\cold_bath.txt', delimiter=',')
df_cold = df_cold.rename(columns={'Time':'time','Temperature':'cold'})
df_cold.time = [datetime.strptime(x, '%Y-%m-%d %H:%M:%S.%f') for x in df_cold.time.astype(str)]
df_cold['time'] = df_cold.time + dt_cold
df_cold = df_cold.set_index('time')
ds_cold = df_cold.to_xarray()

ds_ref = xr.merge([ds_ambient,
                   ds_earth,
                   ds_cold])
print(ds_ref)
ds_ref = ds_ref.resample(time='1min').mean(dim='time')

plt.figure()
ds_ref.ambient.plot()
ds_ref.cold.plot()
ds_ref.earth.plot()

#%%
# grab the .ddf files
files = glob(r'D:\Active\2017026_TWIGA\SENSORNET\channel_1\*.ddf')
# Load in the data
ds = read_sensornet_files(files)

# Correct the timestamp
t0 = datetime.strptime('2019-08-24 10:58:00', '%Y-%m-%d %H:%M:%S')
delta_t = np.datetime64(t0) - ds.time[0]
ds['time'] = ds.time + delta_t

# Check if data looks OK
plt.figure()
ds.ST.isel(time=0).plot()
ds['REV-ST'].isel(time=0).plot()

plt.figure()
np.log(ds.ST / ds['AST']).isel(time=0).plot()
np.log(ds['REV-ST'] / ds['REV-AST']).isel(time=0).plot()

# Add the reference temperatures
ds_ref2 = ds_ref.reindex({'time':ds.time}, method='nearest')
ds['ambient'] = ds_ref2.ambient
ds['cold'] = ds_ref2.cold
ds['earth'] = ds_ref2.earth

#%% Do the data shift to line up the channels

shift1, shift2 = suggest_cable_shift_double_ended(ds, irange=range(-10,10))

ds = shift_double_ended(ds, shift1)

plt.figure()
np.log(ds.ST / ds['AST']).isel(time=0).plot()
np.log(ds['REV-ST'] / ds['REV-AST']).isel(time=0).plot()

#%% Define the slocies

sections = {'ambient': [slice(10,25)],
            'cold':    [slice(45, 55)]}

#sections = {'ambient': [slice(10, 25), slice(1270, 1280)],
#            'cold':    [slice(47.5, 52.5), slice(1210, 1220)]}

ds.sections = sections

#%% Stokes/Anti-stokes variance
# See: https://python-dts-calibration.readthedocs.io/en/latest/examples/notebooks/08Calibrate_double_wls.ipynb.html
st_label = 'ST'
ast_label = 'AST'
rst_label = 'REV-ST'
rast_label = 'REV-AST'

st_var, resid = ds.variance_stokes(st_label=st_label)
ast_var, astresid = ds.variance_stokes(st_label=ast_label)
rst_var, _ = ds.variance_stokes(st_label=rst_label)
rast_var, _ = ds.variance_stokes(st_label=rast_label)

#%% Residual plots
dtscplot.plot_residuals_reference_sections(resid, sections)
dtscplot.plot_residuals_reference_sections(astresid, sections)

#%% Run the calibration, check the residuals
# See: https://python-dts-calibration.readthedocs.io/en/latest/reference/dtscalibration.html
ds.calibration_double_ended(
    sections=sections,
    st_label=st_label,
    ast_label=ast_label,
    rst_label=rst_label,
    rast_label=rast_label,
    st_var=st_var,
    ast_var=ast_var,
    rst_var=rst_var,
    rast_var=rast_var,
    store_tmpw='TMPW',
    method='wls',
    solver='sparse')

ds.TMPW.plot()

resid = ds.temperature_residuals('TMPW')
dtscplot.plot_residuals_reference_sections(resid, sections, title='Calibrated temperature residuals')

#%% Check the calibrated temperature
ds.isel(time=-1).TMPW.plot(label='calibrated')

#%% Confidence intervals¶

ds.conf_int_double_ended(
    p_val='p_val',
    p_cov='p_cov',
    st_label=st_label,
    ast_label=ast_label,
    rst_label=rst_label,
    rast_label=rast_label,
    st_var=st_var,
    ast_var=ast_var,
    rst_var=rst_var,
    rast_var=rast_var,
    store_tmpf='TMPF',
    store_tmpb='TMPB',
    store_tmpw='TMPW',
    store_tempvar='_var',
    conf_ints=[2.5, 50., 97.5],
    mc_sample_size=500,  # <- choose a much larger sample size
    ci_avg_time_flag=False)

ds1 = ds.isel(time=-1)  # take only the first timestep
ds1.TMPW.plot(linewidth=0.7, figsize=(12, 8))
ds1.TMPW_MC.isel(CI=0).plot(linewidth=0.7, label='CI: 2.5%')
ds1.TMPW_MC.isel(CI=2).plot(linewidth=0.7, label='CI: 97.5%')
plt.legend();

(ds1.TMPW_MC_var**0.5).plot(figsize=(12, 4));
plt.ylabel('$\sigma$ ($^\circ$C)');

#%% Add the verification baths and check the residuals to see if the calibration went well
sections = {'ambient': [slice(10, 25), slice(1270, 1280)],
            'cold':    [slice(45, 55), slice(1210, 1220)],
            'earth':   [slice(610,640)]
            }

ds.sections = sections

resid = ds.temperature_residuals('TMPW')
dtscplot.plot_residuals_reference_sections(resid, sections, title='Calibrated temperature residuals')

#%% Export data to CSV
df= ds[['TMPF', 'TMPB', 'TMPW']].to_dataframe()
print(df)

df.to_csv(r'D:\Active\2017026_TWIGA\SENSORNET\CSV\ch01_calib.csv', header=None, index=True, sep=',', mode='w')



