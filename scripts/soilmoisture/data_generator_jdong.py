import os
from datetime import datetime

import numpy as np
import pandas as pd
from scipy.io import loadmat
import scipy.interpolate as interpolate

from util import (datetime_to_matlab, smooth, get_roeseta_params)

from write_selector import write_selector
from write_atmosph import write_atmosphere
from write_meteo import write_meteo
from write_clm import write_clm
from write_profile import write_profile

from tqdm import tqdm

# %% Script control

verbose = False
ws = "D:/PassiveRealDTS"
# ws = "D:/python_attempt"

# writelist = ["METEO", "ATMOSPH", "SELECTOR", "PROFILE", "CLM"]
writelist = ["CLM"]

ensemble_size = 300

# BASIC INFO
start_date = pd.Timestamp("2011-01-01")
start_date_num = datetime_to_matlab(start_date)
start_time = 107.
end_time = 151.

latitude = 35.5
longitude = -98.
timezone = -6

basic_info = {
    "iVer": 4,
    "Hed": "Written with methods derived from Pydrus 0.0.2",
    "LUnit": "cm",
    "TUnit": "days",
    "MUnit": "mmol",
    "lWat": True,
    "lChem": False,
    "lTemp": True,
    "lSink": False,
    "lRoot": False,
    "lShort": False,
    "lWDep": True,
    "lScreen": False,
    "lVariabBC": True,
    "lEquil": True,
    "lInverse": False,
    "lSnow": False,
    "lHP1": False,
    "lMeteo": True,
    "lVapor": True,
    "lActiveU": False,
    "lFluxes": False,
    "lIrrig": False,
    "CosAlpha": 1,
    "lDummy": False
}

# METEO
meteo_info = {
    "Radiation": 1,
    "Penman-Hargreaves": "f",
    "lEnBal": True,
    "lDaily": False,
    "Latitude": 3.385000e+01,
    "Altitude": 306,
    "ShortWaveRadA": 0.25,
    "ShortWaveRadB": 0.50,
    "LongWaveRadA": 0.90,
    "LongWaveRadB": 0.10,
    "LongWaveRadA1": 0.34,
    "LongWaveRadB1": -0.139,
    "WindHeight": 200,
    "TempHeight": 200,
    "iCrop": 0,
    "SunShine": 3,
    "RelativeHum": 0,
    "cloud": [1.35, -0.35],
    "Albedo": 0.23
}

# ATMOSPHERE
atmosphere_info = {
    "lDailyVar": False,
    "lSinusVar": False,
    "lLai": False,
    "lBCCycles": False,
    "lInterc": False,
    "lExtinct": 0.463,
    "hCritS": 0,
}

# WATER FLOW
water_flow = {
    "MaxIt": 1250,
    "TolTh": 0.010,
    "TolH": 5,
    "TopInf": True,
    "WLayer": True,
    "KodTop": -1,
    "InitCond": True,
    "BotInf": False,
    "qGWLF": False,
    "FreeD": True,
    "SeepF": False,
    "KodBot": -1,
    "DrainF": False,
    "hSeep": 0,
    "hTab1": 1e-005,
    "hTabN": 100000,
    "Model": 0,
    "Hysteresis": 0
}

# MATERIALS
materials_df = pd.DataFrame(columns=["thr", "ths", "Alfa", "n", "Ks", "l"],
                            data=[[0.062, 0.399, 0.0119, 1.48, 13.092, 0.5]],
                            index=[0])
materials = {"water": materials_df}

# TIME
time_info = {
    "dt": 0.003,
    "dtMin": 0.00001,
    "dtMax": 0.14,
    "dMul": 1.3,
    "dMul2": 0.7,
    "ItMin": 3,
    "ItMax": 7,
    "MPL": 1,
    "tInit": 107,
    "tMax": 151,
    "lPrintD": False,
    "nPrintSteps": 1,
    "tPrintInterval": 1,
    "lEnter": True
}

times = [150.90]

# HEAT TRANSPORT
heat_transport_info = {
    "tAmpl": 0,
    "tPeriod": 0,
    "Campbell": 2,
    "SnowMF": 0.43,
    "kTopT": -1,
    "TTop": 20,
    "kBotT": 0,
    "TBot": 20
}

heat_params = pd.DataFrame(columns=["Qn", "Qo", "Disper.", "B1", "B2", "B3", "Cn", "Co", "Cw"],
                           data=[[0.60, 0, 0.001000, 3.731290e-01, 5.194220e-01, 1.666762e+00,
                                  183327000000000, 187370000000000, 312035000000000]],
                           index=[0])

# PROFILE
nlay = 101

# CLM

stdv = 0.2  # for varying alpha and tau

clm_info = {
    "xL": 0.3,  # mentioned separately in matlab script
    "LAI": 1.82,  # varied in ensembles
    "weight": 0.99,
    "T3 vegetation?": 1.00,
    "Vcmax25": 10.0,  # mentioned separately in matlab script
    "ra_photo": 4.32,  # varied in ensembles
    "rb_photo": 15.0,  # mentioned separately in matlab script
    "Latitude": 35.50,
    "Longitude": -98.00,
    "Timezone": -6.00,
    "alpha_leaf": [0.17, 0.55],  # varied in ensembles
    "alpha_stem": [0.29, 0.50],  # varied in ensembles
    "tau_leaf": [0.06, 0.39],  # varied in ensembles
    "tau_stem": [0.11, 0.23],  # varied in ensembles
    "Zatm": 2.00,
    "Zom": 0.06,
    "d": 0.34,
    "Ztop": 0.50,
    "CNL": 25.00,
    "SLA0": 0.03,
    "SLAm": 0.00,
    "m_photo": 5.00,
    "alpha_wj": 0.04,
    "FN": 0.64,
    "FLNR": 0.09,
    "fai_c": -1500000.00,
    "fai_o": -74000.00,
    "field_cap": 0.30
}

# FILES
meteofile = "../../data/PBS_HYDRUS/Likelihoodfunctions/RealDTS/MeteroData.mat"
atmofile = "../../data/PBS_HYDRUS/Likelihoodfunctions/RealDTS/Hydra_mois_B.mat"
fprofile = "../../data/PBS_Hydrus/Likelihoodfunctions/profile_mode.txt"


# %% METEO.IN

if "METEO" in writelist:
    meteodata = loadmat(meteofile)
    meteodata = meteodata["MeteroData"]

    # use only data where these conditions apply
    # (opposite from m-file which deletes all unused data)
    mask = ((meteodata[:, 0] > start_time + start_date_num) &
            (meteodata[:, 0] < end_time + start_date_num))

    cols = ["t", "Rad", "TMax", "RHMean", "Wind"]
    meteodf = pd.DataFrame(meteodata[mask, :], columns=cols)
    meteodf["t"] -= start_date_num
    meteodf["TMin"] = meteodf["TMax"]
    meteodf["SunHours"] = 0.6
    reorder = ["t", "Rad", "TMax", "TMin", "RHMean", "Wind",
               "SunHours", "CropHeight", "Albedo", "LAI(SCF)", "rRoot"]
    meteodf = meteodf.reindex(columns=reorder, fill_value="")

    # doesn't do anything:
    mask = meteodf["Rad"] > -10
    meteodf = meteodf.loc[mask]

    # only every 6
    meteodf = meteodf.iloc[::6]

    for i in (tqdm(range(ensemble_size), desc="METEO") if not verbose
              else range(ensemble_size)):
        meteo_ens = meteodf.copy()
        m, n = meteo_ens.shape
        meteo_ens["Rad"] = (meteo_ens["Rad"] *
                            (1 + 0.075 * np.random.randn(m))).clip(upper=114)
        meteo_ens["TMax"] = meteo_ens["TMax"] + 0.5 * np.random.randn(m)
        # TMin remains the same
        meteo_ens["RHMean"] = (meteo_ens["RHMean"] *
                               (1 + 0.1 * np.random.randn(m))).clip(upper=100)
        meteo_ens["Wind"] = meteo_ens["Wind"] + (1 + 0.1 * np.random.randn(m))
        # Sunhours remains the same

        # smooth
        # MATLAB says 4 in second param, but it is changed to 3 internally
        meteo_ens["Rad"] = smooth(meteo_ens["Rad"], 3)
        for col in ["TMax", "TMin", "RHMean", "Wind", "SunHours"]:
            # MATLAB says 8, but it is 7 (see reason in comment above)
            meteo_ens[col] = smooth(meteo_ens[col], 7)

        # conversion mph to km/day
        meteo_ens["Wind"] *= 193.2682

        # write file
        write_meteo(basic_info, meteo_info, meteo_ens, fname=f"METEO{i+1}.IN",
                    ws=ws, verbose=verbose)


# %% ATMOSPH.IN

if "ATMOSPH" in writelist:
    atmodata = loadmat(atmofile)
    atmodata = atmodata["Hydra_mois_B"]

    mask = ((atmodata[:, 0] > start_date_num + start_time) &
            (atmodata[:, 0] < start_date_num + end_time))

    # in MATLAB 11 columns are written, but ATMOSPH.IN file has 12:
    # filling in 0.0 in last column, assuming it won't matter...
    cols = ["tAtm", "Prec", "rSoil", "rRoot", "hCritA",
            "rB", "hB", "hTop", "tBot", "Ampl", "RootDepth"]
    atmodf = pd.DataFrame(index=range(mask.sum()), columns=cols, data=0.0)

    atmodf["tAtm"] = atmodata[mask, 0] - start_date_num
    atmodf["Prec"] = atmodata[mask, 1].clip(min=0) * 1440 / (60 * 10)
    atmodf["hCritA"] = 1e6

    for i in (tqdm(range(ensemble_size), desc="ATMOSPH") if not verbose
              else range(ensemble_size)):
        atmo_ens = atmodf.copy()
        m, n = atmo_ens.shape
        atmo_ens["Prec"] *= (1 + 0.1 * np.random.randn(m))

        # write atmosph.in
        write_atmosphere(basic_info, atmosphere_info, atmo_ens,
                         fname=f"ATMOSPH{i+1}.IN", ws=ws, verbose=verbose)

# %% SELECTOR.IN

# Provided in MATLAB but seemingly unsused
# t_step = 0.01
# sand = 30.9
# clay = 36.4
# silt = 100 - sand - clay
# bulk_density = 1.47
if "SELECTOR" in writelist:
    sand_percentage = np.random.rand(ensemble_size)
    cor = 0.0
    sand = sand_percentage * 55 + 10
    silt = (100 - sand) * (0.9 * np.random.rand() + 0.1)
    clay = 100 - silt - sand
    bulk_density = 1.2 + 0.5 * \
        (sand_percentage * 0.3 + (1 - cor) * np.random.rand())

    # alpha bounds
    alpha1 = np.array([0, 100])
    alpha2 = np.array([0.27, 0.96])

    spl = interpolate.UnivariateSpline(alpha1, alpha2, k=1)

    for i in (tqdm(range(ensemble_size), desc="SELECTOR") if not verbose
              else range(ensemble_size)):
        # TODO: NOT WORKING! ALWAYS GETS THE SAME FILE.
        soil_hydrology = get_roeseta_params(sand[i], silt[i], bulk_density[i],
                                            "cm", "day")
        porosity = 1 - soil_hydrology[1]
        alpha = spl(sand[i])

        q = sand[i] / 100
        if q < 0.2:
            lab_0 = 3
        else:
            lab_0 = 2

        lab_w = 0.594
        lab_q = 7.7

        lab_s = lab_q**q * lab_0**(1 - q)
        lab_sat = lab_s**porosity * lab_w**(1 - porosity)

        soil_thermal = np.array([porosity,
                                 0.,
                                 0.001,
                                 sand[i] / 100.,
                                 # porosity - sand[i] / 100. * porosity,
                                 alpha,
                                 lab_sat,  # clay[i] / 100 * porosity,
                                 1.83327e+014 * 1.,
                                 1.8737e+014,
                                 3.12035e+014])

        # write selector.in
        write_selector(basic_info, time_info, times, water_flow,
                       materials, heat_transport_info, heat_params,
                       n_materials=1, n_layers=1,
                       fname=f"SELECTOR_LU{i+1}.IN", ws=ws,
                       verbose=verbose)

# %% PROFILE.DAT

if "PROFILE" in writelist:
    profile_mode = np.loadtxt(fprofile)
    profile_mode[:, 1] = np.arange(0, -101, -1)
    cols = ['x', 'h', 'Mat', 'Lay', 'Beta',
            'Axz', 'Bxz', 'Dxz', 'Temp', 'Conc']
    profdf = pd.DataFrame(profile_mode, columns=cols)
    profdf["Axz"] = 0

    for icol in ['x', 'Lay', 'Beta', 'Axz', 'Bxz', 'Dxz', 'Temp']:
        profdf[icol] = profdf[icol].astype(int)

    SMp = np.zeros((nlay, ensemble_size))
    TMp = np.zeros((nlay, ensemble_size))

    for i in (tqdm(range(ensemble_size), desc="PROFILE") if not verbose
              else range(ensemble_size)):
        SMp[:, i] = np.linspace(0.15 + 0.15 * np.random.rand(),
                                0.2 + 0.01 * np.random.rand(), nlay)
        TMp[:, i] = np.linspace(20 + 1.0 * np.random.randn(),
                                16 + 1.0 * np.random.rand(), nlay)

        prof_ens = profdf.copy()
        prof_ens["Conc"] = TMp[:, i]
        prof_ens["Mat"] = SMp[:, i]

        obs_nodes = [3, 6, 11, 21, 51, 1]

        # write profile.dat
        write_profile(basic_info, prof_ens, obs_nodes, fname=f"PROFILE{i+1}.DAT",
                      ws=ws, verbose=verbose)

# %% CLM_INPUT.IN
if "CLM" in writelist:

    LAI = 0.5 + 3 * np.random.rand(ensemble_size)  # assumed LAI, site A 1st
    ra_photo = 2 + 3 * np.random.rand(ensemble_size)

    # root distribution
    root_dist = pd.DataFrame(index=range(
        nlay), data=0.0, columns=["root_dist"])
    root_dist.iloc[:3, 0] = [0.86, 0.12, 0.02]

    # alpha and tau
    alpha_leaf = np.array([0.11, 0.35]) * (1 + np.random.randn() * stdv)
    alpha_stem = np.array([0.31, 0.53]) * (1 + np.random.randn() * stdv)
    tau_leaf = np.array([0.05, 0.34]) * (1 + np.random.randn() * stdv)
    tau_stem = np.array([0.12, 0.25]) * (1 + np.random.randn() * stdv)

    clm_info.update({"alpha_leaf": list(alpha_leaf),
                     "alpha_stem": list(alpha_stem),
                     "tau_leaf": list(tau_leaf),
                     "tau_stem": list(tau_stem)})

    for i in (tqdm(range(ensemble_size), desc="CLM") if not verbose
              else range(ensemble_size)):
        clm_info.update({"LAI": LAI[i], "ra_photo": ra_photo[i]})
        write_clm(clm_info, root_dist, fname=f"CLM_INPUTS{i+1}.IN", ws=ws,
                  verbose=verbose)
