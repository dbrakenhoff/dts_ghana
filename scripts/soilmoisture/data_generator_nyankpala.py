import os

import numpy as np
import pandas as pd
import scipy.interpolate as interpolate
from scipy import interpolate as interpolate
from tqdm import tqdm

import dtscalibration as dts
from util import get_roeseta_params, smooth
from write_atmosph import write_atmosphere
from write_clm import write_clm
from write_meteo import write_meteo
from write_profile import write_profile
from write_selector import write_selector

# %% Script control

verbose = False  # if True, prints messages when files are succesfully written
ws = "D:/PassiveRealDTS"  # hard-coded in Hydrus.exe

if not os.path.exists("D:/"):
    # map folder to drive D:/
    os.system("subst D: ./hydrus_io")
    # create input file directory
    if not os.path.exists(ws):
        os.mkdir(ws)

writelist = ["METEO", "ATMOSPH", "SELECTOR", "PROFILE", "CLM"]
# writelist = ["PROFILE"]

ensemble_size = 300  # hard-coded into Hydrus.exe

# LOAD DATA

# tahmo
tahmo = df = pd.read_csv("../../data/tahmo/TA00616.csv",
                         parse_dates=True, index_col=[0])
meta = pd.read_csv("../../data/tahmo/metadata.csv").squeeze()

smcols = ['soilmoisture S000988 (m3/m3)',  # middle?
          'soilmoisture S000989 (m3/m3)',  # shallowest?
          'soilmoisture S000990 (m3/m3)']  # deepest

stcols = ['soiltemperature S000988 (degrees Celsius)',  # middle?
          'soiltemperature S000989 (degrees Celsius)',  # shallowest?
          'soiltemperature S000990 (degrees Celsius)']  # deepest

# dts
channel = "ch1"
calibstr = "ambientearth"
fnetcdf = f"../../data/nc/nyankpala_campaign1_{channel}_calib_{calibstr}.nc"
ds = dts.open_datastore(fnetcdf)

times = ds.time.to_pandas().values  # DTS times
tstart = pd.Timestamp(times[0])  # first DTS obs
t0 = tstart.floor("D")  # in Hydrus t=0 is midnight, so get midnight before
tstart_num = (tstart - t0).to_numpy() / np.timedelta64(1, "D")
tend = pd.Timestamp(times[-1])
dt_in_days = (tend - tstart).total_seconds() / (24 * 60 * 60)
tend_num = tstart_num + dt_in_days

# added to tend for writing TAHMO data:
# adds extra data in METEO and ATMOSPH
tdelta = pd.Timedelta(days=0.5)

# BASIC INFO
start_time = tstart_num
end_time = tend_num

latitude = meta["latitude"]
longitude = meta["longitude"]
timezone = 0

basic_info = {
    "iVer": 4,
    "Hed": "Written with methods derived from Pydrus 0.0.2",
    "LUnit": "cm",
    "TUnit": "days",
    "MUnit": "mmol",
    "lWat": True,
    "lChem": False,
    "lTemp": True,
    "lSink": False,
    "lRoot": False,
    "lShort": False,
    "lWDep": True,
    "lScreen": False,
    "lVariabBC": True,
    "lEquil": True,
    "lInverse": False,
    "lSnow": False,
    "lHP1": False,
    "lMeteo": True,
    "lVapor": True,
    "lActiveU": False,
    "lFluxes": False,
    "lIrrig": False,
    "CosAlpha": 1,
    "lDummy": False
}

# METEO
meteo_info = {
    "Radiation": 1,
    "Penman-Hargreaves": "f",
    "lEnBal": True,
    "lDaily": False,
    "Latitude": latitude,
    "Altitude": meta["elevation (m)"],
    "ShortWaveRadA": 0.25,
    "ShortWaveRadB": 0.50,
    "LongWaveRadA": 0.90,
    "LongWaveRadB": 0.10,
    "LongWaveRadA1": 0.34,
    "LongWaveRadB1": -0.139,
    "WindHeight": 250,
    "TempHeight": 250,
    "iCrop": 0,
    "SunShine": 3,
    "RelativeHum": 0,
    "cloud": [1.35, -0.35],
    "Albedo": 0.23
}

# ATMOSPHERE
atmosphere_info = {
    "lDailyVar": False,
    "lSinusVar": False,
    "lLai": False,
    "lBCCycles": False,
    "lInterc": False,
    "lExtinct": 0.463,
    "hCritS": 0,
}

# WATER FLOW
water_flow = {
    "MaxIt": 1250,
    "TolTh": 0.010,
    "TolH": 5,
    "TopInf": True,
    "WLayer": True,
    "KodTop": -1,
    "InitCond": True,
    "BotInf": False,
    "qGWLF": False,
    "FreeD": True,
    "SeepF": False,
    "KodBot": -1,
    "DrainF": False,
    "hSeep": 0,
    "hTab1": 1e-005,
    "hTabN": 100000,
    "Model": 0,
    "Hysteresis": 0
}

# MATERIALS
materials_df = pd.DataFrame(columns=["thr", "ths", "Alfa", "n", "Ks", "l"],
                            data=[[0.062, 0.399, 0.0119, 1.48, 13.092, 0.5]],
                            index=[0])
materials = {"water": materials_df}

# TIME
time_info = {
    "dt": 0.005,  # 0.003,
    "dtMin": 0.001,  # 0.00001,
    "dtMax": 0.14,
    "dMul": 1.3,
    "dMul2": 0.7,
    "ItMin": 3,
    "ItMax": 7,
    "MPL": 1,
    "tInit": tstart_num,
    "tMax": tend_num,
    "lPrintD": False,
    "nPrintSteps": 1,
    "tPrintInterval": 1,
    "lEnter": True
}

times = [tend_num]

# HEAT TRANSPORT
heat_transport_info = {
    "tAmpl": 0,
    "tPeriod": 0,
    "Campbell": 2,
    "SnowMF": 0.43,
    "kTopT": -1,
    "TTop": 20,
    "kBotT": 0,
    "TBot": 20
}

heat_params = pd.DataFrame(columns=["Qn", "Qo", "Disper.", "B1", "B2", "B3", "Cn", "Co", "Cw"],
                           data=[[0.60, 0, 0.001000, 3.731290e-01, 5.194220e-01, 1.666762e+00,
                                  183327000000000, 187370000000000, 312035000000000]],
                           index=[0])

# PROFILE
nlay = 101
obs_nodes = [1, 5, 10, 15, 25, 50]

# initial states soil temperature and moisture at top and bottom of profile
soiltemp_top_init = 28
st_top_randomfactor = 2.0  # for scaling random added values
soiltemp_btm_init = 23
st_btm_randomfactor = 1.0  # for scaling random added values
soilmois_top_init = 0.22
sm_top_randomfactor = 0.10  # for scaling random added values
soilmois_btm_init = 0.30
sm_btm_randomfactor = 0.01  # for scaling random added values

# CLM
stdv = 0.2  # for varying alpha and tau
lai_random_uniform_factor = 2  # multiply random uniform for LAI ensemble
raphoto_random_uniform_factor = 3  # multiply random uniform for ra_photo ens.

clm_info = {
    "xL": 0.3,  # mentioned separately in matlab script
    "LAI": 0.25,  # varied in ensembles by adding random value [0, 3]
    "weight": 0.99,
    "T3 vegetation?": 1.00,
    "Vcmax25": 10.0,  # mentioned separately in matlab script
    "ra_photo": 2.0,  # varied in ensembles by adding random value [0, 3]
    "rb_photo": 15.0,  # mentioned separately in matlab script
    "Latitude": latitude,
    "Longitude": longitude,
    "Timezone": timezone,
    "alpha_leaf": [0.17, 0.55],  # varied in ensembles
    "alpha_stem": [0.29, 0.50],  # varied in ensembles
    "tau_leaf": [0.06, 0.39],  # varied in ensembles
    "tau_stem": [0.11, 0.23],  # varied in ensembles
    "Zatm": meta["installation height (m)"],
    "Zom": 0.06,
    "d": 0.34,
    "Ztop": 0.50,
    "CNL": 25.00,
    "SLA0": 0.03,
    "SLAm": 0.00,
    "m_photo": 5.00,
    "alpha_wj": 0.04,
    "FN": 0.64,
    "FLNR": 0.09,
    "fai_c": -1500000.00,
    "fai_o": -74000.00,
    "field_cap": 0.30
}

# FILES
# meteofile = "../../data/PBS_HYDRUS/Likelihoodfunctions/RealDTS/MeteroData.mat"
# atmofile = "../../data/PBS_HYDRUS/Likelihoodfunctions/RealDTS/Hydra_mois_B.mat"
fprofile = "../../data/PBS_Hydrus/Likelihoodfunctions/profile_mode.txt"

# %% METEO.IN

if "METEO" in writelist:
    meteodata = tahmo.loc[tstart:tend + tdelta]
    cols = ["t", "Rad", "TMax", "Tmin", "RHMean", "Wind"]
    meteodf = pd.DataFrame(columns=cols, index=range(meteodata.index.size))
    meteodf["t"] = (meteodata.index - tstart).to_numpy() \
        / np.timedelta64(1, "D") + tstart_num
    meteodf["TMax"] = meteodata['temperature (degrees Celsius)'].values
    meteodf["TMin"] = meteodata['temperature (degrees Celsius)'].values
    meteodf["SunHours"] = 0.6
    # convert from W/m2 to MJ/day/m2
    meteodf["Rad"] = meteodata['radiation (W/m2)'].values * \
        (24 * 60 * 60) / 1e6
    meteodf['RHMean'] = meteodata['relativehumidity (-)'].values
    # convert from m/s to km/day
    meteodf["Wind"] = meteodata['windspeed (m/s)'].values * \
        (24 * 60 * 60) / (1e3)

    reorder = ["t", "Rad", "TMax", "TMin", "RHMean", "Wind",
               "SunHours", "CropHeight", "Albedo", "LAI(SCF)", "rRoot"]
    meteodf = meteodf.reindex(columns=reorder, fill_value="")

    # use only every 6 steps
    # meteodf = meteodf.iloc[::6]

    for i in (tqdm(range(ensemble_size), desc="METEO") if not verbose
              else range(ensemble_size)):
        meteo_ens = meteodf.copy()
        m, n = meteo_ens.shape
        meteo_ens["Rad"] = (meteo_ens["Rad"] *
                            (1 + 0.075 * np.random.randn(m)))  # .clip(upper=114)
        meteo_ens["TMax"] = meteo_ens["TMax"] + 0.5 * np.random.randn(m)
        # TMin remains the same
        meteo_ens["RHMean"] = (meteo_ens["RHMean"] *
                               (1 + 0.1 * np.random.randn(m))).clip(upper=100)
        meteo_ens["Wind"] = meteo_ens["Wind"] + (1 + 0.1 * np.random.randn(m))
        # Sunhours remains the same

        # smooth
        # MATLAB script uses 4 as second param, but its changed to 3 internally
        meteo_ens["Rad"] = smooth(meteo_ens["Rad"], 3)
        for col in ["TMax", "TMin", "RHMean", "Wind", "SunHours"]:
            # MATLAB uses 8, but it is 7 (see reason in comment above)
            meteo_ens[col] = smooth(meteo_ens[col], 7)

        # write file
        write_meteo(basic_info, meteo_info, meteo_ens, fname=f"METEO{i+1}.IN",
                    ws=ws, verbose=verbose)

# %% ATMOSPH.IN

if "ATMOSPH" in writelist:
    atmodata = tahmo.loc[tstart:tend + tdelta]

    # in MATLAB 11 columns are written, but ATMOSPH.IN file has 12:
    # filling in 0.0 in last column, assuming it won't matter...
    cols = ["tAtm", "Prec", "rSoil", "rRoot", "hCritA",
            "rB", "hB", "hTop", "tBot", "Ampl", "RootDepth"]
    atmodf = pd.DataFrame(index=atmodata.index, columns=cols, data=0.0)

    atmodf["tAtm"] = (atmodata.index - tstart).to_numpy() \
        / np.timedelta64(1, "D") + tstart_num
    # set first t to 0, so data starts at same time as model
    # atmodf.loc[atmodf.index[0], "tAtm"] = 0.0

    # prec is mm per 5 minutes: convert to cm/day
    atmodf["Prec"] = atmodata.loc[:, 'precipitation (mm)'] * (12 * 24) / 10
    atmodf["hCritA"] = 1e6

    # use only every 6 obs TODO: resample properly!
    # atmodf = atmodf.iloc[::6]

    for i in (tqdm(range(ensemble_size), desc="ATMOSPH") if not verbose
              else range(ensemble_size)):
        atmo_ens = atmodf.copy()
        m, n = atmo_ens.shape
        atmo_ens["Prec"] *= (1 + 0.1 * np.random.randn(m))

        # write atmosph.in
        write_atmosphere(basic_info, atmosphere_info, atmo_ens,
                         fname=f"ATMOSPH{i+1}.IN", ws=ws, verbose=verbose)

# %% SELECTOR.IN

# Provided in MATLAB but seemingly unsused
# t_step = 0.01
# sand = 30.9
# clay = 36.4
# silt = 100 - sand - clay
# bulk_density = 1.47

if "SELECTOR" in writelist:
    sand_percentage = np.random.rand(ensemble_size)
    cor = 0.0
    sand = sand_percentage * 55 + 10
    silt = (100 - sand) * (0.9 * np.random.rand() + 0.1)
    clay = 100 - silt - sand
    bulk_density = 1.2 + 0.5 * \
        (sand_percentage * 0.3 + (1 - cor) * np.random.rand())

    # alpha bounds
    alpha1 = np.array([0, 100])
    alpha2 = np.array([0.27, 0.96])

    spl = interpolate.UnivariateSpline(alpha1, alpha2, k=1)

    for i in (tqdm(range(ensemble_size), desc="SELECTOR") if not verbose
              else range(ensemble_size)):
        # TODO: NOT WORKING! ALWAYS GETS THE SAME FILE.
        soil_hydrology = get_roeseta_params(sand[i], silt[i], bulk_density[i],
                                            "cm", "day")
        porosity = 1 - soil_hydrology[1]
        alpha = spl(sand[i])

        q = sand[i] / 100
        if q < 0.2:
            lab_0 = 3
        else:
            lab_0 = 2

        lab_w = 0.594
        lab_q = 7.7

        lab_s = lab_q**q * lab_0**(1 - q)
        lab_sat = lab_s**porosity * lab_w**(1 - porosity)

        soil_thermal = np.array([porosity,
                                 0.,
                                 0.001,
                                 sand[i] / 100.,
                                 # porosity - sand[i] / 100. * porosity,
                                 alpha,
                                 lab_sat,  # clay[i] / 100 * porosity,
                                 1.83327e+014 * 1.,
                                 1.8737e+014,
                                 3.12035e+014])

        # write selector.in
        write_selector(basic_info, time_info, times, water_flow,
                       materials, heat_transport_info, heat_params,
                       n_materials=1, n_layers=1,
                       fname=f"SELECTOR_LU{i+1}.IN", ws=ws,
                       verbose=verbose)

# %% PROFILE.DAT

if "PROFILE" in writelist:
    # column names
    cols = ['x', 'h', 'Mat', 'Lay', 'Beta',
            'Axz', 'Bxz', 'Dxz', 'Temp', 'Conc']

    # create dataframe
    profdf = pd.DataFrame(columns=cols, index=range(nlay))

    # set values (some of this data should probably be defined in the Input data section)
    profdf["x"] = np.arange(1, nlay + 1, dtype=int)
    profdf["h"] = np.arange(0, -nlay, -1, dtype=float)
    profdf["Lay"] = 1
    profdf["Beta"] = 1
    profdf["Axz"] = 0
    profdf["Bxz"] = 1
    profdf["Dxz"] = 1
    profdf["Temp"] = 1

    # Initial profiles
    SMp = np.zeros((nlay, ensemble_size))
    TMp = np.zeros((nlay, ensemble_size))

    for i in (tqdm(range(ensemble_size), desc="PROFILE") if not verbose
              else range(ensemble_size)):
        SMp[:, i] = np.linspace(soilmois_top_init +
                                sm_top_randomfactor * (np.random.rand() - 0.5),
                                soilmois_btm_init +
                                sm_btm_randomfactor * (np.random.rand() - 0.5),
                                nlay)
        TMp[:, i] = np.linspace(soiltemp_top_init +
                                st_top_randomfactor * np.random.randn(),
                                soiltemp_btm_init +
                                st_btm_randomfactor * np.random.randn(),
                                nlay)

        prof_ens = profdf.copy()
        prof_ens["Conc"] = TMp[:, i]
        prof_ens["Mat"] = SMp[:, i]

        # write profile.dat
        write_profile(basic_info, prof_ens, obs_nodes, fname=f"PROFILE{i+1}.DAT",
                      ws=ws, verbose=verbose)

# %% CLM_INPUT.IN
if "CLM" in writelist:

    LAI = clm_info["LAI"] + lai_random_uniform_factor * \
        np.random.rand(ensemble_size)  # assumed LAI
    ra_photo = clm_info["ra_photo"] + \
        raphoto_random_uniform_factor * np.random.rand(ensemble_size)

    # root distribution
    root_dist = pd.DataFrame(index=range(
        nlay), data=0.0, columns=["root_dist"])
    root_dist.iloc[:3, 0] = [0.86, 0.12, 0.02]

    # alpha and tau
    alpha_leaf = np.array([0.11, 0.35]) * (1 + np.random.randn() * stdv)
    alpha_stem = np.array([0.31, 0.53]) * (1 + np.random.randn() * stdv)
    tau_leaf = np.array([0.05, 0.34]) * (1 + np.random.randn() * stdv)
    tau_stem = np.array([0.12, 0.25]) * (1 + np.random.randn() * stdv)

    clm_info.update({"alpha_leaf": list(alpha_leaf),
                     "alpha_stem": list(alpha_stem),
                     "tau_leaf": list(tau_leaf),
                     "tau_stem": list(tau_stem)})

    for i in (tqdm(range(ensemble_size), desc="CLM") if not verbose
              else range(ensemble_size)):
        clm_info.update({"LAI": LAI[i], "ra_photo": ra_photo[i]})
        write_clm(clm_info, root_dist, fname=f"CLM_INPUTS{i+1}.IN", ws=ws,
                  verbose=verbose)
