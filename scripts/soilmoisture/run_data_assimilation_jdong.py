import os
import sys

from subprocess import PIPE, Popen
import numpy as np
import pandas as pd
from scipy import interpolate
from scipy.io import loadmat
import matplotlib.pyplot as plt

from util import datetime_to_matlab


# %% Load DTS data
tempfinal_fname = ("../../data/PBS_HYDRUS/Likelihoodfunctions/"
                   "RealDTS/TempFinal.mat")
tempfinal = loadmat(tempfinal_fname)
tempfinal = tempfinal["TempFinal"]

# time settings
start_date = pd.Timestamp("2011-01-01")
start_date_num = datetime_to_matlab(start_date)

# get specific datasets
time = tempfinal["Time"][0, 0].squeeze() - 10.5 / 24
t5 = tempfinal["T5"][0, 0].squeeze() - 273.15
t10 = tempfinal["T10"][0, 0].squeeze() - 273.15
t15 = tempfinal["T15"][0, 0].squeeze() - 273.15

# model time
mtime = np.arange(time[0], time[-1], 1 / 24.)

# model ws
os.system("subst D:/ ./hydrus_io")
ws = "D:/PassiveRealDTS"
if not os.path.isdir(ws):
    os.mkdir(ws)

hydrus_exe = "C:/gitlab/dts_ghana/hydrus_src/bin/hydrus_realdts.exe"

# loop over each x location on cable
for i in range(1):

    # get observations for all t at xi
    obs_at_xi = np.c_[t5[:, i], t10[:, i], t15[:, i]]

    # use a spline to interpolate obs
    spl = interpolate.PchipInterpolator(time, obs_at_xi, axis=0)
    obs_spline_at_xi = spl(mtime)

    # write file with obs
    obsdf = pd.DataFrame(index=mtime, columns=["T5", "T10", "T15"],
                         data=obs_spline_at_xi)

    obsdf_w_noise = obsdf + 0.0 * np.random.randn(*obsdf.shape)

    obsdf = pd.concat([obsdf, obsdf_w_noise], axis=1)

    obsdf.to_csv(os.path.join(ws, "obs_siteA.txt"), header=False,
                 float_format="%.5f")

    # run HYDRUS
    p = Popen([hydrus_exe], stdout=PIPE, stderr=PIPE)
    while p.poll() is None:
        text = p.stdout.readline()
        sys.stdout.write(text.decode("utf-8"))

    # Post-processing
    resultsdir = "D:/"

    # Soil and temperature data
    data = np.loadtxt(os.path.join(resultsdir, "FinallResultsES1.txt"))
    m, n = data.shape
    N_ens = int((n - 1) / 2)  # no. of ensembles

    # last_idx = np.floor(m / 6) * 6  # round so there is multiple of six
    # data = data[:last_idx, :]

    idx = np.arange(0, m, 6)
    time = data[idx, 0]
    M = {}
    T = {}
    Mmean = np.zeros((len(time), 6))
    Tmean = np.zeros((len(time), 6))

    for j in range(6):
        m = data[idx + j - 1, 1:N_ens + 1]
        t = data[idx + j - 1, N_ens + 1:]
        M[f"M{j+1}"] = m
        T[f"T{j+1}"] = t
        Mmean[:, j] = m.mean(axis=1)
        Tmean[:, j] = t.mean(axis=1)

    # STP Par
    STPPar = np.loadtxt(os.path.join(resultsdir, "FinallResultsESPar1.txt"))
    m, n = STPPar.shape
    idx = np.arange(0, m, 4)
    par1 = STPPar[idx + 0, :]
    par2 = STPPar[idx + 1, :]
    par3 = STPPar[idx + 2, :]
    par4 = STPPar[idx + 3, :]

    par_mean = [
        par1[-1, :].mean(),
        par2[-1, :].mean(),
        par3[-1, :].mean(),
        par4[-1, :].mean(),
    ]

    # SHP Par
    SHPPar = np.loadtxt(os.path.join(
        resultsdir, "FinallResultsESPar_SHP1.txt"))
    m, n = SHPPar.shape
    idx = np.arange(0, m, 7)
    parh1 = SHPPar[idx + 0, 1:]
    parh2 = SHPPar[idx + 1, 1:]
    parh3 = SHPPar[idx + 2, 1:]
    parh4 = SHPPar[idx + 3, 1:]
    parh5 = SHPPar[idx + 4, 1:]
    lai = SHPPar[idx + 5, 1:]
    vcmax = SHPPar[idx + 6, 1:]

    # Cable depths
    cable_depths = np.loadtxt(os.path.join(
        resultsdir, "FinalCableDepths1.txt"))
    m, n = cable_depths.shape
    idx = np.arange(0, m, 2)
    cable1 = cable_depths[idx + 0, :]
    cable2 = cable_depths[idx + 1, :]

    # Flux
    flux = np.loadtxt(os.path.join(resultsdir, "FinallResultsESFlux1.txt"))
    m, n = flux.shape
    idx = np.arange(0, m, 5)
    flux_hv = flux[idx + 0, 1:]
    flux_lev = flux[idx + 1, 1:]
    flux_hg = flux[idx + 2, 1:]
    flux_leg = flux[idx + 3, 1:]
    flux_g = flux[idx + 4, 1:]

    # Profile and Tuning
    profile = np.loadtxt(os.path.join(resultsdir, "FinalProfile1.txt"))
    tuning = np.loadtxt(os.path.join(resultsdir, "FinalTuning1.txt"))


# Plot soil moisture (probe vs ensemble)
sm_fname = ("../../data/PBS_HYDRUS/Likelihoodfunctions/"
            "RealDTS/Hydra_mois_B.mat")
sm_sensor = loadmat(sm_fname)
sm_sensor = sm_sensor["Hydra_mois_B"]

fig, ax = plt.subplots(1, 1, figsize=(12, 5), dpi=100)
for n in range(N_ens):
    p, = ax.plot(time, M["M2"][:, n], color="gray", lw=0.5)

e, = ax.plot(time, Mmean[:, 1], color="C3")

mask = ((sm_sensor[:, 0] - start_date_num >= time[0]) &
        (sm_sensor[:, 0] - start_date_num <= time[-1]))
a, = ax.plot(sm_sensor[mask, 0] - start_date_num,
             sm_sensor[mask, 3], color="C0")


ax.legend([p, e, a], ["ensemble", "estimate", "probe"], loc="best")

ax.grid(b=True)
ax.set_ylabel(r"$\theta$ (-)")
ax.set_xlim(time[0], time[-1])
ax.set_xlabel("time (days)")

plt.show()

# Plot soil temperature (probe vs ensemble)
t_fname = ("../../data/PBS_HYDRUS/Likelihoodfunctions/"
            "RealDTS/HydraB.mat")
t_sensor = loadmat(t_fname)
t_sensor = t_sensor["HydraB"]

fig, ax = plt.subplots(1, 1, figsize=(12, 5), dpi=100)
for n in range(N_ens):
    p, = ax.plot(time, T["T2"][:, n], color="gray", lw=0.5)

e, = ax.plot(time, Tmean[:, 1], color="C3")

mask = ((t_sensor[:, 0] - start_date_num >= time[0]) &
        (t_sensor[:, 0] - start_date_num <= time[-1]))
a, = ax.plot(t_sensor[mask, 0] - start_date_num,
             t_sensor[mask, 3], color="C0")


ax.legend([p, e, a], ["ensemble", "estimate", "probe"], loc="best")

ax.grid(b=True)
ax.set_ylabel(r"temperature ($\deg$ C)")
ax.set_xlim(time[0], time[-1])
ax.set_xlabel("time (days)")

plt.show()
