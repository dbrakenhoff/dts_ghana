import os
import sys
from subprocess import PIPE, Popen

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
# %% Profile
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy import interpolate

import dtscalibration as dts

mpl.interactive(True)

# %% Load data
run = True  # set to False to only do post-processing results

# tahmo
tahmo = df = pd.read_csv("../../data/tahmo/TA00616.csv",
                         parse_dates=True, index_col=[0])
meta = pd.read_csv("../../data/tahmo/metadata.csv").squeeze()

smcols = ['soilmoisture S000989 (m3/m3)',  # shallowest?
          'soilmoisture S000988 (m3/m3)',  # middle?
          'soilmoisture S000990 (m3/m3)']  # deepest

stcols = ['soiltemperature S000989 (degrees Celsius)',  # shallowest?
          'soiltemperature S000988 (degrees Celsius)',  # middle?
          'soiltemperature S000990 (degrees Celsius)']  # deepest

# dts
channel = "ch1"
calibstr = "ambientearth"
fnetcdf = f"../../data/nc/nyankpala_campaign1_{channel}_calib_{calibstr}.nc"
ds = dts.open_datastore(fnetcdf)

dtstimes = ds.time.to_pandas().values  # DTS times
tstart = pd.Timestamp(dtstimes[0])  # first DTS obs
t0 = tstart.floor("D")  # in Hydrus t=0 is midnight, so get midnight before
tstart_num = (tstart - t0).to_numpy() / np.timedelta64(1, "D")
tend = pd.Timestamp(dtstimes[-1])
dt_in_days = (tend - tstart).total_seconds() / (24 * 60 * 60)
tend_num = tstart_num + dt_in_days

# find alignment top and bottom cable (I did it visually)
# (maybe beter to determine by calculating cross-correlation or something?)
xloc = 250  # location to estimate soil moisture
sel_time = 10  # selected time index for check plot
xmid = 630  # estimate of middle of the cable
dx = 37.1  # dx, to align shallow and deeper cables

dsc1 = ds.sel(x=slice(-45, xmid))  # deeper
dsc2 = ds.sel(x=slice(xmid, 1300))  # shallower
dsc2 = dsc2.assign(x=-1 * (dsc2.x - dsc2.x.isel(x=-1)) - dx)

dsc1x = dsc1.sel(x=xloc, method="nearest")  # deeper
dsc2x = dsc2.sel(x=xloc, method="nearest")  # shallower

fig, ax = plt.subplots(1, 1, figsize=(10, 4), dpi=150)
ax.plot(dsc1.isel(time=sel_time).x.values,
        dsc1.isel(time=sel_time).tmpw.values)
ax.plot(dsc2.isel(time=sel_time).x.values,
        dsc2.isel(time=sel_time).tmpw.values)
ax.plot(dsc1x.x, dsc1x.isel(time=sel_time).tmpw, "C0x")
ax.plot(dsc2x.x, dsc2x.isel(time=sel_time).tmpw, "C1x")
ax.grid(b=True)
ax.set_xlabel("x (m)")
ax.set_ylabel("temperature ($\circ$C)")
ax.set_title("Temperature along cable (t = {})".format(
    ds.isel(time=sel_time).time.values))

times = (dtstimes - t0.to_numpy()) / np.timedelta64(1, "D")

dt_in_days = (tend - tstart).total_seconds() / (24 * 60 * 60)
mtime = np.arange(tstart_num, tend_num, 1. / 24.)  # hourly steps

# get specific datasets
t5 = dsc2x.tmpw.values
t10 = dsc1x.tmpw.values
tdummy = np.zeros(times.shape)

# map directory containing model ws to D: drive
# (ws is hard-coded in Hydrus.exe)
os.system("subst D: ./hydrus_io")
ws = "D:/PassiveRealDTS"
if not os.path.isdir(ws):
    os.mkdir(ws)

hydrus_exe = "../../hydrus_src/bin/hydrus_realdts.exe"
# hydrus_exe = "../../hydrus_src/bin/hydrus_openloop.exe"

# loop over each x location on cable (not currently implemented)
# to loop over each x, the DTS obs file has to be re-written in the loop
for i in range(1):

    # get observations for all t at xi
    obs_at_xi = np.c_[tdummy, t5, t10]

    # use a spline to interpolate obs
    spl = interpolate.PchipInterpolator(times, obs_at_xi, axis=0)
    obs_spline_at_xi = spl(mtime)

    # write file with obs
    obsdf = pd.DataFrame(index=mtime, columns=["Tdummy", "T5", "T10"],
                         data=obs_spline_at_xi)

    obsdf_w_noise = obsdf + 0.0 * np.random.randn(*obsdf.shape)

    obsdf = pd.concat([obsdf, obsdf_w_noise], axis=1)

    obsdf.to_csv(os.path.join(ws, "obs_siteA.txt"), header=False,
                 float_format="%.5f")

    # run HYDRUS
    if run:
        p = Popen([hydrus_exe], stdout=PIPE, stderr=PIPE)
        while p.poll() is None:
            text = p.stdout.readline()
            sys.stdout.write(text.decode("utf-8"))

    # Post-processing
    resultsdir = "D:/"

    # Soil and temperature data
    data = np.loadtxt(os.path.join(resultsdir, "FinallResultsES1.txt"))
    m, n = data.shape
    N_ens = int((n - 1) / 2)  # no. of ensembles

    # step every 6 timesteps
    idx = np.arange(0, m, 6)
    time = data[idx, 0]
    M = {}
    T = {}
    Mmean = np.zeros((len(time), 6))
    Tmean = np.zeros((len(time), 6))

    for j in range(6):
        m = data[idx + j - 1, 1:N_ens + 1]
        t = data[idx + j - 1, N_ens + 1:]
        M[f"M{j+1}"] = m
        T[f"T{j+1}"] = t
        Mmean[:, j] = m.mean(axis=1)
        Tmean[:, j] = t.mean(axis=1)

    # STP Par
    STPPar = np.loadtxt(os.path.join(resultsdir, "FinallResultsESPar1.txt"))
    m, n = STPPar.shape
    idx = np.arange(0, m, 4)
    par1 = STPPar[idx + 0, :]
    par2 = STPPar[idx + 1, :]
    par3 = STPPar[idx + 2, :]
    par4 = STPPar[idx + 3, :]

    par_mean = [
        par1[-1, :].mean(),
        par2[-1, :].mean(),
        par3[-1, :].mean(),
        par4[-1, :].mean(),
    ]

    # SHP Par
    SHPPar = np.loadtxt(os.path.join(
        resultsdir, "FinallResultsESPar_SHP1.txt"))
    m, n = SHPPar.shape
    idx = np.arange(0, m, 7)
    parh1 = SHPPar[idx + 0, 1:]
    parh2 = SHPPar[idx + 1, 1:]
    parh3 = SHPPar[idx + 2, 1:]
    parh4 = SHPPar[idx + 3, 1:]
    parh5 = SHPPar[idx + 4, 1:]
    lai = SHPPar[idx + 5, 1:]
    vcmax = SHPPar[idx + 6, 1:]

    # Cable depths
    cable_depths = np.loadtxt(os.path.join(
        resultsdir, "FinalCableDepths1.txt"))
    m, n = cable_depths.shape
    idx = np.arange(0, m, 2)
    cable1 = cable_depths[idx + 0, :]
    cable2 = cable_depths[idx + 1, :]

    # Flux
    flux = np.loadtxt(os.path.join(resultsdir, "FinallResultsESFlux1.txt"))
    m, n = flux.shape
    idx = np.arange(0, m, 5)
    flux_hv = flux[idx + 0, 1:]
    flux_lev = flux[idx + 1, 1:]
    flux_hg = flux[idx + 2, 1:]
    flux_leg = flux[idx + 3, 1:]
    flux_g = flux[idx + 4, 1:]

    # Profile and Tuning
    profile = np.loadtxt(os.path.join(resultsdir, "FinalProfile1.txt"))
    tuning = np.loadtxt(os.path.join(resultsdir, "FinalTuning1.txt"))


# %% Plot soil moisture (probe vs ensemble)
fig, ax = plt.subplots(1, 1, figsize=(12, 5), dpi=100)
for n in range(N_ens):
    p, = ax.plot(time, M["M2"][:, n], color="gray", lw=0.5)

e, = ax.plot(time, Mmean[:, 1], color="C3")

smprobe = tahmo.loc[tstart:tend, smcols]
smprobe_time = (smprobe.index - t0).total_seconds() / (24 * 60 * 60)
probes = []
colors = ["k", "DarkCyan", "DarkMagenta"]
for icol, c in zip(smcols, colors):
    a, = ax.plot(smprobe_time, smprobe[icol], c=c)
    probes.append(a)

ax.legend([p, e] + probes,
          ["ensemble", "estimate 5cm"] + [f"probe_{i}" for i in range(3)],
          loc="best")

ax.grid(b=True)
ax.set_ylabel(r"$\theta$ (-)")
ax.set_xlim(times[0], times[-1])
ax.set_xlabel("time (days)")

fig.savefig("soilmoisture_ensembles.png", dpi=300, bbox_inches="tight")


# %% Plot soil temperature (probe vs ensemble)

fig, ax = plt.subplots(1, 1, figsize=(12, 5), dpi=100)
for n in range(N_ens):
    p, = ax.plot(time, T["T2"][:, n], color="gray", lw=0.5)

e, = ax.plot(time, Tmean[:, 1], color="C3")

stprobe = tahmo.loc[tstart:tend, stcols]
probes = []
colors = ["k", "DarkCyan", "DarkMagenta"]
for icol, c in zip(stcols, colors):
    a, = ax.plot(smprobe_time, stprobe[icol], c=c)
    probes.append(a)

tc1, = ax.plot(mtime, obsdf["T5"].iloc[:, 0], c="navy")
tc2, = ax.plot(mtime, obsdf["T10"].iloc[:, 0], c="SteelBlue")

ax.legend([p, e, tc1, tc2] + probes,
          ["ensemble", "estimate 5cm", "dts 5cm", "dts 10cm"] +
          [f"probe_{i}" for i in range(3)],
          loc="best", ncol=2)

ax.grid(b=True)
ax.set_ylabel(r"temperature ($\deg$ C)")
ax.set_xlim(time[0], time[-1])
ax.set_xlabel("time (days)")

fig.savefig("soiltemp_ensembles.png", dpi=300, bbox_inches="tight")

# %%
fig, ax = plt.subplots(1, 1, figsize=(14, 3))
ms = ax.matshow(profile[::2, 1:].T)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="3%", pad=0.05)
ax.yaxis.set_inverted(False)
ax.xaxis.set_ticks_position("bottom")
plt.colorbar(ms, cax=cax)
fig.suptitle("soil moisture?")
ax.set_xlabel("timestep")
ax.set_ylabel("depth?")
fig.tight_layout()

fig, ax = plt.subplots(1, 1, figsize=(14, 3))
ms = ax.matshow(profile[1::2, 1:].T)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="3%", pad=0.05)
ax.yaxis.set_inverted(False)
ax.xaxis.set_ticks_position("bottom")
plt.colorbar(ms, cax=cax)
fig.suptitle("soil temperature?")
ax.set_xlabel("timestep")
ax.set_ylabel("depth?")
fig.tight_layout()

# %% tuning

fig, ax = plt.subplots(4, 1, figsize=(14, 10), sharex=True)

for i in range(4):
    ax[i].plot(tuning[:, i])
    ax[i].grid(b=True)

ax[0].set_title("Tuning parameters?")
fig.tight_layout()

# %% cable depths

fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(8, 2), sharex=True)
ms = ax1.matshow(cable1[:, 1:])
divider = make_axes_locatable(ax1)
cax = divider.append_axes("right", size="3%", pad=0.05)
plt.colorbar(ms, cax=cax)

ms = ax2.matshow(cable2[:, 1:])
divider = make_axes_locatable(ax2)
cax = divider.append_axes("right", size="3%", pad=0.05)
plt.colorbar(ms, cax=cax)

for iax in [ax1, ax2]:
    iax.yaxis.set_inverted(False)
    iax.xaxis.set_ticks_position("bottom")
    iax.set_ylabel("?")
ax2.set_xlabel("ensemble number?")
ax1.set_title("cable depth?")
fig.tight_layout()


# %% Comparing METEO input and DTS and probe and model temp

f = (r".\hydrus_io\PassiveRealDTS\METEO1.IN")

df = pd.read_csv(f, skiprows=25, header=None, skipfooter=1, sep="\s+",
                 names=['t', "Rad", "TMax", "TMin", "RHMean",
                        "Wind", "SunHours"], engine="python")
# df["t"] = df["t"] - (df["t"].iloc[-1] - dt_in_days)
df = df.set_index("t")
fig, axes = plt.subplots(2, 1, figsize=(12, 7), dpi=100, sharex=True)
# for n in range(6):
e, = axes[0].plot(time, Tmean[:, 1], label="model temp, highest stored layer")
# axes[0].legend(loc="best")
tahmo_time = (tahmo.loc[tstart:tend].index -
              t0).to_numpy() / np.timedelta64(1, "D")
axes[0].plot(tahmo_time, tahmo.loc[tstart:tend,
                                   'temperature (degrees Celsius)'], c="r",
             label="air temperature (TAHMO)")
axes[0].plot(tahmo_time, tahmo.loc[tstart:tend, stcols[0]],
             c="k", label="soil temperature (TAHMO)")
axes[0].plot(mtime, obsdf["T5"].iloc[:, 0], c="navy", label="DTS 5cm depth")
axes[0].plot(mtime, obsdf["T10"].iloc[:, 0], c="cyan", label="DTS 10cm depth")
# df["TMax"].plot(ax=axes[0], label="TMax (from METEO.IN)")
# df["TMin"].plot(ax=axes[0], label="TMin (from METEO.IN)")

df["Rad"].plot(ax=axes[1], label="Radiation (from METEO.IN)")
axes[1].plot(tahmo_time, tahmo.loc[tstart:tend,
                                   "radiation (W/m2)"] * (24 * 50 * 60 * 1e-6),
             label="Radiation (TAHMO)")

for iax in axes:
    iax.legend(loc="best", ncol=2)
    iax.grid(b=True)

axes[0].set_xlim(tstart_num, tend_num)
axes[0].set_ylabel("Temperature (degrees C)")
axes[1].set_ylabel("Radiation (MJ/d/m2)")

fig.savefig("overview_temperature_radiation.png", dpi=300, bbox_inches="tight")

fig, ax = plt.subplots(1, 1, figsize=(12, 5), dpi=100)
for i in range(6):
    e, = ax.plot(time, Tmean[:, i], label=f"T{i}")
ax.legend(loc="best")
ax.grid(b=True)
ax.set_xlabel("t (days)")
ax.set_ylabel("temperature ($\circ$ C)")
fig.savefig("soiltemperature_different_depths.png", dpi=300,
            bbox_inches="tight")

fig, ax = plt.subplots(1, 1, figsize=(12, 5), dpi=100)
for i in range(6):
    e, = ax.plot(time, Mmean[:, i], label=f"SM{i}")
ax.legend(loc="best")
ax.grid(b=True)
ax.set_xlabel("t (days)")
ax.set_ylabel(r"$\theta$ (-)")
fig.savefig("soilmoisture_different_depths.png", dpi=300,
            bbox_inches="tight")
