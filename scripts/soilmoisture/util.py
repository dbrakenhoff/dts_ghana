import os
from datetime import datetime, timedelta

import numpy as np
from pandas import Timedelta


def datetime_to_matlab(tindex):
    mdn = tindex + Timedelta(days=366)
    frac = (tindex - tindex.round("D")).seconds / (24.0 * 60.0 * 60.0)
    return mdn.toordinal() + frac


def datenum_to_datetime(datenum):
    """
    Convert Matlab datenum into Python datetime.
    Parameters
    ----------
    datenum: float
        date in datenum format

    Returns
    -------
    datetime :
        Datetime object corresponding to datenum.
    """
    days = datenum % 1.
    return datetime.fromordinal(int(datenum)) \
        + timedelta(days=days) \
        - timedelta(days=366)


def smooth(a, window):
    """MATLAB (default) moving average smoothing function

    Parameters
    ----------
    a : np.array
        1-D array to smooth
    window : int
        windowsize for moving average smoothing, must be odd.

    Returns
    -------
    arr : np.array
        smoothed array

    """
    assert window % 2 == 1, "window has to be odd!"
    out0 = np.convolve(a, np.ones(window, dtype=int), 'valid') / window
    r = np.arange(1, window - 1, 2)
    start = np.cumsum(a[:window - 1])[::2] / r
    stop = (np.cumsum(a[:-window:-1])[::2] / r)[::-1]
    return np.concatenate((start, out0, stop))


def get_roeseta_params(sand, silt, bulkdens, len_unit, t_unit):

    lines = [
        "{:5.2f}\n".format(sand),
        "{:5.2f}\n".format(silt),
        "{:5.2f}\n".format(100 - sand - silt),
        "{:5.2f}".format(bulkdens)
    ]

    # write input file
    with open("test.txt", "w") as fw:
        fw.writelines(lines)

    # run program Roeseta
    os.system("Roeseta.exe")

    # load results
    curdir = os.path.dirname(__file__)
    params = np.loadtxt(os.path.join(curdir, "../../data/rosetta/resul.txt"))

    # get length and time multipliers
    lendict = {"m": 1e-2, "cm": 1, "mm": 10}
    len_mult = lendict[len_unit]

    tdict = {"day": 1., "hour": 1. / 24., "minute": 1. /
             24. / 60., "second": 1. / 24. / 60. / 60.}
    t_mult = tdict[t_unit]

    # add extra parameter = 0.5
    params = np.r_[params.squeeze(), 0.5 * np.ones((1,))]
    params[2] *= len_mult
    params[4] *= len_mult * t_mult

    # TODO: reactivate when exe is working again
    # clean up
    # os.remove("test.txt")
    # os.remove("resul.txt")

    return params
