import os
import pandas as pd


def write_atmosphere(basic_info, atmosphere_info, atmosphere_df,
                     fname="ATMOSPH.IN", ws=".", verbose=True):
    """Method to write the ATMOSPH.IN file

    """
    # 1 Write Header information
    lines = ["Pcp_File_Version={}\n".format(basic_info["iVer"]),
             "*** BLOCK I: ATMOSPHERIC INFORMATION  "
             "**********************************\nMaxAL "
             "(MaxAL = number of atmospheric data-records)\n"]

    # Print some values
    lines.append("{}\n".format(atmosphere_df.index.size))

    vars5 = ["lDailyVar", "lSinusVar", "lLai", "lBCCycles", "lInterc"]

    # add dummies and add to lines
    lines.append(" ".join(vars5 + ["lDummy"] * 5 + ["\n"]))
    vals = []
    for var in vars5[:-1]:
        val = atmosphere_info[var]
        if var:
            if val is True:
                vals.append("t")
            elif val is False:
                vals.append("f")
            else:
                vals.append(str(val))
    # add dummies vals and add to lines
    lines.append(" ".join(vals + ["f"] * 5 + ["\n"]))

    lines.append("hCritS (max. allowed pressure head at the soil "
                 "surface)\n{}\n".format(atmosphere_info["hCritS"]))

    lines.append(atmosphere_df.to_string(index=False))
    lines.append("\nend*** END OF INPUT FILE ATMOSPH.IN "
                 "**********************************")
    # Write the actual file
    fname = os.path.join(ws, fname)
    with open(fname, "w") as file:
        file.writelines(lines)
    if verbose:
        print("Successfully wrote {}".format(fname))


if __name__ == "__main__":

    basic_info = {"iVer": 4}

    atmosphere_info = {
        "lDailyVar": False,
        "lSinusVar": False,
        "lLai": False,
        "lBCCycles": False,
        "lInterc": False,
        "lExtinct": 0.463,
        "hCritS": 1e30,
    }

    atmocols = ["tAtm", "Prec", "rSoil", "rRoot", "hCritA", "rB", "hB", "hT",
                "tTop", "tBot", "Ampl", "RootDepth"]
    atmodf = pd.DataFrame(columns=atmocols, data=0, index=range(10))
    ws = './python_files'

    write_atmosphere(basic_info, atmosphere_info, atmodf,
                     fname="ATMOSPH.IN", ws=ws, verbose=True)
