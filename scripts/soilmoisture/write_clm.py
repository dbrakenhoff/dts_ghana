import os
import pandas as pd
import numpy as np


def write_clm(clm_info, root_dist, description=None,
              fname="CLM_INPUT.IN", ws=".", verbose=True):

    if description is None:
        description = "CLM file written by Python {}".format(
            pd.Timestamp.now().strftime("%Y-%m-%d %H:%M:%S"))

    lines = [
        "Inputs for CLM boundary\n"
        "{}\n".format(description)
    ]

    extra_desc = {
        "alpha_stem": " (CLM 4, Table3.1, note alpha has two values for each)",
        "tau_stem": " (CLM 4, Table3.1, note tau has two values for each)",
        "weight": " (LAI, and the ratio between leaf and stem)",
        "Zatm": ",m (Height of temperature measurement, in m)",
        "Ztop": " (LSM P 62. Ztop is in CLM table 2.2)",
        "field_cap": " (CLM 4, Page 163, Table 8.1)",
        "rb_photo": " (CLM 4, Table 8.3)"

    }

    sep = "-" * 77 + "\n"
    lines.append(sep)

    variables = [["Latitude", "Longitude", "Timezone"],
                 ["xL", "alpha_leaf", "alpha_stem"],
                 ["tau_leaf", "tau_stem"],
                 ["LAI", "weight", "T3 vegetation?", "Vcmax25"]]
    for varlist in variables:
        lines.append(", ".join(
            [var + extra_desc[var] if var in extra_desc.keys()
             else var for var in varlist]) + "\n")
        values = []
        for var in varlist:
            val = clm_info[var]
            if isinstance(val, list):
                values += val
            else:
                values.append(val)
        template = "{}  " * len(values)
        lines.append(template.format(*values) + "\n")

    lines.append(sep)

    variables = [["Zatm"],
                 ["Zom", "d", "Ztop"],
                 ["CNL", "SLA0", "SLAm", "m_photo", "alpha_wj", "FN",
                  "FLNR", "fai_c", "fai_o", "field_cap"],
                 ["ra_photo", "rb_photo"]]
    for varlist in variables:
        lines.append(", ".join(
            [var + extra_desc[var] if var in extra_desc.keys()
             else var for var in varlist]) + "\n")
        template = "{:.2f}  " * len(varlist)
        lines.append(template.format(*[clm_info[var]
                                       for var in varlist]) + "\n")

    lines.append(sep)

    lines.append("Root distribution\n")
    lines.append(root_dist.transpose().to_string(index=False, header=False))

    # Write the actual file
    fname = os.path.join(ws, fname)
    with open(fname, "w") as file:
        file.writelines(lines)
    if verbose:
        print("Successfully wrote {}".format(fname))


if __name__ == "__main__":

    description = "CLM file by Python {}".format(
        pd.Timestamp.now().strftime("%Y-%m-%d %H:%M:%S"))

    clm_info = {
        "xL": 0.30,
        "LAI": 1.82,
        "weight": 0.99,
        "T3 vegetation?": 1.00,
        "Vcmax25": 10.0,
        "ra_photo": 4.32,
        "rb_photo": 15.00,
        "Latitude": 35.50,
        "Longitude": -98.00,
        "Timezone": -6.00,
        "alpha_leaf": [0.13, 0.40],
        "alpha_stem": [0.40, 0.69],
        "tau_leaf": [0.05, 0.34],
        "tau_stem": [0.10, 0.21],
        "Zatm": 2.00,
        "Zom": 0.06,
        "d": 0.34,
        "Ztop": 0.50,
        "CNL": 25.00,
        "SLA0": 0.03,
        "SLAm": 0.00,
        "m_photo": 5.00,
        "alpha_wj": 0.04,
        "FN": 0.64,
        "FLNR": 0.09,
        "fai_c": -1500000.00,
        "fai_o": -74000.00,
        "field_cap": 0.30

    }

    root_dist = pd.DataFrame(index=range(101), data=0.0, columns=["root_dist"])
    root_dist.iloc[:3, 0] = [0.86, 0.12, 0.02]

    ws = "./python_files"

    write_clm(clm_info, root_dist, ws=ws)
