import os
import pandas as pd


def write_meteo(basic_info, meteo_info, meteodf,
                fname="METEO.IN", ws=".", verbose=True):

    # Header information
    lines = [
        "Pcp_File_Version={}\n"
        "{}\n".format(basic_info["iVer"],
                      "* METEOROLOGICAL PARAMETERS AND INFORMATION " + "*" * 35)
    ]

    variables = ["MeteoRecords", "Radiation", "Penman-Hargreaves"]
    lines.append("  ".join(variables) + "\n")
    lines.append("{}  {}  {}\n".format(meteodf.index.size,
                                       *[meteo_info[val] for val in
                                         variables[1:]]))

    variables = ["lEnBal", "lDaily"]

    lines.append("{}  {}  lDummy  lDummy  lDummy  lDummy  lDummy  lDummy  "
                 "lDummy  lDummy\n".format(*variables))
    lines.append("{}  {}  f  f  f  "
                 "t  f  f  f  f\n".format(*["t" if meteo_info[var] else "f"
                                            for var in variables]))

    variables = [["Latitude", "Altitude"],
                 ["ShortWaveRadA", "ShortWaveRadB"],
                 ["LongWaveRadA", "LongWaveRadB"],
                 ["LongWaveRadA1", "LongWaveRadB1"],
                 ["WindHeight", "TempHeight"]
                 ]

    for varlist in variables:
        lines.append("  ".join(varlist) + "\n")
        lines.append("{}  {}".format(*[meteo_info[val]
                                       for val in varlist]) + "\n")

    lines.append("iCrop (=0: no crop, =1: constant, =2: table, =3: daily)"
                 "  SunShine  RelativeHum\n")
    lines.append("{}  {}  {}\n".format(meteo_info["iCrop"],
                                       meteo_info["SunShine"],
                                       meteo_info["RelativeHum"]))

    lines.append("cloud\n")
    lines.append("{}  {}\n".format(*meteo_info["cloud"]))
    lines.append("Albedo\n")
    lines.append("{}\n".format(meteo_info["Albedo"]))

    lines.append("Daily values\n")
    lines.append("  ".join(meteodf.columns) + "\n")
    lines.append("[T]  [MJ/m2/d]\n")
    lines.append(meteodf.to_string(index=False, header=False) + "\n")

    lines.append('end *** END OF INPUT FILE METEO.IN' + "*" * 34)

    # Write the actual file
    fname = os.path.join(ws, fname)
    with open(fname, "w") as file:
        file.writelines(lines)
    if verbose:
        print("Successfully wrote {}".format(fname))


if __name__ == "__main__":
    ws = "./python_files"

    # meteo data
    meteocols = ["t", "Rad", "TMax", "TMin", "RHMean", "Wind",
                 "SunHours", "CropHeight", "Albedo", "LAI(SCF)", "rRoot"]
    meteodf = pd.DataFrame(columns=meteocols, data=0.0,
                         index=range(10))

    meteo_info = {
        "Radiation": 1,
        "Penman-Hargreaves": "f",
        "lEnBal": True,
        "lDaily": False,
        "Latitude": 3.385000e+01,
        "Altitude": 306,
        "ShortWaveRadA": 0.25,
        "ShortWaveRadB": 0.50,
        "LongWaveRadA": 0.90,
        "LongWaveRadB": 0.10,
        "LongWaveRad1": 0.34,
        "LongWaveRadB1": -0.139,
        "WindHeight": 200,
        "TempHeight": 200,
        "iCrop": 0,
        "SunShine": 3,
        "RelativeHum": 0,
        "cloud": [1.35, -0.35],
        "Albedo": 0.23
    }

    basic_info = {
        "iVer": 4,
        "Hed": "Written with methods derived from Pydrus 0.0.2",
        "LUnit": "cm",
        "TUnit": "days",
        "MUnit": "mmol",
        "lWat": True,
        "lChem": False,
        "lTemp": True,
        "lSink": False,
        "lRoot": False,
        "lShort": False,
        "lWDep": True,
        "lScreen": False,
        "lVariabBC": True,
        "lEquil": True,
        "lInverse": False,
        "lSnow": False,
        "lHP1": False,
        "lMeteo": True,
        "lVapor": True,
        "lActiveU": False,
        "lFluxes": False,
        "lIrrig": False,
        "CosAlpha": 1}

    write_meteo(basic_info, meteo_info, meteodf, ws=ws)
