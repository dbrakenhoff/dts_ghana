import os
import pandas as pd


def write_profile(basic_info, profile_df, obs_nodes, n_solutes=1,
                  fname="PROFILE.DAT", ws=".", verbose=True):
    """Method to write the profile.dat file.

    """
    # 1 Write Header information
    lines = ["Pcp_File_Version={}\n"
             "2\n"
             "1  0.000000e+000  1.000000e+000  1.000000e+000\n"
             "2 -5.000000e+001  1.000000e+000  1.000000e+000\n"
             "{} {} {} {}".format(basic_info["iVer"],
                                  profile_df.index.size,
                                  n_solutes,
                                  1 if basic_info["lChem"] else 0,
                                  1),
             # 2. Write the profile data
             profile_df.to_string(index=False),
             # 3. Write observation points
             "\n{}\n".format(len(obs_nodes)),
             "".join(["   {}".format(i) for i in obs_nodes])]

    # Write the actual file
    fname = os.path.join(ws, fname)
    with open(fname, "w") as file:
        file.writelines(lines)

    if verbose:
        print("Successfully wrote {}".format(fname))


if __name__ == "__main__":
    basic_info = {
        "iVer": 4,
        "lChem": False
    }

    n_solutes = 1

    obs_nodes = [3, 6, 11, 21, 51, 1]

    cols = ['x', 'h', 'Mat', 'Lay', 'Beta',
            'Axz', 'Bxz', 'Dxz', 'Temp', 'Conc']
    profile = pd.DataFrame(columns=cols, index=range(10), data=0.0)

    ws = "./python_files"

    write_profile(basic_info, profile, obs_nodes, ws=ws)
