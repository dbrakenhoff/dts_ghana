import os
import pandas as pd


def write_selector(basic_info, time_info, times, water_flow, materials,
                   heat_transport_info, heat_params, n_materials=1, n_layers=1,
                   drains=False, fname="SELECTOR_LU.IN", ws=".",
                   description=None, verbose=True):
    """Write the selector.in file.

    """

    if description is None:
        description = "SELECTOR file written by Python {}".format(
            pd.Timestamp.now().strftime("%Y-%m-%d %H:%M:%S"))

    # Create Header string
    string = "*** BLOCK {:{}{}{}}\n"

    # Write block A: BASIC INFORMATION
    lines = [
        "Pcp_File_Version={}\n"
        "{}{}\n"
        "{}\n"
        "LUnit TUnit MUnit  (indicated units are "
        "obligatory for all input data)\n"
        "{}\n"
        "{}\n"
        "{}\n".format(basic_info["iVer"],
                      string.format("A: BASIC INFORMATION ", "*", "<", 61),
                      basic_info["Hed"], description,
                      basic_info["LUnit"], basic_info["TUnit"],
                      basic_info["MUnit"])
    ]

    vars_list = [["lWat", "lChem", "lTemp", "lSink", "lRoot", "lShort",
                  "lWDep", "lScreen", "lVariabBC", "lEquil", "lInverse",
                  "\n"],
                 ["lSnow", "lHP1", "lMeteo", "lVapor", "lActiveU", "lFluxes",
                  "lIrrig", "lDummy", "lDummy", "lDummy", "\n"]]

    for variables in vars_list:
        lines.append("  ".join(variables))
        lines.append("  ".join("t" if basic_info[var] else "f" for
                               var in variables[:-1]))
        lines.append("\n")

    lines.append("NMat NLay CosAlfa \n"
                 "{} {} {}\n".format(n_materials, n_layers,
                                     basic_info["CosAlpha"]))

    # Write block B: WATER FLOW INFORMATION
    lines.append(string.format("B: WATER FLOW INFORMATION ", "*", "<", 61))
    lines.append("MaxIt  TolTh  TolH   (maximum number of iterations and "
                 "tolerances)\n")
    variables = ["MaxIt", "TolTh", "TolH"]
    lines.append(
        "   ".join([str(water_flow[var]) for var in variables]))
    lines.append("\n")

    vars_list = [["TopInf", "WLayer", "KodTop", "InitCond", "\n"],
                 ["BotInf", "qGWLF", "FreeD", "SeepF", "KodBot", "DrainF",
                  "hSeep", "\n"]]

    upper_condition = (water_flow["KodTop"] < 0
                       and not water_flow["TopInf"])

    lower_condition = ((water_flow["KodBot"] < 0)
                       and not water_flow["BotInf"]
                       and not water_flow["qGWLF"]
                       and not water_flow["FreeD"]
                       and not water_flow["SeepF"]
                       )

    if upper_condition or lower_condition:
        vars_list.append(["rTop", "rBot", "rRoot", "\n"])

    if water_flow["qGWLF"]:
        vars_list.append(["GWL0L", "Aqh", "Bqh", "\n"])

    vars_list.append(["hTab1", "hTabN", "\n"])
    vars_list.append(["Model", "Hysteresis", "\n"])

    if water_flow["Hysteresis"] > 0:
        vars_list.append(["iKappa", "\n"])

    for variables in vars_list:
        lines.append("  ".join(variables))
        values = []
        for var in variables[:-1]:
            val = water_flow[var]
            if val is True:
                values.append("t")
            elif val is False:
                values.append("f")
            else:
                values.append(str(val))
        values.append("\n")
        lines.append("     ".join(values))

    if drains:
        raise NotImplementedError

    # Write the material parameters (TODO: hard-coded reference to water, is that right?)
    lines.append(materials["water"].to_string(index=False))
    lines.append("\n")

    # Write BLOCK C: TIME INFORMATION
    lines.append(string.format("C: TIME INFORMATION ", "*", "<", 61))
    vars_list = [
        ["dt", "dtMin", "dtMax", "dMul", "dMul2", "ItMin", "ItMax",
            "MPL", "\n"], ["tInit", "tMax", "\n"],
        ["lPrintD", "nPrintSteps", "tPrintInterval", "lEnter", "\n"]]
    for variables in vars_list:
        lines.append(" ".join(variables))
        values = []
        for var in variables[:-1]:
            val = time_info[var]
            if val is True:
                values.append("t")
            elif val is False:
                values.append("f")
            else:
                values.append(str(val))
        values.append("\n")
        lines.append(" ".join(values))

    lines.append("TPrint(1),TPrint(2),...,TPrint(MPL)\n")
    for i in range(int(len(times) / 6) + 1):
        lines.append(
            " ".join([str(time) for time in times[i * 6:i * 6 + 6]]) + "\n")

    # Write BLOCK D: Root Growth Information
    # if basic_info["lRoot"]:
    #     lines.append(
    #         string.format("D: ROOT GROWTH INFORMATION ", "*", "<", 72))
    #     lines.append("iRootDepthEntry\n{}\n".format(root_growth[
    #         "iRootIn"]))
    #     d = root_growth.copy()
    #     d.pop("iRootIn")
    #     d["\n"] = "\n"
    #     lines.append("    ".join(d.keys()))
    #     lines.append("    ".join(str(p) for p in d.values()))

    # Write Block E - Heat transport information
    if basic_info["lTemp"]:
        lines.append(string.format("E: HEAT TRANSPORT INFORMATION ",
                                   "*", "<", 61))
        lines.append(heat_params.to_string(index=False))
        lines.append(
            "\nAmpl tPeriod Campbell SnowMF lDummy lDummy lDummy "
            "lDummy lDummy\n"
            "{} {} {} {} f f f f f\n"
            "kTopT TTop kBotT TBot\n"
            "{} {} {} {}\n".format(heat_transport_info["tAmpl"],
                                   heat_transport_info["tPeriod"],
                                   heat_transport_info["Campbell"],
                                   heat_transport_info["SnowMF"],
                                   heat_transport_info["kTopT"],
                                   heat_transport_info["TTop"],
                                   heat_transport_info["kBotT"],
                                   heat_transport_info["TBot"]))

    # Write Block F - Solute transport information
    # if basic_info["lChem"]:
    #     lines.append(string.format("C: SOLUTE TRANSPORT INFORMATION ",
    #                                "*", "<", 72))
    #     lines.append(" Epsi lUpW lArtD lTDep cTolA cTolR MaxItC PeCr "
    #                  "No.Solutes lTort iBacter lFiltr nChPar\n"
    #                  "{} {} {} {} {} {} {} {} {} {} {} {} {}\n"
    #                  "iNonEqul lWatDep lDualNEq lInitM lInitEq lTort "
    #                  "lDummy lDummy lDummy lDummy lCFTr\n"
    #                  "{} {} {} {} {} {} {} {} {} {} {}\n".format(
    #                      solute_transport["Epsi"],
    #                      "t" if solute_transport["lUpW"] else "f",
    #                      "t" if solute_transport["lArtD"] else "f",
    #                      "t" if solute_transport["ltDep"] else "f",
    #                      solute_transport["cTolA"],
    #                      solute_transport["cTolR"],
    #                      solute_transport["MaxItC"],
    #                      solute_transport["PeCr"],
    #                      n_solutes,
    #                      "t" if solute_transport["lTort"] else "f",
    #                      solute_transport["iBacter"],
    #                      "t" if solute_transport["lFiltr"] else "f",
    #                      get_empty_solute_df().columns.size + 2,
    #                      solute_transport["iNonEqual"],
    #                      "t" if solute_transport["lWatDep"] else "f",
    #                      "t" if solute_transport["lDualEq"] else "f",
    #                      "f", "f",
    #                      "t" if solute_transport["lTort"] else "f",
    #                      "f", "f", "f", "f", "f"
    #                  ))

    #     # Write the material parameters
    #     lines.append(materials["solute"].to_string(index=False))
    #     lines.append("\n")

    #     for sol in solutes:
    #         lines.append(
    #             "DifW DifG\n{} {}\n{}\n".format(
    #                 sol["difw"],
    #                 sol["difg"],
    #                 sol["data"].to_string(index=False)))

    #     lines.append("kTopSolute SolTop kBotSolute SolBot\n{} {} {} {}\n"
    #                  "tPulse\n{}\n".format(solute_transport["kTopCh"],
    #                                        " ".join([str(s["top_conc"])
    #                                                  for s in
    #                                                  solutes]),
    #                                        solute_transport["kBotCh"],
    #                                        " ".join([str(s["bot_conc"])
    #                                                  for s in
    #                                                  solutes]),
    #                                        solute_transport["tPulse"]
    #                                        ))

    # Write Block G - Root water uptake information
    # if basic_info["lSink"]:
    #     lines.append(string.format("G: ROOT WATER UPTAKE INFORMATION ",
    #                                "*", "<", 72))
    #     vars_list = [["iMoSink", "cRootMax", "OmegaC", "\n"]]

    #     if root_uptake["iMoSink"] == 0:
    #         vars_list.append(
    #             ["P0", "P2H", "P2L", "P3", "r2H", "r2L", "\n"])

    #     for variables in vars_list:
    #         lines.append(" ".join(variables))
    #         lines.append("    ".join(str(root_uptake[var]) for var in
    #                                  variables[:-1]))
    #         lines.append("\n")

    #     lines.append("POptm(1),POptm(2),...,POptm(NMat)\n")
    #     lines.append("    ".join(str(p) for p in root_uptake[
    #         "POptm"]))
    #     lines.append("\n")

    #     if basic_info["lChem"]:
    #         lines.append("Solute Reduction\nf\n")

    # Write Block J - Inverse solution information
    # if basic_info["lInverse"]:
    #     raise NotImplementedError("The inverse modeling module from "
    #                               "Hydrus-1D will not be supported. "
    #                               "Python packages are used for this.")

    # Write Block K – Carbon dioxide transport information

    # Write Block M – Meteorological information
    # if basic_info["lMeteo"]:
    #     print("NO METEO INFO...")
        # raise NotImplementedError

    # Write END statement
    lines.append('*** END OF INPUT FILE SELECTOR.IN' + "*" * 36)

    # Write the actual file
    fname = os.path.join(ws, fname)
    with open(fname, "w") as file:
        file.writelines(lines)

    if verbose:
        print("Successfully wrote {}".format(fname))


if __name__ == "__main__":

    basic_info = {
        "iVer": 4,
        "Hed": "Written with methods derived from Pydrus 0.0.2",
        "LUnit": "cm",
        "TUnit": "days",
        "MUnit": "mmol",
        "lWat": True,
        "lChem": False,
        "lTemp": True,
        "lSink": False,
        "lRoot": False,
        "lShort": False,
        "lWDep": True,
        "lScreen": False,
        "lVariabBC": True,
        "lEquil": True,
        "lInverse": False,
        "lSnow": False,
        "lHP1": False,
        "lMeteo": True,
        "lVapor": True,
        "lActiveU": False,
        "lFluxes": False,
        "lIrrig": False,
        "CosAlpha": 1}

    description = "Model for Jianzhi Dong's Soil Moisture DTS "

    n_materials = 1
    n_layers = 1

    water_flow = {
        "MaxIt": 1250,
        "TolTh": 0.010,
        "TolH": 5,
        "TopInf": True,
        "WLayer": True,
        "KodTop": -1,
        "InitCond": True,
        "BotInf": False,
        "qGWLF": False,
        "FreeD": True,
        "SeepF": False,
        "KodBot": -1,
        "DrainF": False,
        "hSeep": 0,
        "hTab1": 1e-005,
        "hTabN": 100000,
        "Model": 0,
        "Hysteresis": 0
    }

    materials_df = pd.DataFrame(columns=["thr", "ths", "Alfa", "n", "Ks", "l"],
                                data=[[0.062, 0.399, 0.0119, 1.48, 13.092, 0.5]],
                                index=[0])

    materials = {
        "water": materials_df
    }

    drains = False

    time_info = {
        "dt": 0.003,
        "dtMin": 0.00001,
        "dtMax": 0.14,
        "dMul": 1.3,
        "dMul2": 0.7,
        "ItMin": 3,
        "ItMax": 7,
        "MPL": 1,
        "tInit": 107,
        "tMax": 151,
        "lPrintD": False,
        "nPrintSteps": 1,
        "tPrintInterval": 1,
        "lEnter": True,
    }

    times = range(6)

    heat_params = pd.DataFrame(columns=["Qn", "Qo", "Disper.", "B1", "B2", "B3", "Cn", "Co", "Cw"],
                               data=[[0.60, 0, 0.001000, 3.731290e-01, 5.194220e-01, 1.666762e+00,
                                      183327000000000, 187370000000000, 312035000000000]],
                               index=[0])

    heat_transport_info = {
        "tAmpl": 0,
        "tPeriod": 0,
        "Campbell": 2,
        "SnowMF": 0.43,
        "kTopT": -1,
        "TTop": 20,
        "kBotT": 0,
        "TBot": 20
    }

    ws = "./python_files"

    write_selector(basic_info, time_info, times, water_flow,
                   materials, heat_transport_info, heat_params,
                   n_materials=n_materials, n_layers=n_layers,
                   fname="SELECTOR_LU.IN", ws=ws, description=description,
                   verbose=True)
